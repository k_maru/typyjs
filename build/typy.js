/*!
* TypyJS JavaScript Library v0.0.4
* Copyright 2013, Kazuhide Maruyama
* TypyJS may be freely distributed under the MIT license.
*/
;(function(global){

    "use strict";

    var Typy = {
        version: "0.0.4"
    };

/* core */

var parseInt10 = function (val) {
        return parseInt(val, 10);
    },
    parseInt16 = function (val) {
        return parseInt(val, 16);
    },
    toString = Object.prototype.toString,
    hasOwnProperty = Object.prototype.hasOwnProperty,
    isXWithCallback = function(func, args){
        var i = 0, l = args.length;
        if(l === 0){
            return false;
        }else{
            for(; i < l; i++){
                if(!func(args[i])){
                    return false;
                }
            }
        }
        return true;
    },
    isX = function(name, args, additional){
        var key = "[object " + name + "]";
        additional = additional || function(){return true;};
        return isXWithCallback(function(v){return toString.call(v) === key && additional(v);}, args);
    },
    /***
     * @module Typy
     * @method isUndef
     * @summary
     * 指定された引数の値がすべて undefined かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean 指定された引数の値がすべて undefined の場合は true 、そうでない場合は false
     * @example
     * Typy.isUndef("abcde") -> false
     * Typy.isUndef(undefined) -> true
     * Typy.isUndef("abcde", undefined) -> false
     * Typy.isUndef(void 0, undefined) -> true
     */
    isUndef = function () {
        return isXWithCallback(function(v){return typeof v === "undefined";}, slice.call(arguments));
    },
    /***
     * @module Typy
     * @method isObj
     * @summary
     * 指定された引数の値がすべて Object かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean
     * 指定された引数の値がすべて Object の場合は true 、 そうでない場合は false
     * @example
     * Typy.isObj({param1: "A"}) -> true
     * Typy.isObj({param1: "A"}, {param2: "B"}) -> true
     * Typy.isObj(1) -> false
     * Typy.isObj(1, {param2: "B"}) -> false
     */
    isObj = function () {
        return isX("Object", slice.call(arguments), function(v){
            return typeof v  !== "undefined" && v !== null;
        });
    },
    /***
     * @module Typy
     * @method isFunc
     * @summary
     * 指定された引数の値がすべて Function かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean
     * 指定された引数の値がすべて Function の場合は true 、 そうでない場合は false
     * @example
     * Typy.isFunc(function(){}) -> true
     * Typy.isFunc(function(){}, function(){}) -> true
     * Typy.isFunc(1) -> false
     * Typy.isFunc(1, function(){}) -> false
     */
    isFunc = function () {
        return isX("Function", slice.call(arguments));
    },
    /***
     * @module Typy
     * @method isStr
     * @summary
     * 指定された引数の値がすべて String かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean
     * 指定された引数の値がすべて String の場合は true 、 そうでない場合は false
     * @example
     * Typy.isStr("abcd") -> true
     * Typy.isStr("abcd", "1234") -> true
     * Typy.isStr(1) -> false
     * Typy.isStr(1, "abcd") -> false
     */
    isStr = function () {
        return isX("String", slice.call(arguments));
    },
    /***
     * @module Typy
     * @method isArray
     * @summary
     * 指定された引数の値がすべて Array かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean
     * 指定された引数の値がすべて Array の場合は true 、 そうでない場合は false
     * @example
     * Typy.isArray([1, 2, 3, 4]) -> true
     * Typy.isArray(["a", "b", "c"], [1, 2, 3]) -> true
     * Typy.isArray(1) -> false
     * Typy.isArray(1, [1, 2, 3]) -> false
     */
    isArray = function () {
        return isX("Array", slice.call(arguments));
    },
    /***
     * @module Typy
     * @method isNum
     * @summary
     * 指定された引数の値がすべて Number かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean
     * 指定された引数の値がすべて Number の場合は true 、 そうでない場合は false
     * @example
     * Typy.isNum(123) -> true
     * Typy.isNum(123, 987) -> true
     * Typy.isNum("abc") -> false
     * Typy.isNum("abc", 123) -> false
     */
    isNum = function () {
        return isX("Number", slice.call(arguments));
    },
    /***
     * @module Typy
     * @method isNull
     * @summary
     * 指定された引数の値がすべて Null かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean
     * 指定された引数の値がすべて Null の場合は true 、 そうでない場合は false
     * @example
     * Typy.isNull(null) -> true
     * Typy.isNull(null, null) -> true
     * Typy.isNull(undefined) -> false
     * Typy.isNull(null, undefined) -> false
     */
    isNull = function () {
        return isXWithCallback(function(v){return v === null;}, slice.call(arguments));
    },
    isUndefOrNull = function () {
        return isXWithCallback(function(v){
            return v === null || typeof v === "undefined";
        }, slice.call(arguments));
    },
    isBool = function () {
        return isX("Boolean", slice.call(arguments));
    },
    isDate = function () {
        return isX("Date", slice.call(arguments));
    },
    isRegExp = function(){
        return isX("RegExp", slice.call(arguments));
    },
    isPrimitive = function () {
        return isXWithCallback(function(v){
            return typeof v === "string" ||
                typeof v === "number" ||
                typeof v === "boolean";
        }, slice.call(arguments));
    },
    isNumeric = function () {
        return isXWithCallback(function(v){
            return !isNaN(parseFloat(v)) && isFinite(v);
        }, slice.call(arguments));
    },
    isInt = function(){
        return isXWithCallback(function(v){
            return isNum(v) &&  (v % 1 === 0);
        }, slice.call(arguments));
    },
    isUnusable = function () {
        return isXWithCallback(function(v){
            return isUndef(v) ||
                isNull(v) ||
                (isNum(v) ? isNaN(v) || !isFinite(v) : false);
        }, slice.call(arguments));
    },
    ifUndef = function (value, def) {
        return isUndef(value) ? def : value;
    },
    ifNull = function (value, def) {
        return isNull(value) ? def : value;
    },
    ifUndefOrNull = function(value, def){
        return isUndef(value) || isNull(value) ? def : value;
    },
    ifUnusable = function (value, def) {
        return isUnusable(value) ? def : value;
    },
    /***
     * @module Typy
     * @method extend
     * @summary 第１引数に指定された Object に対して、後続する引数の値のプロパティを設定します。
     * @param target:Object
     * 拡張する Object
     * @param ...args:Any[] 設定するプロパティを持つ値
     * @return Object プロパティが設定された第１引数に指定された Object
     * @example
     * Typy.extend({prop1: true, prop2: false}, {prop3: "A}, {prop1: "B"})
     *   -> {prop1: "B", prop2: false, prop3: "A"}
     */
    /***
     * @module Typy
     * @override extend
     * @summary 第１引数に指定された Functoin に対して、後続する引数の値のプロパティを設定します。
     * @param target:Functoin 拡張する Functoin
     * @param ...args:Any[] 設定するプロパティを持つ値
     * @return Functoin プロパティが設定された第１引数に指定された Functoin
     */
    /***
     * @module Typy
     * @override extend
     * @summary 引数で指定された値のプロパティを設定した、新しい Object を返します。
     * @param ...args:Any[] 設定するプロパティを持つ値
     * @return Object プロパティが設定された新しい Object
     */
    extend = function () {
        var args = slice.call(arguments),
            target = args[0] || {},
            i, l, p, source;
        if (typeof target !== "object" && !isFunc(target)) {
            target = {};
        }
        if (target == args[0]) {
            args.shift();
        }
        for (i = 0, l = args.length; i < l; i++) {
            source = args[i];
            if (typeof source !== "object" && !isFunc(target)) {
                source = {};
            }
            for (p in source) {
                if (hasOwnProperty.call(source, p)) {
                    target[p] = source[p];
                }
            }
        }
        return target;
    },
    /***
     * @module Typy
     * @method keys
     * @summary 引数で指定された値が持っているプロパティ名を配列で取得します。
     * @param target:Object プロパティを取得する値
     * @return String[] プロパティ名の配列
     * @remarks
     * ES5 で定義される Object.keys 関数が存在する場合は、Object.keys が利用されます。
     * @example
     * Typy.keys({prop1: true, prop2: "A"})
     *   -> ["prop1", "prop2"]
     */
    keys = Object.keys || function (target) {
        if (target !== Object(target)) return [];
        var keys = [], key;
        for (key in target) {
            if (hasOwnProperty.call(target, key)) keys.push(key);
        }
        return keys;
    },
    slice = Array.prototype.slice,
    /***
     * @module Typy
     * @method toArray
     * @summary 引数で指定された値をもとに配列を生成します。
     * @param target:Any 配列を生成する値
     * @return Any[] 生成された配列
     * @remarks
     * 引数が配列の場合は、そのまま返します。引数の値に length プロパティがある場合は、Array.slice　関数を利用して配列が生成されます。
     * それ以外の場合は、引数に指定された値が持つ列挙可能なプロパティの値を配列の値として利用します。
     */
    toArray = function (target) {
        var p,
            result = [];
        if (isUnusable(target)) {
            return;
        }
        if (isArray(target)) {
            return target;
        }
        if (isNum(target.length)) {
            return slice.call(target);
        }
        for (p in target) {
            if (hasOwnProperty.call(target, p)) {
                result.push(target[p]);
            }
        }
        return result;
    },
    takeArg = function(args, count){
        var ary = slice.call(args),
            i = 0, l = count, al = ary.length;
        for(;i < l && i < al; i++){
            ary.shift();
        }
        return ary;
    },
    extcache = [],

    extDefine = function (initializer, funcs, isTarget, unfindable) {
        var constractor,
            extender,
            funckeys, i, l;
        if (!isFunc(initializer)) {
            return;
        }
        constractor = function (target) {
            var self = this;
            for(var p in self){
                if(isFunc(self[p])){
                    (function(p){
                        self[p].x = function(){
                            var r = self[p].apply(self, arguments);
                            if(r instanceof constractor){
                                return r;
                            }
                            return Typy.ext(r);
                        }
                    })(p);
                }
            }

            if (isFunc(isTarget)) {
                if (isTarget(target)) {
                    this._val = target;
                }
            } else {
                this._val = target;
            }
            this._extender = extender;
        };
        extender = function () {
            var args = slice.call(arguments);
            args.unshift(constractor);
            return initializer.apply(null, args);
        };
        constractor.prototype.val = function () {
            return !isTarget(this._val) ? void 0 : this._val;
        };
        constractor.prototype.to = function(target){
            if(isFunc(target) && target._source === Typy){
                return target(this._val);
            }
        };

        extender._constractor = constractor;
        extender._isTarget = isTarget;
        extender._source = Typy;

        addInstanceFunc(extender, "toString", unchainable(function(){
            return this._val + "";
        }));

        funckeys = keys(funcs);
        for (i = 0, l = funckeys.length; i < l; i++) {
            addFunc(extender, funckeys[i], funcs[funckeys[i]]);
        }
        if(!unfindable){
            extcache.push({
                isTarget: isTarget,
                extender: extender
            });
        }

        return extender;
    },
    hasExt = function(val, includeExtended){
        var i, l, extender;
        if(!includeExtended){
            if(!isUnusable(val) && !isUnusable(val._extender)){
                extender = val._extender;
                if(extender._source === Typy && extender._constractor && extender._isTarget){
                    for(i = 0, l = extcache.length; i < l; i++){
                        if(extcache[i].isTarget === extender._isTarget){
                            return true;
                        }
                    }
                }
            }
        }
        for(i = 0, l = extcache.length; i < l; i++){
            if(extcache[i].isTarget(val)){
                return true;
            }
        }
        return false;
    },
    addStaticFunc = function (extender, name, func) {
        if (isUnusable(extender) || !isStr(name) || !isFunc(func)) {
            return;
        }
        extender[name] = func;
    },
    execInstanceFunc = function(extender, context, func, args){
        var result = func.apply(context, args);
        if (!func._unchainable) {
            if (isFunc(extender._isTarget)) {
                if (extender._isTarget(result)) {
                    context._val = result;
                    return context;
                }
            } else {
                context._val = result;
                return context;
            }
        }
        return result;
    },
    addFallbackFunc = function (extender, name) {
        var instanceFunc;
        if (isUnusable(extender) || !isStr(name)) {
            return;
        }
        if (extender._constractor) {
             instanceFunc = function () {
                var args = slice.call(arguments);
                args.unshift(this._val);
                return execInstanceFunc(extender, this, extender[name], args);
            };

            extender._constractor.prototype[name] = instanceFunc;
        }
    },
    addInstanceFunc = function (extender, name, func) {
        var instanceFunc;
        if (isUnusable(extender) || !isStr(name) || !isFunc(func)) {
            return;
        }
        if (extender._constractor) {
            instanceFunc = function () {
                return execInstanceFunc(extender, this, func, slice.call(arguments));
            };

            extender._constractor.prototype[name] = instanceFunc;
        }
    },
    addFunc = function (extender, name, func) {
        addStaticFunc(extender, name, func);
        addFallbackFunc(extender, name);
    },
    unchainable = function (func) {
        if (isFunc(func)) {
            func._unchainable = true;
        }
        return func;
    },
    noop = function () {
    };

extend(Typy, {
    extend: extend,
    noop: noop,
    isUndef: isUndef,
    isUnusable: isUnusable,
    isObj: isObj,
    isStr: isStr,
    isFunc: isFunc,
    isArray: isArray,
    isNull: isNull,
    isNum: isNum,
    isInt: isInt,
    isBool: isBool,
    isDate: isDate,
    isPrimitive: isPrimitive,
    isNumeric: isNumeric,
    isUndefOrNull: isUndefOrNull,
    isRegx: isRegExp,
    ifUndef: ifUndef,
    ifNull: ifNull,
    ifUnusable: ifUnusable,
    ifUndefOrNull: ifUndefOrNull,
    keys: keys,
    toArray:toArray,
    log:function (message) {
        if (console && console.log) {
            console.log(message);
        }
    }
});

Typy.any = extDefine(function (constractor, target){
    return new constractor(target);
}, {}, function(){
    return true;
}, true);

Typy.ext = function(val){
    var i, l, extender;
    if(!isUnusable(val) && !isUnusable(val._extender)){
        extender = val._extender;
        if(extender._source === Typy && extender._constractor && extender._isTarget){
            for(i = 0, l = extcache.length; i < l; i++){
                if(extcache[i].isTarget === extender._isTarget){
                    return val;
                }
            }
        }
    }
    for(i = 0, l = extcache.length; i < l; i++){
        if(extcache[i].isTarget(val)){
            return extcache[i].extender(val);
        }
    }
    return Typy.any(val);
};
extend(Typy.ext,{
    define:extDefine,
    func:addFunc,
    staticFunc:addStaticFunc,
    instanceFunc:addInstanceFunc,
    unchainable:unchainable,
    has: hasExt
});
/* object */
(function (global, Typy) {

    "use strict";

    Typy.obj = extDefine(function (constractor, target) {
        if(isObj(target)){
            return new constractor(target);
        }
        return new constractor({});
    }, {
        keys: unchainable(function(target){
            return Typy.keys(target);
        }),
        values: unchainable(function(target){
            var p, results = [];
            for(p in target){
                if (hasOwnProperty.call(target, p)){
                    results.push(target[p]);
                }
            }
            return results;
        }),
        pairs: unchainable(function(target){
            var p, results = [];
            for(p in target){
                if (hasOwnProperty.call(target, p)){
                    results.push([p, target[p]]);
                }
            }
            return results;
        }),
        prop: unchainable(function(target, key, val){
            if(arguments.length <= 1){
                return;
            }
            if(!isObj(target)){
                if(arg.length >= 2){
                    return;
                }
                return target;
            }
            if(arg.length === 2){
                return target[key];
            }
            target[key] = val;
            return target;
        }),
        extend: function(target){
            if(!isObj(target)){
                return;
            }
            return extend.apply(null, arguments);
        },
        pick: function(target){
            var result = {},
                props = takeArg(arguments, 1), prop, i, l;
            if(!isObj(target)){
                return;
            }
            for(i = 0, l = props.length; i < l; i++){
                prop = props[i];
                if(prop in target){
                    result[prop] = target[prop]
                }
            }
            return result;
        },
        omit: function(target){
            var result = {},
                props = takeArg(arguments, 1), i, l, keys;
            if(!isObj(target)){
                return;
            }
            keys = Typy.keys(target);
            for(i = 0, l = keys.length; i < l; i++){
                if(props.indexOf(keys[i]) < 0){
                    result[keys[i]] = target[keys[i]]
                }
            }
            return result;
        },
        merge: function(target){
            var val, i, l, p;
            if(!isObj(target)){
                return;
            }
            for(i = 1, l = arguments.length; i < l; i++){
                val = arguments[i];
                for(p in val){
                    if(!(p in target) && hasOwnProperty.call(val, p)){
                        target[p] = val[p];
                    }
                }
            }
            return target;
        }
    }, isObj);

})(this, Typy);

var TypyObj = Typy.obj;
/* Array */

(function (global, Typy) {
    "use strict";

    var proto = Array.prototype,
        insert = function (target, index, values) {
            if (!isArray(target) || !isNum(index)) {
                return;
            }
            if (arguments.length === 2) {
                return target;
            }
            if (arguments.length === 3) {
                if (!isArray(values)) {
                    values = [values];
                }
            }
            if (arguments.length > 3) {
                values = slice.call(arguments);
                values.shift();
                values.shift();
            }
            target.splice.apply(target, [index, 0].concat(values));
            return target;
        },
        push = function (target, values) {
            if (!isArray(target)) {
                return;
            }
            var args = slice.call(arguments);
            args.shift();
            return insert.apply(insert, [target, target.length].concat(args));
        },
        flatten = function (target, deep, result) {
            var i, l;
            if (!isArray(target)) {
                return;
            }
            for (i = 0, l = target.length; i < l; i++) {
                if (isArray(target[i])) {
                    !!deep ? flatten(target[i], deep, result) : proto.push.apply(result, target[i]);
                } else {
                    result.push(target[i]);
                }
            }
            return result;
        },
        findDataWithIndex = function(target, callback, fromIndex){
            var i = 0, l, result = {index: -1};
            target = toArray(target);
            if (!isArray(target)) {
                return;
            }
            if (!isFunc(callback)) {
                result.index = TypyArray.indexOf(target, callback, fromIndex);
                if (result.index !== -1) {
                    result.data = target[result.index];
                    return result;
                }
            } else {
                if (isNumeric(fromIndex)) {
                    i = Math.max(0, parseInt10(fromIndex));
                }
                for (l = target.length; i < l; i++) {
                    if (callback(target[i], i)) {
                        result.index = i;
                        result.data = target[i];
                        return result;
                    }
                }
            }
            return result;
        },
        findLastDataWithIndex =  function (target, callback, fromIndex) {
            var l, result = {index: -1};

            target = toArray(target);
            if (!isArray(target)) {
                return;
            }
            if (!isFunc(callback)) {
                result.index = TypyArray.lastIndexOf(target, callback, fromIndex);
                if (result.index !== -1) {
                    result.data = target[result.index];
                    return result;
                }
            }
            l = target.length;
            if (isNumeric(fromIndex)) {
                l = Math.max(Math.min(parseInt10(fromIndex) + 1, target.length), 0);
            }
            while (l--) {
                if (callback(target[l], l)) {
                    result.index = l;
                    result.data = target[l];
                    return result;
                }
            }
            return result;
        };

    var TypyArray = Typy.array = extDefine(function (constractor, target) {
        if (isArray(target)) {
            //copy
            return new constractor(target.concat());
        }
        return new constractor(toArray(target));
    }, {
        /***
         * @module Typy.array
         * @method indexOf
         * @summary 引数に与えられた内容と同じ内容を持つ配列要素の内、最初のもののインデックスを返します。存在しない場合は -1 を返します。
         * @param target:Any[] 検索する配列。配列でない場合は Typy.toArray を利用して配列化されます。
         * @param item:Any 検索する値
         * @param fromIndex?:Number 検索する開始インデックス
         * @return Number インデックス
         */
        indexOf: unchainable(function (target, item, fromIndex) {
            var i = 0, l;
            target = toArray(target);
            if (!isArray(target)) {
                return -1;
            }
            if (proto.indexOf && target.indexOf === proto.indexOf) {
                return proto.indexOf.apply(target, takeArg(arguments, 1));
            }
            if (isNumeric(fromIndex)) {
                i = Math.max(0, parseInt10(fromIndex));
            }
            for (l = target.length; i < l; i++) {
                if (i in target && target[i] === item) {
                    return i;
                }
            }
            return -1;
        }),
        /***
         * @module Typy.array
         * @method lastIndexOf
         * @summary 引数に与えられた内容と同じ内容を持つ配列要素の内、最後のもののインデックスを返します。存在しない場合は -1 を返します。
         * @param target:Any[] 検索する配列。配列でない場合は Typy.toArray を利用して配列化されます。
         * @param item:Any 検索する値
         * @param fromIndex?:Number 検索する開始インデックス
         * @return Number インデックス
         * @remarks 検索は後方から開始されます。
         */
        lastIndexOf: unchainable(function (target, item, fromIndex) {
            var start = fromIndex, l;
            target = toArray(target);
            if (!isArray(target)) {
                return -1;
            }
            if (proto.lastIndexOf && target.lastIndexOf === proto.lastIndexOf) {
                return proto.lastIndexOf.apply(target, takeArg(arguments, 1));
            }
            if (arguments.length < 3) {
                start = target.length;
            }
            if (isNumeric(start)) {
                l = Math.max(Math.min(parseInt10(start) + 1, target.length), 0);
            }
            while (l--) {
                if (l in target && target[l] === item) {
                    return l;
                }
            }
            return -1;
        }),
        /***
         * @module Typy.array
         * @method find
         * @summary 指定された関数の結果が真を返す最初の要素を返します。
         * @return Any 判定の関数の結果で真が返った最初の要素
         * @param target:Any[] 検索する配列。配列でない場合は Typy.toArray を利用して配列化されます。
         * @param callback:Function(value:Any,index:Number) 判定を実行する関数
         * @param fromIndex?:Number 検索を開始するインデックス
         */
        find: unchainable(function (target, callback, fromIndex) {
            var result = findDataWithIndex(target, callback, fromIndex);
            if(result){
                return result.data;
            }
        }),
        /***
         * @module Typy.array
         * @method findIndex
         * @summary 指定された関数の結果が真を返す最初の要素のインデックスを返します。
         * @return Number 判定の関数の結果で真が返った最初の要素のインデックス
         * @param target:Any[] 検索する配列。配列でない場合は Typy.toArray を利用して配列化されます。
         * @param callback:Function(value:Any,index:Number) 判定を実行する関数
         * @param fromIndex?:Number 検索を開始するインデックス
         */
        findIndex: unchainable(function (target, callback, fromIndex) {
            var result = findDataWithIndex(target, callback, fromIndex);
            if(result){
                return result.index;
            }
        }),
        /***
         * @module Typy.array
         * @method findLast
         * @summary 指定された関数の結果が真を返す最後の要素を返します。
         * @return Any 判定の関数の結果で真が返った最後の要素
         * @param target:Any[] 検索する配列。配列でない場合は Typy.toArray を利用して配列化されます。
         * @param callback:Function(value:Any,index:Number) 判定を実行する関数
         * @param fromIndex?:Number 検索を開始するインデックス
         * @remarks 検索は後方から開始されます。
         */
        findLast: unchainable(function (target, callback, fromIndex) {
            var result = findLastDataWithIndex(target, callback, fromIndex);
            if(result){
                return result.data;
            }
        }),
        /***
         * @module Typy.array
         * @method findLastIndex
         * @summary 指定された関数の結果が真を返す最後の要素のインデックスを返します。
         * @return Any 判定の関数の結果で真が返った最後の要素のインデックス
         * @param target:Any[] 検索する配列。配列でない場合は Typy.toArray を利用して配列化されます。
         * @param callback:Function(value:Any,index:Number) 判定を実行する関数
         * @param fromIndex?:Number 検索を開始するインデックス
         * @remarks 検索は後方から開始されます。
         */
        findLastIndex: unchainable(function (target, callback, fromIndex) {
            var result = findLastDataWithIndex(target, callback, fromIndex);
            if(result){
                return result.index;
            }
        }),
        each: function (target, callback, context) {
            var i, l;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return;
            }
            if (proto.forEach && target.forEach === proto.forEach) {
                proto.forEach.apply(target, takeArg(arguments, 1));
                return target;
            }
            for (i = 0, l = target.length; i < l; i++) {
                if (i in target) {
                    callback.call(context, target[i], i, target);
                }
            }
            return target;
        },
        map: function (target, callback, context) {
            var result = [], i, l;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return [];
            }
            if (proto.map && target.map === proto.map) {
                return proto.map.apply(target, takeArg(arguments, 1));
            }
            for (i = 0, l = target.length; i < l; i++) {
                if (i in target) {
                    result.push(callback.call(context, target[i], i, target));
                }
            }
            return result;
        },
        reduce: unchainable(function (target, callback, initialValue) {
            var value, i, l;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return;
            }
            if (proto.reduce && target.reduce === proto.reduce) {
                return proto.reduce.apply(target, takeArg(arguments, 1));
            }
            i = 0;
            value = initialValue;
            if (arguments.length < 2) {
                i = 1;
                value = target[0];
            }
            for (l = target.length; i < l; i++) {
                value = callback(value, target[i], i, target);
            }
            return value;

        }),
        reduceRight: unchainable(function (target, callback, initialValue) {
            var value, l;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return;
            }
            if (proto.reduceRight && target.reduceRight === proto.reduceRight) {
                return proto.reduceRight.apply(target, takeArg(arguments, 1));
            }
            l = target.length;
            value = initialValue;
            if (arguments.length < 2) {
                l--;
                value = target[l];
            }
            while (l--) {
                value = callback(value, target[l], l, target);
            }
            return value;
        }),
        filter: function (target, callback, context) {
            var result = [], i, l, val;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return [];
            }
            if (proto.filter && proto.filter === target.filter) {
                return proto.filter.apply(target, takeArg(arguments, 1));
            }
            for (i = 0, l = target.length; i < l; i++) {
                if (i in target) {
                    val = target[i];
                    if (callback.call(context, target[i], i, target)) {
                        result.push(val);
                    }
                }
            }
            return result;
        },
        every: function (target, callback, context) {
            var i, l;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return [];
            }
            if (proto.every && proto.every === target.every) {
                return proto.every.apply(target, takeArg(arguments, 1));
            }
            for (i = 0, l = target.length; i < l; i++) {
                if (i in target && !callback.call(context, target[i], i, target)) {
                    return false;
                }
            }
            return true;
        },
        some: function (target, callback, context) {
            var i, l;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return [];
            }
            if (proto.some && proto.some === target.some) {
                return proto.some.apply(target, takeArg(arguments, 1));
            }
            for (i = 0, l = target.length; i < l; i++) {
                if (i in target && callback.call(context, target[i], i, target)) {
                    return true;
                }
            }
            return false;
        },
        contains: function (target, val) {
            target = toArray(target);
            if (!isArray(target)) {
                return;
            }
            return TypyArray.indexOf(target, val) >= 0;
        },
        skip: function (target, count) {
            var result = [], i, l;
            if (!isArray(target) || !isNum(count)) {
                return result;
            }
            i = Math.max(count < 0 ? target.length + count : count, 0);

            for (l = target.length; i < l; i++) {
                result.push(target[i]);
            }
            return result;
        },
        take: function (target, count) {
            var result = [], i;
            if (!isArray(target) || !isNum(count)) {
                return result;
            }
            count = count < 0 ? target.length + count : count;
            if (count < 0) {
                return result;
            }
            for (i = 0; i < Math.min(count, target.length); i++) {
                result.push(target[i]);
            }
            return result;
        },
        first: unchainable(function (target) {
            if (isArray(target)) {
                return target[0];
            }
        }),
        last: unchainable(function (target) {
            if (isArray(target)) {
                return target[target.length - 1];
            }
        }),
        sort: function (target, callback) {
            var copy;
            if (!isArray(target)) {
                return [];
            }
            copy = target.concat();
            copy.sort(callback);
            return copy;
        },
        remove: function (target, index, length) {
            if (!isArray(target) || !isNum(index)) {
                return;
            }
            length = isNum(length) ? length : 1;
            target.splice(index, length);
            return target;
        },
        insert:insert,
        push:push,
        add:push,
        flatten: function (target, deep) {
            return flatten(target, deep, []);
        },
        max: unchainable(function (target) {
            var i, l, lastVal;

            if (!isArray(target)) {
                return;
            }
            lastVal = target[0];
            for (i = 1, l = target.length; i < l; i++) {
                lastVal = Math.max(lastVal, target[i]);
            }
            return lastVal;
        }),
        min: unchainable(function (target) {
            var i, l, lastVal;

            if (!isArray(target)) {
                return;
            }
            lastVal = target[0];
            for (i = 1, l = target.length; i < l; i++) {
                lastVal = Math.min(lastVal, target[i]);
            }
            return lastVal;
        })
    }, isArray);

    addStaticFunc(TypyArray, "make", function (length, data) {
        var args = toArray(arguments),
            i, dataPoint,
            result = [];
        if (isNum(length) && length >= 0) {
            args.shift();
            dataPoint = 0;
            for (i = 0; i < length; i++) {
                result.push(args.length === 0 ? undefined : (isFunc(args[dataPoint]) ? args[dataPoint](i) : args[dataPoint]));
                dataPoint++;
                if (args.length <= dataPoint) {
                    dataPoint = 0;
                }
            }
            return result;
        }
    });

})(this, Typy);

var TypyArray = Typy.array;

/* function */

(function (global, Typy) {
    "use strict";

    Typy.func = extDefine(function (constractor, target) {
        if (isFunc(target)) {
            return new constractor(target);
        }
        return new constractor(Typy.noop());
    }, {
        bind: function(target, context){
            var args;
            if (!isFunc(target)) {
                return;
            }
            if (Function.prototype.bind && target.bind === Function.prototype.bind) {
                return target.bind.apply(target, slice.call(arguments, 1));
            } else {
                args = slice.call(arguments, 2);
                return function () {
                    return target.apply(context, args.concat(slice.call(arguments)));
                };
            }
        }
    }, isFunc);
    addInstanceFunc(Typy.func, "exec", unchainable(function () {
        return this._val.apply(void 0, arguments);
    }));
})(this, Typy);

var TypyFunc = Typy.func;
/* string */
(function (global, Typy) {

    "use strict";

    var namedFormat = {},
        regexp = RegExp,
        escapeRegExp = function (value){
            if(!isStr(value)) value = value + "";
            return value.replace(/([\\/'*+?|()\[\]{}.^$])/g,'\\$1');
        },
        repeat = function (target, count) {
            if (!isStr(target)) return;
            if (isNumeric(count)) {
                count = parseInt10(count);
            }
            count = isNum(count) ? count < 0 ? 0 : Math.floor(count) : 0;
            if (count === 0) return "";

            return (new Array(count + 1)).join(target);
        },
        startsWith = function (target, value, ignoreCase) {
            if (!isStr(target)) return;
            var source = isRegExp(value) ? value.source : escapeRegExp(value);
            ignoreCase = !isUndef(ignoreCase) ? !!ignoreCase :
                         isRegExp(value) ? value.ignoreCase : false;
            source = (source.charAt(0) === "^" ? "" : "^") + source;
            return regexp(source, ignoreCase ? "i" : "").test(target);
        },
        endsWith = function (target, value, ignoreCase) {
            if (!isStr(target)) return;
            var source = isRegExp(value) ? value.source : escapeRegExp(value);
            ignoreCase = !isUndef(ignoreCase) ? !!ignoreCase :
                isRegExp(value) ? value.ignoreCase : false;
            source = source + (source.charAt(source.length - 1) === "$" ? "" : "$");
            return regexp(source, ignoreCase ? "i" : "").test(target);
        },
        format = function (target) {
            var args = takeArg(arguments, 1);
            if (!target) {
                return target;
            }
            if (args.length === 0) {
                return target;
            }
            if (args.length === 1 && isArray(args[0])) {
                args = args[0];
            }
            return target.toString().replace(/\{?\{(.+?)\}\}?/g, function (match, arg1) {
                var val, splitPos, prop, rootProp, format, formatPos,
                    param = arg1;
                if (match.substr(0, 2) === "{{" && match.substr(match.length - 2) === "}}") {
                    return match.replace("{{", "{").replace("}}", "}");
                }
                formatPos = param.indexOf(":");
                if(formatPos > -1){
                    format = param.substr(formatPos + 1);
                    param = param.substr(0, formatPos);
                }
                splitPos = Math.min(param.indexOf(".") === -1 ? param.length : param.indexOf("."),
                    param.indexOf("[") === -1 ? param.length : param.indexOf("["));
                if (splitPos < param.length) {
                    rootProp = param.substr(0, splitPos);
                    prop = "['" + param.substr(0, splitPos) + "']" + param.substr(splitPos);
                } else {
                    rootProp = param;
                    prop = "['" + param + "']";
                }
                val = (new Function("return arguments[0]" + prop + ";"))(isNumeric(rootProp) ? args : args[0]);
                if(format){
                    if(isStr(val)){
                        if(namedFormat[format] && isFunc(namedFormat[format].format)){
                            val = namedFormat[format].format(val, format);
                        }
                    }else{
                        if(!isFunc(val.format) && hasExt(val)){
                            val = Typy.ext(val);
                        }
                        if(isFunc(val.format)){
                            val = val.format(format) + "";
                        }
                    }
                }
                val = isUndef(val) ? "" : val;
                if (match.substr(0, 2) === "{{") {
                    val = "{" + val;
                }
                if (match.substr(match.length - 2) === "}}") {
                    val = val + "}";
                }
                return val;
            });
        };

    Typy.str = extDefine(function (constractor, target) {
        if (arguments.length < 3) {
            target = isUndef(target) || target == null ? "" : target;
            return new constractor(target.toString());
        }
        return new constractor(format.apply(null, slice.call(arguments)));
    }, {
        format:function () {
            return format.apply(null, slice.call(arguments));
        },
        lower:function (target) {
            if (isStr(target)) {
                return target.toLowerCase();
            }
        },
        upper:function (target) {
            if (isStr(target)) {
                return target.toUpperCase();
            }
        },
        repeat:repeat,
        padLeft:function (target, length, padChars) {
            if (!isStr(target)) return;
            if (!isNum(length) && isNumeric(length)) {
                length = parseInt10(length);
            }
            length = isNum(length) ? length < 0 ? 0 : Math.floor(length) : 0;
            if (isUndef(padChars) || isNull(padChars)) padChars = " ";
            var margin = length - target.length;
            if (margin < 1) return target;
            var paddingChars = repeat(padChars.toString(), margin);
            return paddingChars.substr(0, margin) + target;
        },
        padRight:function (target, length, padChars) {
            if (!isStr(target)) return;
            if (!isNum(length) && isNumeric(length)) {
                length = parseInt10(length);
            }
            length = isNum(length) ? length < 0 ? 0 : Math.floor(length) : 0;
            if (isUndef(padChars) || isNull(padChars)) padChars = " ";
            var margin = length - target.length;
            if (margin < 1) return target;
            var paddingChars = repeat(padChars.toString(), margin);
            return target + paddingChars.substr(0, margin);
        },
        startsWith:startsWith,
        endsWith:endsWith,
        trimLeft:function (target) {
            if (!isStr(target)) return;
            return target.replace(/^[\s　]+/, "");
        },
        trimRight:function (target) {
            if (!isStr(target)) return;
            return target.replace(/[\s　]+$/, "");
        },
        trim:function (target) {
            if (!isStr(target)) return;
            return target.replace(/^[\s　]+|[\s　]+$/g, "");
        },
        contains:function (target, value, ignoreCase) {
            if (!isStr(target)) return;
            var source = isRegExp(value) ? value.source : escapeRegExp(value);
            ignoreCase = !isUndef(ignoreCase) ? !!ignoreCase :
                isRegExp(value) ? value.ignoreCase : false;
            return regexp(source, ignoreCase ? "i" : "").test(target);
        },
        capitalize:function (target) {
            if (!isStr(target)) return;
            return target.charAt(0).toUpperCase() + target.substring(1);
        },
        camelize:function (target) {
            if (!isStr(target)) return;
            return target.replace(/(\-|_|\s)+(.)?/g, function (match, separator, chr) {
                return chr ? chr.toUpperCase() : '';
            });
        },
        reverse:function (target) {
            if (!isStr(target)) return;
            return target.split("").reverse().join("")
        },
        clipRight:function (target, length) {
            if (!isStr(target)) return;
            length = isNum(length) ? length < 0 ? 0 : length : 0;
            if (target.length <= length) return target;
            return target.substring(target.length - length, target.length);
        },
        clipLeft:function (target, length) {
            if (!isStr(target)) return;
            length = isNum(length) ? length < 0 ? 0 : length : 0;
            if (target.length <= length) return target;
            return target.substr(0, length);
        },
        surrounds:function (target, start, end, force) {
            var normalize = function (val) {
                if (isUndef(val) || isNull(val)) {
                    val = "";
                }
                return val.toString();
            };
            if (!isStr(target)) return;
            start = normalize(start);
            if (arguments.length === 4) {
                end = normalize(end);
                force = !!force;
            } else if (arguments.length === 3) {
                if (isBool(end)) {
                    force = !!end;
                    end = start;
                } else {
                    force = false;
                    end = normalize(end);
                }
            } else if (arguments.length <= 2) {
                end = start;
                force = false;
            }
            if (startsWith(target, start) && !force) {
                start = "";
            }
            if (endsWith(target, end) && !force) {
                end = "";
            }
            return start + target + end;
        },
        surroundsWith:function (target, start, end, ignoreCase) {
            if (!isStr(target)) return;
            start = ifUndefOrNull(start, "");
            if(arguments.length === 3 && isBool(end)){
                ignoreCase = end;
                end = void 0;
            }
            end = ifUndefOrNull(end, start);
            return startsWith(target, start, ignoreCase) && endsWith(target, end, ignoreCase);
        },
        /***
         * @module Typy.str
         * @method split
         * @summary
         * 文字列を指定した区切り文字をもとに複数の部分文字列に区切ることにより、String オブジェクトを文字列の配列に分割します。
         * @param target:String 対象となる文字列
         * @param separator:String 区切り文字
         * @param limit?:Number 見つかった分割結果の数の制限の指定
         * @return String[]
         * 分割された文字列の新しい配列
         */
        /***
         * @module Typy.str
         * @override split
         * @summary
         * 文字列を指定した正規表現に一致する文字列をもとに複数の部分文字列に区切ることにより、String オブジェクトを文字列の配列に分割します。
         * @param target:String 対象となる文字列
         * @param separator:RegExp 区切り正規表現
         * @param limit?:Number 見つかった分割結果の数の制限の指定
         * @return String[]
         * 分割された文字列の新しい配列
         */
        split: function(target, separator, limit){
            if (!isStr(target)) return;
            if(!limit){
                limit = -1;
            }
            return target.split(separator, limit);
        },
        /***
         * @module Typy.str
         * @method replace
         * @summary 指定された値を指定された値で置き換えます。
         */
        replace: function(target, value, replaceValue){
            if (!isStr(target)) return;
            return target.replace(value, replaceValue);
        }
    }, isStr);

    addStaticFunc(Typy.str, "namedFormat", function(name){
        if(!isStr(name)) return;
        if(arguments.length === 1){
            return namedFormat[name];
        }
        if(arguments.length >= 2){
            namedFormat[name] = arguments[1];
        }
    });

})(this, Typy);

var TypyStr = Typy.str;
/* lang */
(function (global, Typy) {

    var langs = {},
        currentLang,
        defaultLangDef = {
            text:{

            },
            numeric:{

            },
            calendar:{
                months:{
                    shortNames:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),
                    names:"January February March April May June July August September October November December".split(" ")
                },
                meridiem:{
                    shortNames:{ ante:"A", post:"P" },
                    names:{ ante:"AM", post:"PM" }
                },
                weekdays:{
                    shortNames:"Sun Mon Tue Wed Thu Fri Sat".split(" "),
                    names:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" ")
                },
                defaultFormats:["yyyy/M/d", "M/d/yyyy"]
            }
        },
        mixin = function () {
            var options, name, src, copy, copyIsArray, clone,
                target = arguments[0] || {},
                i = 1,
                length = arguments.length;

            if (typeof target !== "object" && !isFunc(target)) {
                target = {};
            }

            for (; i < length; i++) {
                if ((options = arguments[ i ]) != null) {
                    for (name in options) {
                        src = target[ name ];
                        copy = options[ name ];
                        if (target === copy) {
                            continue;
                        }
                        if (copy && ( isObj(copy) || (copyIsArray = isArray(copy)) )) {
                            if (copyIsArray) {
                                copyIsArray = false;
                                clone = src && isArray(src) ? src : [];
                            } else {
                                clone = src && isObj(src) ? src : {};
                            }
                            target[ name ] = mixin(clone, copy);
                        } else if (copy !== undefined) {
                            target[ name ] = copy;
                        }
                    }
                }
            }
            return target;
        }, i, l, props = ["calendar", "numeric", "text"];

    langs["en"] = mixin({}, defaultLangDef);
    currentLang = "en";

    var TypyLang = Typy.lang = function () {
        var targetLang,
            targetLangDef,
            langDef,
            newLangDef;
        if (arguments.length === 0) {
            return langs[currentLang];
        }
        if (arguments.length === 1) {
            return langs[arguments[0]];
        } else if (arguments.length >= 2) {
            targetLang = arguments[0];
            targetLangDef = arguments[1] || {};
            langDef = langs[targetLang] || {};
            newLangDef = mixin({}, defaultLangDef, targetLangDef, langDef);
            langs[arguments[0]] = newLangDef;
        }
    };
    Typy.lang.current = function () {
        if (arguments.length === 0) {
            return currentLang;
        }
        if (arguments.length >= 1) {
            currentLang = arguments[0];
            return TypyLang;
        }
    };

    for(i = 0, l = props.length; i < l; i++){
        (function(p){
            Typy.lang[p] = function(){
                var def, arg = arguments;
                if (arg.length === 0) {
                    return langs[currentLang] ? langs[currentLang][p] : undefined
                }
                if (arg.length > 0) {
                    def = langs[currentLang] || mixin({}, defaultLangDef);
                    def[p] = def[p] || {};
                    def[p] = mixin(def[p], arg[0]);
                    langs[currentLang] = def;
                    return def;
                }
            }
        }).call(null, props[i]);
    }


})(this, Typy);

var TypyLang = Typy.lang;
/* Date */

// dependon array.js, lang.js, string.js
(function (global, Typy) {

    "use strict";
    var proto = Date.prototype,
        getFullYear = proto.getFullYear,
        getMonth = proto.getMonth,
        getDate = proto.getDate,
        getHours = proto.getHours,
        getMinutes = proto.getMinutes,
        getSeconds = proto.getSeconds,
        getMilliseconds = proto.getMilliseconds,
        getTime = proto.getTime,

        setFullYear = proto.setFullYear,
        setMonth = proto.setMonth,
        setDate = proto.setDate,
        setHours = proto.setHours,
        setMinutes = proto.setMinutes,
        setSeconds = proto.setSeconds,
        setMilliseconds = proto.setMilliseconds,
        setTime = proto.setTime,
        unitYear = "Year", unitMonth = "Month", unitDate = "Date",
        unitHour = "Hours", unitMinute = "Minutes", unitSecond = "Seconds", unitMillSecond = "Milliseconds",
        unitWeek = "Week", unitTime = "Time",
        cachedFormatTokens = {},
        namedFormat = {
            "ODataJSON":{
                parse:function (val, lang, langDef) {
                    var match = /^\/Date\((-?\d+)(\+|-)?(\d+)?\)\/$/i.exec(val),
                        result, offset, curMin;
                    if (match) {
                        result = new Date(+match[1]);
                        if(match[3]){
                            offset = (+match[3]);
                            if(match[2] === "-"){
                                offset = -offset;
                            }
                            curMin = result.getUTCMinutes();
                            result.setUTCMinutes(curMin - offset);
                        }
                        return result;
                    }
                },
                format:function (val, lang, langDef) {
                    return "/Date(" + getTime.call(val) + ")/";
                }
            }
        },
        tokenize = function (format) {
            var i, l, ch,
                tokens = [],
                escaped = false,
                token, quote,
                tokenizeLiteral = function (target, index) {
                    var match = format.substr(index).match(new RegExp(target + "+"));
                    if (match) {
                        return match[0].length;
                    }
                    return 1;
                };
            if (cachedFormatTokens[format]) {
                return cachedFormatTokens[format];
            }
            for (i = 0, l = format.length; i < l; i++) {

                ch = format.charAt(i);
                if(escaped){
                    tokens.push(ch);
                    escaped = false;
                    continue;
                }
                if(ch === "\\"){
                    escaped = true;
                    continue;
                }
                if(ch === "'" || ch === "\""){
                    if(ch === quote){
                        quote = false;
                    }else{
                        quote = ch;
                    }
                    continue;
                }
                if(quote){
                    tokens.push(ch);
                    continue;
                }
                switch (ch) {
                    case "d":
                    case "f":
                    case "h":
                    case "H":
                    case "m":
                    case "M":
                    case "s":
                    case "t":
                    case "y":
                    case "z":
                        token = {
                            type:ch,
                            length:tokenizeLiteral(ch, i)
                        };
                        tokens.push(token);
                        i += (token.length - 1);
                        break;
                    case "/":
                    case ":":
                        token = {
                            type:ch
                        };
                        tokens.push(token);
                        break;
                    default:
                        tokens.push(ch);
                }
            }
            cachedFormatTokens[format] = tokens;
            return tokens;
        },
        resolveTokenForParse = function (token, startPos, value, dateDef, lang, langDef) {
            var meridiemVal, target, result, i, l,
                names, sign,
                tokenType = token.type,
                tokenLength = token.length,
                zeroPaddingTwoLengthResolveToken = function (valPos) {
                    var result, text, num, code;

                    code = value.charCodeAt(startPos);
                    if (48 <= code && code <= 57) { //0-9
                        text = value.charAt(startPos);
                        result = 1;
                        code = value.charCodeAt(startPos + 1);
                        if (48 <= code && code <= 57) { //0-9
                            text += value.charAt(startPos + 1);
                            result = 2;
                        }
                        num = parseInt10(text);
                        if (!isNaN(num)) {
                            dateDef.values[valPos] = num;
                            return result;
                        }
                    }
                },
                fixLengthResolveToken = function (valPos) {
                    var match = value.substr(startPos).match(new RegExp("^\\d{" + tokenLength + "}"));
                    if(match && match.length > 0){
                        dateDef.values[valPos] = parseInt10(match[0]);
                        return token.length;
                    }
                },
                millisecondsResolveToken = function () {
                    var text = "0." + value.substr(startPos, tokenLength);
                    if (isNumeric(text)) {
                        dateDef.values[6] = parseFloat(text) * 1000;
                        return token.length;
                    }
                },
                makeMd = function (index, key, matchName) {
                    if (tokenLength === 1) {
                        return zeroPaddingTwoLengthResolveToken(index);
                    } else if (tokenLength === 2) {
                        return fixLengthResolveToken(index);
                    } else {
                        names = tokenLength === 3 ? langDef[key].shortNames : langDef[key].names;
                        for (i = 0, l = names.length; i < l; i++) {
                            target = value.substr(startPos, names[i].length).toUpperCase();
                            if (target === names[i].toUpperCase()) {
                                matchName(i);
                                return names[i].length;
                            }
                        }
                    }
                },
                makeHhms = function (index) {
                    if (tokenLength === 1) {
                        return zeroPaddingTwoLengthResolveToken(index);
                    } else {
                        return fixLengthResolveToken(index);
                    }
                };

            if (tokenType === "y") {
                if (tokenLength === 1 || tokenLength === 2) {
                    if (tokenLength === 1) {
                        result = zeroPaddingTwoLengthResolveToken(0);
                    } else {
                        result = fixLengthResolveToken(0);
                    }
                    if (result) {
                        //30以下だったら2000年代、30以上だったら1900年代
                        dateDef.values[0] += (dateDef.values[0] < 30 ? 2000 : 1900);
                    }
                    return result;
                } else {
                    return fixLengthResolveToken(0);
                }
            }
            if (tokenType === "M") {
                return makeMd(1, "months", function (index) {
                    dateDef.values[1] = index + 1;
                });
            }
            if (tokenType === "d") {
                return makeMd(2, "weekdays", function (index) {
                    dateDef.weekday = index;
                });
            }
            if (tokenType === "h") {
                dateDef.militaryTime = false;
                return makeHhms(3);
            }

            if (tokenType === "H") {
                dateDef.militaryTime = true;
                return makeHhms(3);
            }

            if (tokenType === "m") {
                return makeHhms(4);
            }

            if (tokenType === "s") {
                return makeHhms(5);
            }

            if (tokenType === "f") {
                return millisecondsResolveToken();
            }

            if (tokenType === "t") {
                meridiemVal = tokenLength === 1 ? langDef.meridiem.shortNames : langDef.meridiem.names;
                target = value.substr(startPos, meridiemVal.ante.length).toUpperCase();
                if (target === meridiemVal.ante.toUpperCase()) {
                    dateDef.anteMeridiem = true;
                    return meridiemVal.ante.length;
                }
                target = value.substr(startPos, meridiemVal.post.length).toUpperCase();
                if (target === meridiemVal.post.toUpperCase()) {
                    dateDef.anteMeridiem = false;
                    return meridiemVal.post.length;
                }
            }

            if (tokenType === "z") {
                target = value.substr(startPos, 1);
                if (target === "+") {
                    sign = 1;
                } else if (target === "-") {
                    sign = -1;
                } else {
                    return;
                }

                if (tokenLength <= 2) {
                    target = value.substr(startPos + 1, 2).match(tokenLength === 1 ? /\d{1,2}/ : /\d{2}/);
                    if (target) {
                        dateDef.timeoffset = parseInt10(target[0]) * 60 * sign;
                        return target[0].length + 1;
                    }
                } else {
                    target = value.substr(startPos + 1, 5).match(/(\d{1,2}):(\d{2})/);
                    if (!target) {
                        target = value.substr(startPos + 1, 4).match(/(\d{2})(\d{2})/);
                    }
                    if (target && parseInt10(target[2]) < 60) {
                        dateDef.timeoffset = ((parseInt10(target[1]) * 60) + parseInt10(target[2])) * sign;
                        return target[0].length + 1
                    }
                }
            }
        },
        makeDateFromArray = function (array, militaryTime, anteMeridiem, weekday, timeoffset) {
            var now = new Date(),
                hour = array[3];
            //全て数値じゃないとNG
            if (!TypyArray.every(array, function (item) {
                return !isUnusable(item) && isNum(item);
            })) {
                return;
            }
            //全て0未満だとNG
            if (!TypyArray.some(array, function (item) {
                return isNum(item) && item > -1;
            })) {
                return;
            }

            //AM/PMが付与されてる場合で13時以上や時間未設定はNG
            if (!isUndef(anteMeridiem)) {
                if (hour > 12 || hour < 0) {
                    return;
                }
            }

            //12時間制で
            if (!isUndef(militaryTime) && !militaryTime && hour > -1) {
                //13時とかはNG
                if (hour > 12) {
                    return;
                }
                //AMの時の12時は0時
                if ((isUndef(anteMeridiem) || (!isUndef(anteMeridiem) && anteMeridiem)) && hour === 12) {
                    array[3] = hour = 0;
                }
                //PMの時の0-11時は+12時間
                if (!isUndef(anteMeridiem) && !anteMeridiem && hour < 12) {
                    array[3] = hour = (hour + 12);
                }
            }

            //24時間制で
            if (!isUndef(militaryTime) && militaryTime && hour > -1) {
                //午前が指定されている場合に12時以上はNG
                if (!isUndef(anteMeridiem) && anteMeridiem) {
                    if (hour >= 12) {
                        return;
                    }
                }
                //午後が指定されている場合に11時以前はNG
                if (!isUndef(anteMeridiem) && !anteMeridiem) {
                    if (hour < 12) {
                        return;
                    }
                }
            }
            //時間しか指定されてない場合は現在日付
            if (array[0] < 0 && array[1] < 0 && array[2] < 0) {
                if (array[0] < 0) array[0] = getFullYear.call(now);
                if (array[1] < 0) array[1] = getMonth.call(now);
                if (array[2] < 0) array[2] = getDate.call(now);
            } else {
                if (array[0] < 0) array[0] = getFullYear.call(now);
                if (array[1] < 0) array[1] = 0;
                if (array[2] < 0) array[2] = 1;
            }

            if (array[3] < 0) array[3] = 0;
            if (array[4] < 0) array[4] = 0;
            if (array[5] < 0) array[5] = 0;
            if (array[6] < 0) array[6] = 0;

            var result = new Date();

            setFullYear.call(result, array[0], array[1], array[2]);
            setHours.call(result, array[3], array[4], array[5], array[6]);

            if (getFullYear.call(result) === array[0] &&
                getMonth.call(result) === array[1] &&
                getDate.call(result) === array[2] &&
                getHours.call(result) === array[3] &&
                getMinutes.call(result) === array[4] &&
                getSeconds.call(result) === array[5] &&
                getMilliseconds.call(result) === array[6]) {

                //曜日が指定されていて日付と一致していない場合はNG
                if (!isUndef(weekday)) {
                    if (result.getDay() !== weekday) {
                        return;
                    }
                }

                //timezoneの時刻調整
                if (!isUndef(timeoffset)) {
                    setMinutes.call(result, getMinutes.call(result) + 0 - timeoffset - (new Date()).getTimezoneOffset());
                }

                return result;
            }
        },
        makeDateFromStringAndFormat = function (value, format) {
            var langDef = TypyLang.calendar(),
                lang = TypyLang.current(),
                dateDef = {
                    values:[-1, -1, -1, -1, -1, -1, -1]
                },
                named = namedFormat[format],
                formatTokens, token, i, l,
                lastValueEndPos = 0,
                resolveLength = 0;

            if (named && isFunc(named.parse)) {
                return named.parse(value, lang, langDef);
            }

            formatTokens = tokenize(format);
            for (i = 0, l = formatTokens.length; i < l; i++) {
                resolveLength = undefined;
                token = formatTokens[i];
                if (isStr(token)) {
                    if (value.substr(lastValueEndPos, token.length) === token) {
                        resolveLength = token.length;
                    }
                } else {
                    //TODO 多言語から取得してチェック
                    if (token.type === "/" || token.type === ":") {
                        if (value.substr(lastValueEndPos, 1) === token.type) {
                            resolveLength = 1;
                        }
                    } else {
                        resolveLength = resolveTokenForParse(formatTokens[i], lastValueEndPos, value, dateDef, lang, langDef);
                    }
                }

                if (isUndef(resolveLength)) {
                    dateDef = null;
                    break;
                }
                lastValueEndPos += resolveLength;
            }
            if (!dateDef) {
                return;
            }
            if (dateDef.values[1] === 0) {
                return;
            } else if (dateDef.values[1] >= 1) {
                dateDef.values[1] -= 1;
            }

            return makeDateFromArray(dateDef.values, dateDef.militaryTime, dateDef.anteMeridiem, dateDef.weekday, dateDef.timeoffset);
        },
        makeDateFromStringAndDefaultFormats = function (value) {
            var formats = TypyLang.calendar().defaultFormats,
                i = 0,
                length = (isArray(formats) ? formats.length : 0),
                result;
            for (; i < length; i++) {
                result = makeDateFromStringAndFormat(value, formats[i]);
                if (result) {
                    break;
                }
            }
            return result;
        },
        makeTextByFormat = function (source, format) {
            if (!isStr(format)) {
                return;
            }
            var named = namedFormat[format];
            if (named && isFunc(named.format)) {
                return named.format(source, TypyLang.current(), TypyLang.calendar());
            }
            return makeTextByFormatTokens(source, tokenize(format), TypyLang.current(), TypyLang.calendar());
        },
        makeTextByFormatTokens = function (source, tokens, lang, langDef) {
            var tokenItem,
                type,
                tlength,
                meridiem,
                timezoneoffset,
                texts = [],
                clipRight = TypyStr.clipRight,
                i, l,
                getNonMilitaryHour = function () {
                    var hour = source.getHours();
                    return hour > 12 ? hour - 12 : hour;
                },
                makeMd = function (l, lang, addend, func, func2) {
                    if (l === 1) {
                        texts.push(func.call(source) + addend);
                    }
                    if (l === 2) {
                        texts.push(clipRight("0" + (func.call(source) + addend), 2));
                    }
                    if (l >= 3) {
                        texts.push((tlength === 3 ? langDef[lang].shortNames : langDef[lang].names)[(func2 || func).call(source)]);
                    }
                },
                makeHhms = function (l, func) {
                    if (l === 1) {
                        texts.push(func(source));
                    }
                    if (l >= 2) {
                        texts.push(clipRight("0" + func(source), 2));
                    }
                };

            for (i = 0, l = tokens.length; i < l; i++) {
                tokenItem = tokens[i];
                if (isStr(tokenItem)) {
                    texts.push(tokenItem);
                    continue;
                }
                type = tokenItem.type;
                tlength = tokenItem.length;
                if (type === "/" || type === ":") {
                    //TODO: 多言語から取得
                    texts.push(type);
                } else if (type === "y") {
                    if (tlength === 1) {
                        texts.push(clipRight(parseInt10(("" + getFullYear.call(source))).toString(), 2));
                    } else {
                        texts.push(clipRight(Array(tlength + 1).join("0") + getFullYear.call(source), tlength));
                    }
                } else if (type === "M") {
                    makeMd(tlength, "months", 1, getMonth);
                } else if (type === "d") {
                    makeMd(tlength, "weekdays", 0, getDate, proto.getDay);
                } else if (type === "h") {
                    makeHhms(tlength, getNonMilitaryHour);
                } else if (type === "H") {
                    makeHhms(tlength, function (s) {
                        return getHours.call(s);
                    });
                } else if (type === "m") {
                    makeHhms(tlength, function (s) {
                        return getMinutes.call(s);
                    });
                } else if (type === "s") {
                    makeHhms(tlength, function (s) {
                        return getSeconds.call(s);
                    });
                } else if (type === "f") {
                    texts.push((getMilliseconds.call(source) + "000").substr(0, tlength));
                } else if (type === "t") {
                    meridiem = tlength === 1 ? langDef.meridiem.shortNames : langDef.meridiem.names;
                    texts.push(getHours.call(source) > 12 ? meridiem.post : meridiem.ante);
                } else if (type === "z") {
                    timezoneoffset = source.getTimezoneOffset();
                    texts.push((timezoneoffset < 0 ? "+" : "-") + (function () {
                        if (tlength === 1) {
                            return Math.floor(Math.abs(timezoneoffset / 60)).toString();
                        } else if (tlength === 2) {
                            return clipRight("0" + Math.floor(Math.abs(timezoneoffset / 60)).toString(), 2);
                        } else {
                            return clipRight("0" + Math.floor(Math.abs(timezoneoffset / 60)).toString(), 2) +
                                ":" +
                                clipRight("0" + Math.floor(Math.abs(timezoneoffset % 60)).toString(), 2);
                        }
                    })());
                }
            }
            return texts.join("");
        },
        add = function (target, unit, value) {
            if (isDate(target) && isStr(unit) && isNumeric(value)) {
                var firstDayInMonth = new Date(getFullYear.call(target), getMonth.call(target), 1),
                    addend = parseInt10(value);
                if (unit === unitYear) {
                    unit = "FullYear";
                }
                if (unit === unitWeek) {
                    setDate.call(target, getDate.call(target) + (addend * 7));
                } else {
                    target["set" + unit](target["get" + unit]() + addend);
                    if (unit === unitMonth) {
                        setMonth.call(firstDayInMonth, getMonth.call(firstDayInMonth) + addend);
                        if (getMonth.call(firstDayInMonth) !== getMonth.call(target)) {
                            setDate.call(target, 0);
                        }
                    }
                }
            }
            return target;
        },
        dayOf = function (target, unit, start) {
            if (isDate(target) && isStr(unit)) {
                var newYear,
                    diff;
                if (unit === unitYear) {
                    newYear = new Date(getFullYear.call(target), 0, 1);
                    return Math.floor((getTime.call(target) - getTime.call(newYear)) / (24 * 60 * 60 * 1000)) + 1;
                }
                if (unit === unitMonth) {
                    return getDate.call(target);
                }
                if (unit === unitWeek) {
                    start = isNumeric(start) ?
                        Math.max(0, Math.min(parseInt(start, 10), 7)) : 0;
                    diff = target.getDay() - start;
                    return diff < 0 ? 7 + diff :
                        diff > 6 ? diff - 6 :
                            diff;
                }
            }
        },
        startOf = function (target, unit, start) {
            if (isDate(target) && isStr(unit)) {
                var dayOfWeek = 0,
                    diff;
                if (unit === unitWeek) {
                    dayOfWeek = dayOf(target, unitWeek, 0);
                    start = isNumeric(start) ? Math.max(0, Math.min(parseInt10(start), 7)) : 0;

                    diff = 0 - dayOfWeek + start;
                    setDate.call(target, getDate.call(target) + (diff > 0 ? diff - 7 : diff));
                    unit = unitDate;
                }
                switch (unit) {
                    case unitYear:
                        setMonth.call(target, 0);
                    case unitMonth:
                        setDate.call(target, 1);
                    case unitDate:
                        setHours.call(target, 0);
                    case unitHour:
                        setMinutes.call(target, 0);
                    case unitMinute:
                        setSeconds.call(target, 0);
                    case unitSecond:
                        setMilliseconds.call(target, 0);
                }
            }
            return target;
        },
        endOf = function (target, unit, start) {
            if (isDate(target) && isStr(unit)) {
                startOf(target, unit, start);
                if (unit === unitWeek) {
                    setDate.call(target, getDate.call(target) + 7);
                } else {
                    add(target, unit, 1);
                }
                setMilliseconds.call(target, -1);
            }
            return target;
        };

    var TypyDate = Typy.date = extDefine(function (constractor, target) {
        var args = slice.call(arguments),
            now = new Date(),
            arrayDate;
        if (isUndef(target)) {
            return new constractor(now);
        }
        if (isUnusable(target)) {
            return new constractor();
        }
        if (isDate(target) || isNum(target)) {
            //clone
            return new constractor(new Date(+target));
        }
        if (isStr(target)) {
            if (args.length > 2 && isStr(args[2])) {
                return new constractor(makeDateFromStringAndFormat(target, args[2]));
            } else {
                return new constractor(makeDateFromStringAndDefaultFormats(target));
            }
        }
        if (isArray(target)) {
            arrayDate = target.slice(0);
            if (arrayDate.length < 1) arrayDate[0] = now.getFullYear();
            if (arrayDate.length < 2) arrayDate[1] = now.getMonth();
            if (arrayDate.length < 3) arrayDate[2] = 1;
            if (arrayDate.length < 4) arrayDate[3] = 0;
            if (arrayDate.length < 5) arrayDate[4] = 0;
            if (arrayDate.length < 6) arrayDate[5] = 0;
            if (arrayDate.length < 7) arrayDate[6] = 0;

            return new constractor(makeDateFromArray.call(null, arrayDate));
        }
    }, {
        format: unchainable(function(target, format) {
            if (isDate(target)) {
                return makeTextByFormat(target, format ? format : TypyDate.defaultFormats[0], TypyLang.current());
            }
        }),
        isLeapYear: unchainable(function (target) {
            if (isDate(target)) {
                var year = target.getFullYear();
                return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
            }
        }),
        diff: unchainable(function(target, dest){
            if(!isDate(target) || isUndef(dest)){
                return;
            }
            if(!isDate(dest)){
                dest = TypyDate.apply(null, takeArg(arguments, 1)).val();
            }
            if(!isDate(dest)){
                return;
            }
            return target - dest;
        })
    }, isDate);

    addStaticFunc(TypyDate, "parse", function (target, format) {
        if (isStr(target)) {
            if (isStr(format)) {
                return makeDateFromStringAndFormat(target, format, TypyLang.current());
            } else {
                return makeDateFromStringAndDefaultFormats(target, TypyLang.current());
            }
        }
    });
    //unit
    TypyArray.each([unitYear, unitMonth, unitDate, unitHour, unitMinute, unitSecond, unitMillSecond, unitTime], function (item) {
        addFunc(TypyDate, item.toLowerCase(), function (target, value) {
            if (isDate(target)) {
                if (isNumeric(value)) {
                    target["set" + (item === unitYear ? "Full" : "") + item](parseInt10(value));
                    return target;
                } else {
                    return target["get" + (item === unitYear ? "Full" : "") + item]();
                }
            }
        });
    });
    //add
    TypyArray.each([unitYear, unitMonth, unitDate, unitHour, unitMinute, unitSecond, unitMillSecond, unitWeek, unitTime], function (item) {
        var name = item.charAt(item.length - 1) !== "s" ? (item + "s") : item;
        name = item === unitDate ? "Days" : name;
        addFunc(TypyDate, "add" + name, function (target, value) {
            return add(target, item, value);
        });
    });
    //dayOf
    TypyArray.each([unitYear, unitMonth, unitWeek], function (item) {
        addFunc(TypyDate, "dayOf" + item, function (target, start) {
            return dayOf(target, item, start);
        });
    });
    //startOf - endOf
    TypyArray.each([unitYear, unitMonth, unitDate, unitHour, unitMinute, unitSecond, unitWeek], function (item) {
        var name = item.charAt(item.length - 1) === "s" ? item.substr(0, item.length - 1) : item;
        name = item === unitDate ? "Day" : name;
        addFunc(TypyDate, "startOf" + name, function (target, start) {
            return startOf(target, item, start);
        });
        addFunc(TypyDate, "endOf" + name, function (target, start) {
            return endOf(target, item, start);
        });
    });
    addStaticFunc(TypyDate, "namedFormat", function(name){
        if(!isStr(name)) return;
        if(arguments.length === 1){
            return namedFormat[name];
        }
        if(arguments.length >= 2){
            namedFormat[name] = arguments[1];
        }
    });
})(this, Typy);

var TypyDate = Typy.date;
/* number */

// dependon array.js
(function (global, Typy) {

    "use strict";

    var math = Math,
        namedFormat = {},
        tokenize = function (format) {
            var i = 0, l = format.length, c,
                escaped = false,
                quote = false,
                result = {
                    plus: {
                        ints: []
                    }
                },
                current = result.plus.ints,
                section = result.plus,
                sectionPos = 0,
                sectionName,
                isInts = true,
                prepareSeparator = function (section) {
                    var i = 0,
                        token,
                        holder = false,
                        separator = false,
                        ints = section.ints;
                    while ((function () {
                        if (ints.length && ints[ints.length - 1].type === ",") {
                            section.pows = (section.pows || 0) + 1;
                            ints.pop();
                            return true;
                        }
                        return false;
                    })()) { }
                    while (i < ints.length) {
                        token = ints[i];
                        if (!isStr(token)) {
                            if (token.holder) {
                                if (holder && separator) {
                                    section.separate = true;
                                }
                                holder = true;
                            } else if (token.separator) {
                                separator = true;
                                ints.splice(i, 1);
                                i--;
                            } else {
                                holder = false;
                                separator = false;
                            }
                        }
                        i++;
                    }

                };
            for (; i < l; i++) {

                c = format.charAt(i);
                if (escaped) {
                    current.push(c);
                    escaped = false;
                } else if (c === "\\") {
                    escaped = true;
                } else if (c === "\"" || c === "'") {
                    if (quote === c) {
                        quote = false;
                    }  else {
                        quote = c;
                    }
                } else if(quote){
                    current.push(c);
                } else if (c === "0" || c === "#") {
                    current.push({
                        type: c,
                        holder: true
                    });
                } else if (c === ",") {
                    if (isInts) {
                        current.push({
                            type: c,
                            separator: true
                        });
                    }
                } else if (c === "%" || c === "‰") {
                    current.push(c);
                    if (c === "%") {
                        section.parcent = (section.parcent || 0) + 1;
                    } else if (c === "‰") {
                        section.permil = (section.permil || 0) + 1;
                    }
                } else if (c === ".") {
                    if (isInts) {
                        isInts = false;
                        current = [];
                        result[(sectionPos === 0) ? "plus" : (sectionPos === 1 ? "minus" : "zero")].decs = current;
                    }
                } else if (c === ";") {
                    prepareSeparator(section);
                    isInts = true;
                    if (sectionPos < 2) {
                        sectionName = sectionPos === 0 ? "minus" : "zero";
                        result[sectionName] = { ints: [] };
                        section = result[sectionName];
                        current = section.ints;
                        sectionPos++;
                    } else {
                        break;
                    }
                } else {
                    current.push(c);
                }
            }

            prepareSeparator(section);

            return result;
        },
        exponentToNumStr = function (val) {
            var ePos = val.indexOf("e");
            if (ePos < 0) {
                return val;
            }
            var mantissa = val.substr(0, ePos);
            var exponent = parseInt(val.substr(ePos + 1), 10);
            var isMinus = val.charAt(0) === "-";
            if (isMinus) {
                mantissa = mantissa(1);
            }
            var dotPos = mantissa.indexOf(".");
            mantissa = mantissa.replace(".", "");
            if (dotPos < 0) {
                dotPos = mantissa.length;
            }
            dotPos = dotPos + exponent;
            if (dotPos <= 0) {
                return "0." + (new Array(Math.abs(dotPos))).join("0") + mantissa;
            } else if (mantissa.length <= dotPos) {
                return mantissa + (new Array(dotPos - mantissa.length + 1)).join("0")
            }
            return mantissa.substr(0, dotPos) + "." + mantissa.substr(dotPos);

        },
        resolveNamedFormat = function(target, format){
            var f = namedFormat[format];
            if(!f || !isFunc(f.format)){
                return false;
            }
            return f.format(target, format) || "";
        },
        round = function(target, prec, func){
            func = func || math.round;
            prec = prec || 0;
            var mul = math.pow(10, math.abs(prec));
            if(prec < 0) mul = 1 / mul;
            return func(target * mul) / mul;
        };

    var TypyNum = Typy.num = extDefine(function (constractor, target) {
        if (isNum(target)) {
            return new constractor(target);
        }
        if (isNumeric(target)) {
            return new constractor(parseFloat(target));
        }
        if(isBool(target)){
            return new constractor(target ? 1 : 0);
        }
        return new constractor(0);
    }, {
        format:unchainable(function (target, format) {
            var namedFormatVal = resolveNamedFormat(target, format);
            if(isStr(namedFormatVal)){
                return namedFormatVal;
            }
            var tokens = tokenize(format),
                isMinus = target < 0,
                targetVal = target,
                targetStr,
                hasToken = function(v){
                    if(!v){
                        return false;
                    }
                    if(v.ints && v.ints.length){
                        return v;
                    }
                    return false;
                },
                targetTokens = isMinus ? (hasToken(tokens.minus) || tokens.plus) : tokens.plus,
                hasDecPlaceholder = function (v) {
                    var i, l;
                    if (!v) {
                        return false;
                    }
                    for (i = 0, l = v.length; i < l; i++) {
                        if (v[i].holder) {
                            return true;
                        }
                    }
                    return false;
                },
                splitPart = function (v) {
                    var splited = v.split(".");
                    if (splited.length === 1) {
                        splited[1] = "";
                    }
                    return splited;
                },
                i, j, l;

            if (targetTokens.parcent > 0) {
                targetVal = targetVal * math.pow(100, targetTokens.parcent);
            }
            if (targetTokens.permil > 0) {
                targetVal = targetVal * math.pow(1000, targetTokens.permil);
            }
            if (targetTokens.pows > 0) {
                targetVal = targetVal / math.pow(1000, targetTokens.pows);
            }

            targetStr = exponentToNumStr(math.abs(targetVal) + "");
            targetVal = parseFloat(targetStr);

            if (targetVal === 0) {
                targetTokens = hasToken(tokens.zero) || targetTokens;
                targetStr = "0";
            } else if (targetVal < 1 && !hasDecPlaceholder(targetTokens.decs)) {
                targetTokens = hasToken(tokens.zero) || targetTokens;
                targetStr = "0";
            }
            var part = splitPart(targetStr),
                tokenPart = targetTokens.ints,
                targetPart = part[0],
                tokenPartVal,
                result = { ints: "", decs: ""},
                dec, hasDec = false;

            for (i = tokenPart.length - 1; i > -1; i--) {
                tokenPartVal = tokenPart[i];
                if (isStr(tokenPartVal)) {
                    result.ints = tokenPartVal + result.ints;
                } else {
                    result.ints = (targetPart.length !== 0 ? targetPart.substr(targetPart.length - 1) :
                        tokenPartVal.type === "0" ? "0" : "") + result.ints;
                    targetPart = targetPart.substr(0, targetPart.length - 1);
                    if (targetTokens.separate) {
                        if ((part[0].length - targetPart.length) % 3 === 0 && targetPart.length !== 0) {
                            result.ints = "," + result.ints;
                        }
                    }

                    if (!TypyArray(tokenPart).take(i).some(function (v) { return !isStr(v); })) {
                        if (targetTokens.separate) {
                            for (j = targetPart.length; j; j--) {
                                result.ints = targetPart.charAt(j - 1) + result.ints;
                                targetPart = targetPart.substr(0, targetPart.length - 1);
                                if ((part[0].length - targetPart.length) % 3 === 0 && targetPart.length !== 0) {
                                    result.ints = "," + result.ints;
                                }
                            }
                        } else {
                            result.ints = targetPart + result.ints;
                        }
                    }
                }
            }

            tokenPart = targetTokens.decs || [];
            targetPart = part[1];

            for (i = 0, l = tokenPart.length; i < l; i++) {
                tokenPartVal = tokenPart[i];
                if(isStr(tokenPartVal)){
                    result.decs += tokenPartVal;
                } else {
                    dec = targetPart.charAt(0) ? targetPart.charAt(0) :
                        tokenPartVal.type === "0" ? "0" : "";
                    if (dec) {
                        hasDec = true;
                    }
                    result.decs += dec;
                    targetPart = targetPart.substr(1);
                }
            }
            return ((isMinus && targetTokens == tokens.plus) ? "-" : "") + result.ints + (hasDec ? "." + result.decs : result.decs);
        }),
        clamp: function (target, min, max) {
            if (isNumeric(target)) {
                target = parseFloat(target);
            }
            if (!isNum(target)) {
                return;
            }
            if (!isNum(min) || !isNum(max)) {
                return NaN;
            }
            var swap;
            if (min > max) {
                swap = min;
                min = max;
                max = swap;
            }
            return math.max(min, math.min(max, target));
        },
        log10:function (target) {
            return math.LOG10E * math.log(target);
        },
        round: round,
        ceil: function(target, prec){
            return round(target, prec, math.ceil);
        },
        floor: function(target, prec){
            return round(target, prec, math.floor);
        }
    }, isNum);

    TypyArray.each("abs,pow,sin,asin,cos,acos,tan,atan,exp,pow,sqrt,log".split(","), function(item){
        addFunc(TypyNum, item, function(){
            return math[item].apply(math, arguments);
        });
    });

    addStaticFunc(TypyNum, "namedFormat", function(name){
        if(!isStr(name)) return;
        if(arguments.length === 1){
            return namedFormat[name];
        }
        if(arguments.length >= 2){
            namedFormat[name] = arguments[1];
        }
    });
})(this, Typy);

var TypyNum = Typy.num;
/* TimeSpan */

// depends array
(function(global, Typy, undef){

    "use strict";

    var msecPerDay = 1000 * 60 * 60 * 24,
        msecPerHour = 1000 * 60 * 60,
        msecPerMinute = 1000 * 60,
        msecPerSecond = 1000,
        msecPerMillisecond = 1,
        cachedFormatTokens = {},
        namedFormat = {},
        isTimeSpan = function(val){
            return val instanceof TimeSpan;
        },
        units = function(val, context,  msecPer, modulo){
            var result = context._val / msecPer,
                round = context._val < 0 ? Math.ceil: Math.floor;

            if(modulo){
                result = result % modulo;
            }
            result = round(result); //Math.floor(result);
            if(isNum(val)){
                context._val -= (result * msecPer);
                context._val += round(val * msecPer); //Math.floor(val * msecPer);
                return context;
            }else{
                return result;
            }
        },
        totalUnit = function(context, msecPer, roundDown){
            var result = context._val / msecPer,
                round = context._val < 0 ? Math.ceil: Math.floor;

            if(roundDown){
                result = round(result);
            }
            return result;
        },
        addUnits = function(context, val, msecPer){
            context._val += val * msecPer;
            return context;
        },
        tokenize = function (format) {
            var i, l, ch,
                tokens = [],
                escaped = false,
                token, quote,
                tokenizeLiteral = function (target, index) {
                    var match = format.substr(index).match(new RegExp(target + "+"));
                    if (match) {
                        return match[0].length;
                    }
                    return 1;
                };
            if (cachedFormatTokens[format]) {
                return cachedFormatTokens[format];
            }
            for (i = 0, l = format.length; i < l; i++) {

                ch = format.charAt(i);
                if(escaped){
                    tokens.push(ch);
                    escaped = false;
                    continue;
                }
                if(ch === "\\"){
                    escaped = true;
                    continue;
                }
                if(ch === "'" || ch === "\""){
                    if(ch === quote){
                        quote = false;
                    }else{
                        quote = ch;
                    }
                    continue;
                }
                if(quote){
                    tokens.push(ch);
                    continue;
                }
                switch (ch) {
                    case "d":
                    case "f":
                    case "h":
                    case "m":
                    case "s":
                        token = {
                            type:ch,
                            length:tokenizeLiteral(ch, i)
                        };
                        tokens.push(token);
                        i += (token.length - 1);
                        break;
                    //TODO: セパレーターをどうするか検討
                    default:
                        tokens.push(ch);
                }
            }
            cachedFormatTokens[format] = tokens;
            return tokens;
        },
        makeText = function(val, format){
            var named = namedFormat[format];
            if (named && isFunc(named.format)) {
                return named.format(source, TypyLang.current());
            }
            var tokens = tokenize(format),
                tokenItem,
                texts = [],
                type, tlength,
                i, l,
                padZero = function(v, length){
                    v = (v === 0 ? 0 : Math.abs(v)) + "";
                    return TypyStr.padLeft(v, Math.max(v.length, length), "0");
                };

            for(i = 0, l = tokens.length; i < l; i++){
                tokenItem = tokens[i];
                if(isStr(tokenItem)){
                    texts.push(tokenItem);
                    continue;
                }
                type = tokenItem.type;
                tlength = tokenItem.length;
                if(type === "d"){
                    texts.push(padZero(val.days(), tlength));
                } else if(type === "h"){
                    texts.push(padZero(val.hours(), Math.min(tlength, 2)));
                } else if(type === "m"){
                    texts.push(padZero(val.minutes(), Math.min(tlength, 2)));
                } else if(type === "s"){
                    texts.push(padZero(val.seconds(), Math.min(tlength, 2)));
                } else if(type === "f"){
                    texts.push((Math.abs(val.milliseconds()) + "000").substr(0, Math.min(tlength, 3)));
                }
            }
            return ((val._val < 0) ? "-" : "") + texts.join("");
        },
        makeTimeSpanFromFormat = function(value, format){
            var named = namedFormat[format],
                tokens, token, i, l, pos = 0,
                values = [0,0,0,0,0],
                isMinus = value.charAt(0) === "-",
                typeNum = {d:0,h:1,m:2,s:3,f:4},
                parseSingleD = function(){
                    var match = value.substr(pos).match(/^[1-9][0-9]*/);
                    if(match && match.length){
                        values[0] = parseInt10(match[0]);
                        return match[0].length;
                    }else{
                        values[0] = -1;
                    }
                },
                parseFixedLength = function(typePos, length){
                    var match = value.substr(pos).match(new RegExp("^\\d{" + length + "}"));
                    if(match && match.length){
                        values[typePos] = parseInt10(match[0]);
                        return length;
                    }else{
                        values[typePos] = -1;
                    }
                },
                parseHMS = function(typePos){
                    var match = value.substr(pos, 2).match(/^\d{1,2}/);
                    if(match && match.length){
                        values[typePos] = parseInt10(match[0]);
                        return match[0].length;
                    }
                },
                parseF = function(length){
                    var match = value.substr(pos, length).match(new RegExp("^\\d{" + length + "}"));
                    if(match && match.length){
                        values[4] = parseInt10(parseFloat("0." + match[0]) * 1000);
                        return length;
                    }
                };
            if (named && isFunc(named.parse)) {
                return named.parse(value);
            }
            if(isMinus){
                value = value.substr(1);
            }

            tokens = tokenize(format);
            for(i = 0, l = tokens.length; i < l; i++){
                token = tokens[i];
                if(isStr(token)){
                    if(value.substr(pos, token.length) === token){
                        pos += token.length;
                    }else{
                        continue;
                    }
                }else{
//                    if(token.type === "." || token.type === ":"){
//                        //TODO: 多言語から取得
//                    }
                    if(token.type === "d"){
                        if(token.length === 1){
                            pos += parseSingleD();
                        }else{
                            pos += parseFixedLength(typeNum[token.type], token.length);
                        }
                    }else if(token.type === "h" || token.type === "m" || token.type === "s"){

                        if(token.length === 1){
                            pos += parseHMS(typeNum[token.type]);
                        }else{
                            pos += parseFixedLength(typeNum[token.type], token.length);
                        }
                    }else if(token.type === "f"){
                        pos += parseF(token.length);
                    }
                }
            }
            if(pos !== value.length){
                return;
            }
            if(values[1] > 23 || values[2] > 59 || values[3] > 59){
                return;
            }
            return  (new TimeSpan(values[0], values[1], values[2], values[3], values[4]))._val * (isMinus ? -1 : 1);
        };

    function TimeSpan(){
        var arg = toArray(arguments);
        this._val = 0;
        if(!isNumeric.apply(null, arg)){
            return;
        }
        if(arg.length === 1){
            //ミリ秒
            this._val = parseInt10(arg[0]);
        }else if(arg.length === 3){
            //時、分、秒
            this._val += msecPerSecond * parseInt10(arg[2]);
            this._val += msecPerMinute * parseInt10(arg[1]);
            this._val += msecPerHour * parseInt10(arg[0]);
        }else if(arg.length === 4){
            //日、時、分、秒
            this._val += msecPerSecond * parseInt10(arg[3]);
            this._val += msecPerMinute * parseInt10(arg[2]);
            this._val += msecPerHour * parseInt10(arg[1]);
            this._val += msecPerDay * parseInt10(arg[0]);
        }else if(arg.length === 5){
            //日、時、分、秒、ミリ秒
            this._val += parseInt10(arg[4]);
            this._val += msecPerSecond * parseInt10(arg[3]);
            this._val += msecPerMinute * parseInt10(arg[2]);
            this._val += msecPerHour * parseInt10(arg[1]);
            this._val += msecPerDay * parseInt10(arg[0]);
        }
    }

    extend(TimeSpan.prototype, {
        days: function(val){
            return units(val, this, msecPerDay);
        },
        hours: function(val){
            return units(val, this, msecPerHour, 24);
        },
        minutes: function(val){
            return units(val, this, msecPerMinute, 60);
        },
        seconds: function(val){
            return units(val, this, msecPerSecond, 60);
        },
        milliseconds: function(val){
            return units(val, this, msecPerMillisecond, 1000);
        },
        totalDays: function(roundDown){
            return totalUnit(this, msecPerDay, roundDown);
        },
        totalHours: function(roundDown){
            return totalUnit(this, msecPerHour, roundDown);
        },
        totalMinutes: function(roundDown){
            return totalUnit(this, msecPerMinute, roundDown);
        },
        totalSeconds: function(roundDown){
            return totalUnit(this, msecPerSecond, roundDown);
        },
        totalMilliseconds: function(roundDown){
            return totalUnit(this, msecPerMillisecond, roundDown);
        },
        add: function(val){
            return addUnits(this, val, msecPerMillisecond);
        },
        addDays: function(val){
            return addUnits(this, val, msecPerDay);
        },
        addHours: function(val){
            return addUnits(this, val, msecPerHour);
        },
        addMinutes: function(val){
            return addUnits(this, val, msecPerMinute);
        },
        addSeconds: function(val){
            return addUnits(this, val, msecPerSecond);
        },
        addMilliseconds: function(val){
            return addUnits(this, val, msecPerMillisecond);
        }
    });

    var TypyTimeSpan = Typy.timeSpan = extDefine(function (constractor, target) {
        var args = takeArg(arguments, 1);
        constractor.prototype.val = function(){
            return !isTimeSpan(this._val) ? void 0 : this._val._val;
        };

        if(args.length === 2 && isStr(target) && isStr(args[1])){
            var milsec = makeTimeSpanFromFormat(target, args[1]);
            if(isNum(milsec)){
                return new constractor(new TimeSpan(milsec))
            }
            return new constractor();
        }
        if(!isNum.apply(Typy, args)){
            return new constractor();
        }

        function F(a){
            return TimeSpan.apply(this, a);
        }
        F.prototype = TimeSpan.prototype;

        return new constractor(new F(args));
    }, {
        format: unchainable(function(target, format) {
            if(!isTimeSpan(target)){
                target = TypyTimeSpan(target)._val;
            }
            if (isTimeSpan(target)) {
                return makeText(target, format);
            }
        })
    }, isTimeSpan);

    TypyArray.each(["days", "hours", "minutes", "seconds", "milliseconds"], function (item) {
        var totalUnitName = "total" + item.charAt(0).toUpperCase() + item.substr(1),
            addUnitsName = "add" + item.charAt(0).toUpperCase() + item.substr(1);
        addInstanceFunc(TypyTimeSpan, item, function (val) {
            if (isTimeSpan(this._val)) {
                return this._val[item].apply(this._val, takeArg(arguments, 0));
            }
        });
        addInstanceFunc(TypyTimeSpan, totalUnitName, function (val) {
            if (isTimeSpan(this._val)) {
                return this._val[totalUnitName].apply(this._val, takeArg(arguments, 0));
            }
        });
        addInstanceFunc(TypyTimeSpan, addUnitsName, function (val) {
            if (isTimeSpan(this._val)) {
                return this._val[addUnitsName].apply(this._val, takeArg(arguments, 0));
            }
        });
    });
    addInstanceFunc(TypyTimeSpan, "add", function (val) {
        if (isTimeSpan(this._val)) {
            return this._val.add(val);
        }
    });
    addStaticFunc(TypyTimeSpan, "namedFormat", function(name){
        if(!isStr(name)) return;
        if(arguments.length === 1){
            return namedFormat[name];
        }
        if(arguments.length >= 2){
            namedFormat[name] = arguments[1];
        }
    });
    addStaticFunc(TypyTimeSpan, "parse", function(target, format){
        if(isStr(target)){
            if(isStr(format)){
                return makeTimeSpanFromFormat(target, format);
            }else{

            }
        }
    });
})(this, Typy);
/* uri */
// This program referred to uri_funcs.js.
// Author (original): Mike J. Brown <mike at skew.org>
// http://skew.org/uri/uri_funcs.js

/******************************************************************************
 uri_funcs.js - URI functions based on STD 66 / RFC 3986

 Author (original): Mike J. Brown <mike at skew.org>
 Version: 2007-01-04

 License: Unrestricted use and distribution with any modifications permitted,
 so long as:
 1. Modifications are attributed to their author(s);
 2. The original author remains credited;
 3. Additions derived from other code libraries are credited to their sources
 and used under the terms of their licenses.

 *******************************************************************************/

// depends array

(function (global, Typy, undefined) {

    //RFC 3986 appendix B.
    var parseUriRegx = /^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?$/,
        missingGroupSupport = (typeof "".match(/(a)?/)[1] !== "string"),
        parseUri = function (target) {
            var matches;
            if (!isStr(target)) {
                return;
            }
            matches = target.match(parseUriRegx);
            matches.shift();
            return {
                schema:!missingGroupSupport && matches[0] === "" ? undefined : matches[1],
                authority:!missingGroupSupport && matches[2] === "" ? undefined : matches[3],
                path:matches[4],
                query:!missingGroupSupport && matches[5] === "" ? undefined : matches[6],
                fragment:!missingGroupSupport && matches[7] === "" ? undefined : matches[8]
            };
        },
        isAbsoluteRegx = /^[A-Z][0-9A-Z+\-\.]*:/i,
        isAbsolute = function (target) {
            return isAbsoluteRegx.test(target);
        },
        toUriString = function (target) {
            var result = "",
                path = target.path;
            if (!isUndef(target.schema)) {
                result += target.schema + ":";
            }
            if (!isUndef(target.authority)) {
                result += "//" + target.authority;
            }
            if (!isUndef(path) && path.length > 0) {
                if (!isUndef(target.authority)) {
                    if (path.charAt(0) !== "/") {
                        path = "/" + path;
                    }
                }
                //TODO 仕様の再確認
//                else{
//                    if(path.substr(0, 2) === "//"){
//                        path = "/" + path.replace(/^\/+/, "");
//                    }
//                }
                result += path;
            }

            if (!isUndef(target.query)) {
                result += "?" + target.query;
            }
            if (!isUndef(target.fragment)) {
                result += "#" + target.fragment;
            }
            return result;
        },
    //RFC 3986 5.2.4
    //Based on code from 4Suite XML:
    //http://skew.org/uri/uri_funcs.js
        removeDotSegments = function (path) {
            var startIsSeparator = false,
                segments,
                seg,
                keepers = [];
            // return empty string if entire path is just "." or ".."
            if (path === "." || path === "..") {
                return "";
            }
            // remove all "./" or "../" segments at the beginning
            while (path) {
                if (path.substring(0, 2) === "./") {
                    path = path.substring(2);
                } else if (path.substring(0, 3) === "../") {
                    path = path.substring(3);
                } else {
                    break;
                }
            }
            if (path.charAt(0) === "/") {
                path = path.substring(1);
                startIsSeparator = true;
            }
            if (path.substring(path.length - 2) === "/.") {
                path = path.substring(0, path.length - 1);
            }
            segments = path.split("/").reverse();
            while (segments.length) {
                seg = segments.pop();
                if (seg === "..") {
                    if (keepers.length) {
                        keepers.pop();
                    } else if (!startIsSeparator) {
                        keepers.push(seg);
                    }
                    if (!segments.length) {
                        keepers.push("");
                    }
                } else if (seg !== ".") {
                    keepers.push(seg);
                }
            }
            // reassemble the kept segments
            return (startIsSeparator && "/" || "") + keepers.join("/");
        },
    //RFC 3986 5.2.2
        absolutize = function (base, relative) {
            var part,
                relSchema, relAuth, relPath, relQuery, relFrag,
                bSchema, bAuth, bPath, bQuery,
                resSchema, resAuth, resPath, resQuery, resFrag;
            if (!isStr(base) || !isAbsolute(base) || !isStr(relative)) {
                return;
            }

            if (relative == "" || relative.charAt(0) == "#") {
                return parseUri(base.split('#')[0] + relative);
            }
            part = parseUri(relative) || {};
            relSchema = part.schema;
            relAuth = part.authority;
            relPath = part.path;
            relQuery = part.query;
            relFrag = part.fragment;
            if (relSchema) {
                resSchema = relSchema;
                resAuth = relAuth;
                resPath = removeDotSegments(relPath);
                resQuery = relQuery;
            } else {
                part = parseUri(base) || {};
                bSchema = part.schema;
                bAuth = part.authority;
                bPath = part.path;
                bQuery = part.query;
                if (relAuth) {
                    resAuth = relAuth;
                    resPath = removeDotSegments(relPath);
                    resQuery = relQuery;
                } else {
                    if (!relPath) {
                        resPath = bPath;
                        resQuery = relQuery ? relQuery : bQuery;
                    } else {
                        if (relPath.charAt(0) === "/") {
                            resPath = removeDotSegments(relPath);
                        } else {
                            //RFC 3986 5.2.3
                            if (bAuth && !bPath) {
                                resPath = "/" + relPath;
                            } else {
                                resPath = bPath.substring(0, bPath.lastIndexOf("/") + 1) + relPath;
                            }
                            resPath = removeDotSegments(resPath);
                        }
                        resQuery = relQuery;
                    }
                    resAuth = bAuth;
                }
                resSchema = bSchema;
            }
            resFrag = relFrag;
            return {
                schema:resSchema,
                authority:resAuth,
                path:resPath,
                query:resQuery,
                fragment:resFrag
            };
        },

        isUri = function (val) {
            return val instanceof Uri;
        },

        property = function (target, value, prop, setCallback) {
            var source;
            if (isUndef(target) && isUndef(value)) {
                return;
            }
            source = isStr(target) ? parseUri(target) : (isUri(target) ? target._val : void 0);
            if (!source) {
                return;
            }
            if (isUndef(value)) {
                return source[prop];
            }
            if (isStr(value)) {
                if (isFunc(setCallback)) {
                    value = setCallback(source, value);
                }
                source[prop] = value;
            }
            if (isStr(target)) {
                return toUriString(source);
            }
            return target;
        };

    function Uri(args) {
        this._val = {};
        if (args.length) {
            if (args.length < 2) {
                this._val = parseUri(args[0]);
            } else {
                this._val = absolutize(args[0], args[1]);
            }
        }
        return this;
    }

    Uri.prototype.toString = function(){
        return toUriString(this._val);
    };

    var TypyUri = Typy.uri = extDefine(function (constractor) {
        constractor.prototype.val = function(){
            return !isUri(this._val) ? void 0 : this._val.toString();
        };

        return new constractor(new Uri(takeArg(arguments, 1)));
    }, {}, isUri);

    TypyArray.each(["schema", "authority", "path", "query", "fragment"], function (item) {
        addFunc(TypyUri, item, function (target, value) {
            return property(target, value, item);
        })
    });

    addStaticFunc(TypyUri, "resolve", function (base, relative) {
        if (!isStr(base) || !isStr(relative)) {
            return;
        }
        var uri = absolutize(base, relative);
        return toUriString(uri);
    });

})(this, Typy);

var TypyUri = Typy.uri;
/* uuid */
// This program referred to UUID.js.
// https://github.com/LiosK/UUID.js

// Copyright (c) 2010-2012 LiosK.
//
//     Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
//     The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
//     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// dependon array.js, string.js

(function (global, Typy) {

//0: timeLow
//1: timeMid
//2: timeHiAndVersion
//3: clockSeqHiAndReserved
//4: clockSeqLow
//5: node

    function Uuid() {
        var vals = slice.call(arguments),
            valStrs = TypyArray.map(vals, function (val, index) {
                return align(val, index === 0 ? 8 :
                    index === 1 || index === 2 ? 4 :
                        index === 3 || index === 4 ? 2 :
                            12);
            }),
            empty = !TypyArray.some(vals, function (val) {
                return val !== 0
            });

        this._vals = vals;
        this._valStrs = valStrs;
        this.isEmpty = function () {
            return empty;
        }
    }

    Uuid.prototype.toString = function (format) {
        var placeholder, args;

        if (isUndef(format) || isNull(format) || format === "") {
            format = "D";
        }
        placeholder = formats[format];
        if (placeholder) {
            args = Array.apply(null, this._valStrs);
            args.unshift(placeholder);
            return TypyStr.format.apply(TypyStr, args);
        }
        return "";
    };

    var random = function (range) {
            if (range < 0) return NaN;
            if (range <= 30) return Math.floor(Math.random() * (1 << range));
            if (range <= 53) return Math.floor(Math.random() * (1 << 30)) +
                Math.floor(Math.random() * (1 << range - 30)) * (1 << 30);
            return NaN;
        },
        parseRegx = /^([0-9a-f]{8})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{12})$/i,
        parseSeparatedRegx = /^[\(\{]?([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{2})([0-9a-f]{2})-([0-9a-f]{12})[\)\}]?$/i,
        parse = function (value) {
            var items;
            if (!isStr(value)) {
                return;
            }
            if (value.length === 32) {
                items = parseRegx.exec(value);
            } else {
                items = parseSeparatedRegx.exec(value);
            }
            if (!items) {
                return;
            }
            return new Uuid(
                parseInt16(items[1]), parseInt16(items[2]),
                parseInt16(items[3]), parseInt16(items[4]),
                parseInt16(items[5]), parseInt16(items[6])
            );
        },
        gen = function () {
            return new Uuid(
                random(32), random(16),
                0x4000 | random(12),
                0x80 | random(6),
                random(8), random(48));
        },
        align = function (value, length) {
            return TypyStr.padLeft(value.toString(16), length, "0");
        },
        emptyuuid = new Uuid(0, 0, 0, 0, 0, 0),
        formats = {
            "N":"{0}{1}{2}{3}{4}{5}",
            "D":"{0}-{1}-{2}-{3}{4}-{5}",
            "B":"{{0}-{1}-{2}-{3}{4}-{5}}",
            "P":"({0}-{1}-{2}-{3}{4}-{5})"
        },
        isUuid = function (val) {
            return val instanceof Uuid;
        };

    var TypyUuid = Typy.uuid = extDefine(function (constractor, target) {
        var empty = false;

        constractor.prototype.val = function(){
            return !isUuid(this._val) ? void 0 : this._val.toString();
        };

        if (isStr(target)) {
            if (target) {
                return new constractor(parse(target));
            }
            empty = false;
        }
        if (isBool(target)) {
            empty = target;
        }
        if (isNum(target)) {
            if (target !== 0) {
                return new constractor();
            }
            empty = true;
        }
        if (empty) {
            return new constractor(emptyuuid);
        }
        return new constractor(gen());
    }, {
        format: unchainable(function(target, format){
            if (isUuid(target)) {
                return target.toString(format);
            }
        })
    }, isUuid);

    addInstanceFunc(TypyUuid, "toString", unchainable(function (format) {
        if (isUuid(this._val)) {
            return this._val.toString(format);
        }
        return "";
    }));

})(this, Typy);

var TypyUuid = Typy.uuid;
if(typeof module !== "undefined"){
    module.exports = Typy;
}

if(typeof define === "function" && define.amd){
    define(function(){
        return Typy;
    });
}

//TODO 検討
global.Typy = Typy;

})(this);
