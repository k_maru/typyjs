module.exports = function(grunt){
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        meta: {
            banner: '/*!\r\n' +
             '* <%= pkg.name %> JavaScript Library v<%= pkg.version %>\r\n' +
             '* Copyright 2013, <%= pkg.author.name %>\r\n' +
             '* <%= pkg.name %> may be freely distributed under the <%= pkg.license %> license.\r\n' +
             '*/\r\n'
        },
        concat: {
            base: {
                options: {
                    banner: '<%= meta.banner %>' + grunt.file.read('src/base/typy.before.txt'),
                    footer: grunt.file.read('src/base/typy.after.txt')
                },
                src: ['src/base/core.js',
                      'src/base/object.js',
                      'src/base/array.js',
                      'src/base/function.js',
                      'src/base/string.js',
                      'src/base/lang.js',
                      'src/base/date.js',
                      'src/base/number.js',
                      'src/base/timeSpan.js',
                      'src/base/uri.js',
                      'src/base/uuid.js'
                ],
                dest: 'build/typy.js'
            }
        },
        uglify: {
            base: {
                options: {
                    banner: '<%= meta.banner %>',
                    mangle: true,
                    sourceMap: 'build/typy.map.js'
                },
                files: {
                    'build/typy.min.js': ['<%= concat.base.dest %>']
                }
            }
        },
        clean: {
            base: [
                'build/*.js'
            ]
        },
        shell: {
            base: {
                command: 'node tools/metadata/metadata-gen.js -i:<%= concat.base.dest %> -o:build/metadata.txt',
                stdout: true
            }
        },
        mochacli: {
            options: {
                reporter: 'dot',
                bail: true
            },
            all: ['test/unit/base/*.js']
        }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-mocha-cli');
    grunt.registerTask('test', ['mochacli']);
    grunt.registerTask('default', ['clean', 'concat', 'uglify', 'shell', 'test']);
};