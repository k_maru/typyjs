require([ "typy", "jquery", "knockout", "!domReady"], function (Typy, $, ko) {

    "use strict";

    var findCoreFuncs = function () {
            return Typy.obj(Typy).keys.x().filter(function (prop) {
                var val = Typy[prop];
                return Typy.isFunc(val) && !val._source && prop !== "ext" && prop !== "lang";
            }).sort().map(function (prop) {
                return {
                    name: prop,
                    type: "static",
                    modl: "Typy"
                };
            }).val();
        },
        findModules = function () {
            return Typy.obj(Typy).keys.x().filter(function (prop) {
                var val = Typy[prop];
                return Typy.isFunc(val) && val._source == Typy;
            }).sort().map(function (prop) {
                return {
                    name: "Typy." + prop,
                    funcs: findFuncs(Typy[prop],"Typy." + prop)
                }
            }).val();
        },
        findFuncs = function (type, modl) {

            var result = [], availableInstanceFuncs = [];

            Typy.array(Typy.keys(type)).filter(function (prop) {
                return !Typy.str.startsWith(prop, "_");
            }).each(function (prop) {
                var func = {
                    name: prop,
                    type: "static",
                    modl: modl
                };
                if (type._constractor.prototype[prop]) {
                    func.type += " instance";
                    availableInstanceFuncs.push(prop);
                }
                result.push(func);
            });

            Typy.obj(type._constractor.prototype).keys.x().filter(function (prop) {
                return !Typy.array.contains(availableInstanceFuncs, prop)
            }).each(function (prop) {
                result.push({
                    name: prop,
                    type: "instance",
                    modl: modl
                });
            });
            return result;

        },
        metadata = {},
        viewModel = function () {
            var self = this;
            self.modules = [
                {
                    name: "Core",
                    funcs: findCoreFuncs()
                }
            ].concat(findModules());
            self.funcs = ko.observableArray();
            self.selectProp = function(target){
                if(metadata){
                    var modl = metadata[target.modl];
                    var prop = modl ? modl[target.name] : void 0;
                    self.funcs.removeAll();
                    if(prop){
                        prop.modl = target.modl;
                        self.funcs.push(prop);
                    }
                }
            };
            self.selectModl = function(target){
                var name = target.name,
                    modl, modFunc, i, l;
                if(name === "Core"){
                    name = "Typy";
                }
                modl = metadata[name];
                self.funcs.removeAll();
                if(!modl){
                    return;
                }
                for(i = 0, l = target.funcs.length; i < l; i++){
                    if(modl.hasOwnProperty(target.funcs[i].name)){
                        modFunc = modl[target.funcs[i].name];
                        if(modFunc){
                            modFunc.modl = name;
                            self.funcs.push(modFunc);
                        }
                    }
                }
            };
        };

    $.get("/build/metadata.txt").done(function(val){
       metadata =  (new Function("return " + val + ";"))()["module"];
    });

    ko.applyBindings(new viewModel());
});
