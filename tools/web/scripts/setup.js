(function(global){

    document.createElement("nav");
    document.createElement("section");
    document.createElement("article");

    var baseUrl = "/tools/web/scripts/";
    global.require = {
        baseUrl: baseUrl,
        paths:{
            "domReady": baseUrl + "/domReady",
            "jquery": baseUrl + "/jquery-1.9.0b1",
            "knockout": baseUrl + "knockout-2.2.0.debug",
            "typy": "/build/typy"
        }
    };

})(window);
