var fs = require("fs")

var OptionsParser = (function () {
    function OptionsParser() {
        var retrieve = function (param) {
            var arg = process.argv, i = 0, l = arg.length, match;
            for(; i < l; i++) {
                match = (new RegExp("^\\-" + param + ":(.*)")).exec(arg[i]);
                if(match) {
                    return match[1];
                }
            }
        };
        this._inname = retrieve("i");
        this._outname = retrieve("o");
    }
    Object.defineProperty(OptionsParser.prototype, "inname", {
        get: function () {
            return this._inname;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OptionsParser.prototype, "outname", {
        get: function () {
            return this._outname;
        },
        enumerable: true,
        configurable: true
    });
    return OptionsParser;
})();
var MetadataGen;
(function (MetadataGen) {
    var metadata = {
        module: {
        }
    };
    function trim(target) {
        return target.replace(/^[\s　]+|[\s　]+$/g, "");
    }
    function startsWith(target, prefix) {
        return target.indexOf(prefix) === 0;
    }
    function removeCommentAster(line) {
        if(startsWith(trim(line), "*")) {
            line = line.replace(/\s*\*\s?/, "");
        }
        return line;
    }
    function extractCommentItemValue(lines, pos, def, wizOpt) {
        var line = trim(removeCommentAster(lines[pos]).replace(def, "")), opt, result = {
value: ""        }, currentPos = pos + 1, requireNewLine = false;
        if(wizOpt) {
            opt = line.split(" ")[0];
            line = line.replace(opt, "");
            result.opt = opt;
        }
        if(line.replace(/\s*/, "")) {
            result.value = trim(line);
            requireNewLine = true;
        }
        while(lines.length > currentPos) {
            line = removeCommentAster(lines[currentPos]);
            if(startsWith(trim(line), "@")) {
                break;
            }
            result.value += (requireNewLine ? "\n" : "") + line;
            requireNewLine = true;
            currentPos++;
        }
        result.lines = currentPos - pos - 1;
        return result;
    }
    function resolveComment(lines) {
        var moduleName, methodName, summary, override, example, params = [], ret, i, l, tline, itemValue, paramOpt;
        for(i = 0 , l = lines.length; i < l; i++) {
            tline = removeCommentAster(lines[i]);
            if(startsWith(tline, "@module")) {
                moduleName = trim(tline.replace("@module", ""));
            } else {
                if(startsWith(tline, "@method")) {
                    methodName = trim(tline.replace("@method", ""));
                } else {
                    if(startsWith(tline, "@override")) {
                        override = trim(tline.replace("@override", ""));
                    } else {
                        if(startsWith(tline, "@summary")) {
                            itemValue = extractCommentItemValue(lines, i, "@summary");
                            summary = itemValue.value;
                            i += itemValue.lines;
                        } else {
                            if(startsWith(tline, "@example")) {
                                itemValue = extractCommentItemValue(lines, i, "@example");
                                example = itemValue.value;
                                i += itemValue.lines;
                            } else {
                                if(startsWith(tline, "@param")) {
                                    itemValue = extractCommentItemValue(lines, i, "@param", true);
                                    paramOpt = itemValue.opt.split(":");
                                    params.push({
                                        name: paramOpt[0],
                                        type: (function () {
                                            paramOpt.shift();
                                            return paramOpt.join(":");
                                        })() || "",
                                        desc: itemValue.value
                                    });
                                    i += itemValue.lines;
                                } else {
                                    if(startsWith(tline, "@return")) {
                                        itemValue = extractCommentItemValue(lines, i, "@return", true);
                                        ret = {
                                            type: itemValue.opt,
                                            desc: itemValue.value
                                        };
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if(!(moduleName in metadata.module)) {
            metadata.module[moduleName] = {
            };
        }
        if(!((methodName || override) in metadata.module[moduleName])) {
            metadata.module[moduleName][methodName || override] = {
            };
        }
        if(methodName) {
            metadata.module[moduleName][methodName].name = methodName;
            metadata.module[moduleName][methodName].summary = summary;
            metadata.module[moduleName][methodName].params = params;
            metadata.module[moduleName][methodName].ret = ret;
            metadata.module[moduleName][methodName].example = example;
        } else {
            if(override) {
                if(!("overrides" in metadata.module[moduleName][override])) {
                    metadata.module[moduleName][override].overrides = [];
                }
                metadata.module[moduleName][override].overrides.push({
                    summary: summary,
                    params: params,
                    ret: ret,
                    example: example
                });
            }
        }
    }
    function run() {
        var options = new OptionsParser();
        fs.readFile(options.inname, "UTF-8", function (err, data) {
            if(err) {
                console.log(err);
                process.exit(1);
                return;
            }
            var docCommentLines = [], inComments = false;
            data.toString().split("\n").forEach(function (line) {
                var trimedLine = trim(line);
                if(inComments) {
                    if(trimedLine === "*/") {
                        resolveComment(docCommentLines);
                        docCommentLines = [];
                        inComments = false;
                    } else {
                        docCommentLines.push(trimedLine);
                    }
                }
                if(trimedLine === "/***") {
                    inComments = true;
                }
            });
            fs.writeFile(options.outname, JSON.stringify(metadata, null, " "), "UTF-8", function (err) {
                if(err) {
                    console.log(err);
                    process.exit(1);
                    return;
                }
                console.log("File " + options.outname + " created.");
                process.exit(0);
            });
        });
    }
    MetadataGen.run = run;
})(MetadataGen || (MetadataGen = {}));
MetadataGen.run();
