/// <reference path="typings/node.d.ts" />

import fs = module("fs");
import path = module("path");

class OptionsParser{
    private _inname: string;
    private _outname: string;

    constructor(){

        var retrieve = function(param): string{
            var arg: string[] = process.argv,
                i: number = 0,
                l: number = arg.length,
                match: RegExpExecArray;
            for(; i < l; i++){
                match = (new RegExp("^\\-" + param + ":(.*)")).exec(arg[i]);
                if(match){
                    return match[1];
                }
            }
        };
        this._inname = retrieve("i");
        this._outname = retrieve("o");
    }

    get inname(): string {
        return this._inname;
    }

    get outname(): string{
        return this._outname;
    }
}

module MetadataGen{

    var metadata = {
        module: {}
    };

    function trim(target: string): string{
        return target.replace(/^[\s　]+|[\s　]+$/g, "");
    }

    function startsWith(target: string, prefix: string): bool{
        return target.indexOf(prefix) === 0;
    }

    function removeCommentAster(line: string) : string{
        if(startsWith(trim(line),"*")){
            line = line.replace(/\s*\*\s?/, "");
        }
        return line;
    }

    function extractCommentItemValue(lines: string[], pos: number, def: string, wizOpt?: bool) :
        {value: string; opt?: string; lines?: number;} {

        var line = trim(removeCommentAster(lines[pos]).replace(def, "")),
            opt: string,
            result: {value: string; opt?: string; lines?: number;} = {value: ""},
            currentPos: number = pos + 1,
            requireNewLine: bool = false;
        if(wizOpt){
            opt = line.split(" ")[0];
            line = line.replace(opt, "");
            result.opt = opt;
        }
        if(line.replace(/\s*/, "")){
            result.value = trim(line);
            requireNewLine = true;
        }
        while(lines.length > currentPos){
            line = removeCommentAster(lines[currentPos]);
            if(startsWith(trim(line), "@")){
                break;
            }
            result.value += (requireNewLine ? "\n" : "") + line;
            requireNewLine = true;
            currentPos++;
        }
        result.lines = currentPos - pos - 1;
        return result;
    }


    function resolveComment(lines: string[]): void{
        var moduleName: string,
            methodName: string,
            summary: string,
            override: string,
            example: string,
            params: {name: string; type: string; desc: string;}[] = [],
            ret: {type: string; desc: string;},
            i: number,
            l: number,
            tline: string,
            itemValue: {value: string; lines: number; opt: string;},
            paramOpt;
        for(i = 0, l = lines.length; i < l; i++){
            tline = removeCommentAster(lines[i]);
            if(startsWith(tline, "@module")){
                moduleName = trim(tline.replace("@module", ""));
            }else if(startsWith(tline, "@method")){
                methodName = trim(tline.replace("@method", ""));
            }else if(startsWith(tline, "@override")){
                override = trim(tline.replace("@override", ""));
            }else if(startsWith(tline, "@summary")){
                itemValue = extractCommentItemValue(lines, i, "@summary");
                summary = itemValue.value;
                i += itemValue.lines;
            }else if(startsWith(tline, "@example")){
                itemValue = extractCommentItemValue(lines, i, "@example");
                example = itemValue.value;
                i += itemValue.lines;
            }else if(startsWith(tline, "@param")){
                itemValue = extractCommentItemValue(lines, i, "@param", true);
                paramOpt = itemValue.opt.split(":");
                params.push({
                    name: paramOpt[0],
                    type: (function(){
                        paramOpt.shift();
                        return paramOpt.join(":");
                    })() || "",
                    desc: itemValue.value
                });
                i += itemValue.lines;
            }else if(startsWith(tline, "@return")){
                itemValue = extractCommentItemValue(lines, i, "@return", true);
                ret = {
                    type: itemValue.opt,
                    desc: itemValue.value
                };
            }
        }
        if(!(moduleName in metadata.module)){
            metadata.module[moduleName] = {};
        }
        if(!((methodName || override) in metadata.module[moduleName])){
            metadata.module[moduleName][methodName || override ] = {};
        }
        if(methodName){
            metadata.module[moduleName][methodName].name = methodName;
            metadata.module[moduleName][methodName].summary = summary;
            metadata.module[moduleName][methodName].params = params;
            metadata.module[moduleName][methodName].ret = ret;
            metadata.module[moduleName][methodName].example = example;
        }else if(override){
            if(!("overrides" in metadata.module[moduleName][override])){
                metadata.module[moduleName][override].overrides = [];
            }
            metadata.module[moduleName][override].overrides.push({
                summary: summary,
                params: params,
                ret: ret,
                example: example
            });
        }
    }

    export function run(): void{
        var options: OptionsParser = new OptionsParser();
        fs.readFile(options.inname, "UTF-8", function (err: Error, data: string): void{
            if(err){
                console.log(err);
                process.exit(1);
                return;
            }
            var docCommentLines = [],
                inComments = false;
            data.toString().split("\n").forEach(function(line: string){
                var trimedLine = trim(line);
                if(inComments){
                     if(trimedLine === "*/"){
                        resolveComment(docCommentLines);
                        docCommentLines = [];
                        inComments = false;
                    }else{
                        docCommentLines.push(trimedLine);
                    }
                }
                if(trimedLine === "/***"){
                    inComments = true;
                }
            });

            fs.writeFile(options.outname, JSON.stringify(metadata, null, " "), "UTF-8", function(err: Error){
                if(err){
                    console.log(err);
                    process.exit(1);
                    return;
                }
                console.log("File " + options.outname + " created.");
                process.exit(0);
            })
        });
    }
}

MetadataGen.run();