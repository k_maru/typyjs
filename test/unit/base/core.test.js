if (typeof window === "undefined" && require) {
    expect = require('expect.js');
    Typy = require("../../../build/typy");
}

describe("base functions", function(){

    function Foo(){}

    it("isUndef", function(){
        expect(Typy.isUndef(void 0)).to.be(true);

        expect(Typy.isUndef()).to.be(false);
        expect(Typy.isUndef(null)).to.be(false);
        expect(Typy.isUndef([])).to.be(false);
        expect(Typy.isUndef(new Array(1,2,3))).to.be(false);
        expect(Typy.isUndef(Foo)).to.be(false);
        expect(Typy.isUndef(NaN)).to.be(false);
        expect(Typy.isUndef(new Date())).to.be(false);
        expect(Typy.isUndef(new RegExp())).to.be(false);
        expect(Typy.isUndef(new Function())).to.be(false);
        expect(Typy.isUndef(/regx/)).to.be(false);
        expect(Typy.isUndef(1)).to.be(false);
        expect(Typy.isUndef("A")).to.be(false);
        expect(Typy.isUndef(true)).to.be(false);
        expect(Typy.isUndef(false)).to.be(false);

        expect(Typy.isUndef(void 0, void 0)).to.be(true);
        expect(Typy.isUndef(void 0, null)).to.be(false);
        expect(Typy.isUndef(null, void 0)).to.be(false);
    });

    it("isObj", function(){

        expect(Typy.isObj({})).to.be(true);
        expect(Typy.isObj(new Object())).to.be(true);
        expect(Typy.isObj(new Foo())).to.be(true);

        expect(Typy.isObj()).to.be(false);
        expect(Typy.isObj(null)).to.be(false);
        expect(Typy.isObj(void 0)).to.be(false);
        expect(Typy.isObj([])).to.be(false);
        expect(Typy.isObj(new Array(1,2,3))).to.be(false);
        expect(Typy.isObj(Foo)).to.be(false);
        expect(Typy.isObj(new Function())).to.be(false);
        expect(Typy.isObj(NaN)).to.be(false);
        expect(Typy.isObj(new Date())).to.be(false);
        expect(Typy.isObj(new RegExp())).to.be(false);
        expect(Typy.isObj(/regx/)).to.be(false);
        expect(Typy.isObj(1)).to.be(false);
        expect(Typy.isObj("A")).to.be(false);
        expect(Typy.isObj(true)).to.be(false);
        expect(Typy.isObj(false)).to.be(false);

        expect(Typy.isObj({}, new Object(), new Foo())).to.be(true);
        expect(Typy.isObj({}, new Object(), [])).to.be(false);
        expect(Typy.isObj([], void 0, {})).to.be(false);

    });

    it("isFunc", function(){

        expect(Typy.isFunc(Foo)).to.be(true);
        expect(Typy.isFunc(new Function())).to.be(true);

        expect(Typy.isFunc({})).to.be(false);
        expect(Typy.isFunc(new Object())).to.be(false);
        expect(Typy.isFunc(new Foo())).to.be(false);
        expect(Typy.isFunc()).to.be(false);
        expect(Typy.isFunc(null)).to.be(false);
        expect(Typy.isFunc(void 0)).to.be(false);
        expect(Typy.isFunc([])).to.be(false);
        expect(Typy.isFunc(new Array(1,2,3))).to.be(false);
        expect(Typy.isFunc(NaN)).to.be(false);
        expect(Typy.isFunc(new Date())).to.be(false);
        expect(Typy.isFunc(new RegExp())).to.be(false);
        expect(Typy.isFunc(/regx/)).to.be(false);
        expect(Typy.isFunc(1)).to.be(false);
        expect(Typy.isFunc("A")).to.be(false);
        expect(Typy.isFunc(true)).to.be(false);
        expect(Typy.isFunc(false)).to.be(false);

        expect(Typy.isFunc(Foo, new Function())).to.be(true);
        expect(Typy.isFunc(Foo, new Object())).to.be(false);
        expect(Typy.isFunc([], Foo)).to.be(false);

    });
});