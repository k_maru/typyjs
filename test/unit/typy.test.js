var timeout = function (func, timeout) {
    var active = true;
    func({
        start:function () {
            if (!active) return false;
            start();
            return true;
        },
        complete:function () {
            active = false;
        }
    });
    setTimeout(function () {
        if (active) {
            start();
            ok(false, "timeout");
        }
        active = false;
    }, timeout);
};

QUnit.module("Typy.extend");
test("プロパティが追加される", function () {
    var source = { a:"foo", b:"bar" },
        dest = { c:1, d:2 },
        result = Typy.extend(source, dest);
    strictEqual(result.a, source.a);
    strictEqual(result.b, source.b);
    strictEqual(result.c, dest.c);
    strictEqual(result.d, dest.d);
});
test("先頭 が undefined", function () {
    var dest = { c:1, d:2 },
        result = Typy.extend(void 0, dest);
    ok(typeof result.a === "undefined");
    ok(typeof result.b === "undefined");
    deepEqual(result.c, dest.c);
    deepEqual(result.d, dest.d);
});
test("第2引数以降 が undefined", function () {
    var source = { a:"foo", b:"bar" },
        dest,
        result = Typy.extend(source, dest);
    strictEqual(result.a, source.a);
    strictEqual(result.b, source.b);
    ok(typeof result.c === "undefined");
    ok(typeof result.d === "undefined");
});
test("結果オブジェクトは第1引数", function () {
    var source = {},
        dest = {a:1},
        result = Typy.extend(source, dest);
    equal(source, result);
    notEqual(dest, result);
});
test("先頭がobjまたはfuncじゃなかったら新しいobj", function () {
    var source = null, dest = {a:1},
        result = Typy.extend(source, dest);
    notEqual(source, result);
    notEqual(dest, result);
});
test("より後ろの引数の内容が利用される", function () {
    var source = { a:"foo", b:"bar" },
        para1 = { b:100, c:1, d:2 },
        para2 = { a:1000, b:10000, c:100000 },
        result = Typy.extend(source, para1, para2);
    strictEqual(result.a, para2.a);
    strictEqual(result.b, para2.b);
    strictEqual(result.c, para2.c);
    strictEqual(result.d, para1.d);
});

QUnit.module("Typy Methods");
test("isUndef", function () {
    ok(Typy.isUndef(void 0), "undefined");
    ok(Typy.isUndef({}.prop), "プロパティ実装なし");
    ok(!Typy.isUndef(false), "false");
    ok(!Typy.isUndef(null), "null");
    ok(!Typy.isUndef(""), "空文字列");

    ok(Typy.isUndef(void 0, void 0), "すべて undefined");
    ok(!Typy.isUndef(void 0, null), "一つ以上 undefined 以外");
    ok(!Typy.isUndef(null, void 0), "一つ以上 undefined 以外(先頭)");
});
test("isUnusable", function () {
    ok(Typy.isUnusable(void 0), "undefined");
    ok(Typy.isUnusable(null), "null");
    ok(Typy.isUnusable(NaN), "NaN");
    ok(Typy.isUnusable(Infinity), "Infinity");
    ok(!Typy.isUnusable(""), "空文字列");
    ok(!Typy.isUnusable(false), "false");
    ok(!Typy.isUnusable(true), "true");
    ok(!Typy.isUnusable(0), "数値の0");

    ok(Typy.isUnusable(void 0, null, NaN, Infinity), "すべて unusable");
    ok(!Typy.isUnusable(void 0, 0, null), "一つ以上 not unusable")
});
test("isStr", function () {
    ok(Typy.isStr("foo"), "文字列");
    ok(Typy.isStr(new String("foo")), "Stringオブジェクト");
    ok(Typy.isStr(String("foo")), "String関数");
    ok(Typy.isStr(""), "空文字列");
    ok(!Typy.isStr(1), "数値");
    ok(!Typy.isStr(void 0), "undefined");

    ok(Typy.isStr("abc", "123", "", new String("abc")), "すべて str");
    ok(!Typy.isStr("abc", 123, "", new String("abc")), "すべて str ではない");
});
test("isNum", function () {
    ok(Typy.isNum(0));
    ok(Typy.isNum(1));
    ok(Typy.isNum(new Number(0)));
    ok(Typy.isNum(Number(0)));
    ok(Typy.isNum(NaN));
    ok(Typy.isNum(Infinity));
    ok(!Typy.isNum("1"));

    ok(Typy.isNum(0,1,1.5, Number(100)), "すべて num");
    ok(!Typy.isNum("0",1,1.5, Number(100)), "すべて num ではない");
});
test("isBool", function () {
    ok(Typy.isBool(true));
    ok(Typy.isBool(false));
    ok(!Typy.isBool(0));
    ok(!Typy.isBool(1));
    ok(!Typy.isBool(NaN));
    ok(!Typy.isBool(Infinity));
    ok(!Typy.isBool("true"));
    ok(!Typy.isBool("false"));

    ok(Typy.isBool(true, false), "すべて bool");
    ok(!Typy.isBool(true, false, void 0), "すべて bool ではない");
});
test("isDate", function () {
    ok(Typy.isDate(new Date()));
    ok(!Typy.isDate({}));
    ok(!Typy.isDate("2001/01/01"));

    ok(Typy.isDate(new Date(), new Date()), "すべて date");
    ok(!Typy.isDate(new Date(), new Date(), "2001/01/01"), "すべて date ではない");
});
test("isFunc", function () {
    ok(Typy.isFunc(function () { }), "関数");
    ok(!Typy.isFunc({}), "オブジェクトリテラル");
    ok(Typy.isFunc(new Function("return true;")), "new Function");

    ok(Typy.isFunc(function(){}, new Function("return true")), "すべて func");
    ok(!Typy.isFunc(function(){}, {},  new Function("return true")), "すべて func ではない");
});
test("isArray", function () {
    ok(Typy.isArray([]), "配列リテラル");
    ok(!Typy.isArray(arguments), "arguments");
    ok(!Typy.isArray("foo"), "文字列");

    ok(Typy.isArray([], new Array(0)), "すべて array");
    ok(!Typy.isArray([], new Array(0), "abcde"), "すべて array ではない");
});
test("isPrimitive", function () {
    ok(Typy.isPrimitive("foo"));
    ok(Typy.isPrimitive(123));
    ok(Typy.isPrimitive(true));
    ok(!Typy.isPrimitive(new String("foo")));
    ok(!Typy.isPrimitive(new Number(123)));
    ok(!Typy.isPrimitive(new Boolean(true)));
    ok(!Typy.isPrimitive({}));
    ok(!Typy.isPrimitive(void 0));

    ok(Typy.isPrimitive("foo", 123, true), "すべて primitive");
    ok(!Typy.isPrimitive("foo", new String("A"), 123, true), "すべて primitive ではない");
});
test("isNumeric", function () {
    ok(Typy.isNumeric("-10"), "負数の文字列");
    ok(Typy.isNumeric("0"), "0の文字列");
    ok(Typy.isNumeric("5"), "整数の文字列");
    ok(Typy.isNumeric(-16), "負数");
    ok(Typy.isNumeric(0), "0の数値");
    ok(Typy.isNumeric(32), "整数");
    ok(Typy.isNumeric("040"), "8進数の文字列");
    ok(Typy.isNumeric(040), "8進数の数値");
    ok(Typy.isNumeric("0xFF"), "16進数の文字列");
    ok(Typy.isNumeric(0xFFF), "16進数の数値");
    ok(Typy.isNumeric("-1.6"), "小数点付きの負数の文字列");
    ok(Typy.isNumeric("4.536"), "小数点付きの文字列");
    ok(Typy.isNumeric(-2.6), "小数点付きの負数の数値");
    ok(Typy.isNumeric(3.1415), "小数点付きの数値");
    ok(Typy.isNumeric(8e5), "e 付きの数値");
    ok(Typy.isNumeric("123e-2"), "e 付きの文字列");
    ok(!Typy.isNumeric(""), "空文字列");
    ok(!Typy.isNumeric("123abcd"), "後ろに数値以外の文字列付き");
    ok(!Typy.isNumeric(NaN), "NaN");
    ok(!Typy.isNumeric(Infinity), "Infinitiy");

    ok(Typy.isNumeric("-10", 10, "123e-3"), "すべて numeric");
    ok(!Typy.isNumeric("-10", 10, "123e-3", "123abcd"), "すべて numeric ではない");
});
test("isInt", function(){
    ok(!Typy.isInt("abcde"), "文字列");
    ok(!Typy.isInt("1123"), "数字の文字列");
    ok(Typy.isInt(12332434), "数値");
    ok(!Typy.isInt(12332434.9837), "小数点付き数値");
    ok(Typy.isInt(12332434.000), "小数点以下がすべて0");

    ok(Typy.isInt(123,4545,246), "すべて int");
    ok(!Typy.isInt(123,4545,246,8743.12), "すべて int ではない");
});
test("isNull", function () {
    ok(Typy.isNull(null), "null");
    ok(!Typy.isNull(void 0), "undefined");
    ok(!Typy.isNull(""), "empty string");
    ok(!Typy.isNull(NaN), "NaN");
    ok(!Typy.isNull(0), "0");

    ok(Typy.isNull(null, null, null), "すべて null");
    ok(!Typy.isNull(null, void 0, null), "すべて null ではない");
});

test("isUndefOrNull", function(){
    ok(Typy.isUndefOrNull(void 0), "undefined");
    ok(Typy.isUndefOrNull({}.prop), "プロパティ実装なし");
    ok(!Typy.isUndefOrNull(false), "false");
    ok(!Typy.isUndefOrNull(""), "空文字列");
    ok(Typy.isUndefOrNull(null), "null");
    ok(!Typy.isUndefOrNull(NaN), "NaN");
    ok(!Typy.isUndefOrNull(0), "0");

    ok(Typy.isUndefOrNull(void 0, void 0, null), "すべて undefined/null");
    ok(!Typy.isUndefOrNull(void 0, null, 0), "一つ以上 undefined/null 以外");
});

test("isRegx", function(){

    ok(Typy.isRegx(/abc/g), "リテラル");
    ok(Typy.isRegx(new RegExp("abc")), "new");

    ok(!Typy.isRegx({}), "オブジェクトリテラル");
    ok(!Typy.isRegx("abc"), "文字列");

    ok(Typy.isRegx(/abc/g, new RegExp("abc")), "すべて regex");
    ok(!Typy.isRegx("abc", /abc/g, new RegExp("abc")), "すべて regex ではない");
});

QUnit.module("Typy.func");

test("bind", function () {
    var obj = {};
    var func1 = function () {
        equal(this, obj);
    };
    Typy.func.bind(func1, obj)();
    Typy.func(func1).bind(obj).val()();
    Typy.func(func1).bind(obj).exec();

    var func2 = function (arg1) {
        equal(arg1, "argument");
    };
    Typy.func.bind(func2, undefined, "argument")();
    Typy.func(func2).bind(undefined, "argument").val()();
    Typy.func(func2).bind(undefined, "argument").exec();

    var func3 = function (arg1, arg2) {
        strictEqual(arg1, "argument1");
        strictEqual(arg2, "argument2");
    };
    Typy.func.bind(func3, undefined, "argument1")("argument2");
    Typy.func(func3).bind(undefined, "argument1").val()("argument2");
    Typy.func(func3).bind(undefined, "argument1").exec("argument2");
});

QUnit.module("Typy.str");

test("Typy.str.startsWith", function() {
    ok(Typy.str.startsWith("ABCDE", "AB"), "文字列で先頭一致");
    ok(Typy.str.startsWith("ABCDE", "ab", true), "文字列で大文字小文字区別なしで先頭一致");
    ok(Typy.str.startsWith("ABCDE", /[BA]{2}/), "正規表現で先頭一致");
    ok(Typy.str.startsWith("ABCDE", /[ba]{2}/i), "正規表現で大文字小文字区別なしで先頭一致");
    ok(!Typy.str.startsWith("ABCDE", /[ba]{2}/i, false), "正規表現で指定したignoreCaseよりも引数のignoreCaseが優先される");
});

test("Typy.str.endsWith", function() {
    ok(Typy.str.endsWith("ABCDE", "DE"), "文字列で末尾一致");
    ok(Typy.str.endsWith("ABCDE", "de", true), "文字列で大文字小文字区別なしで末尾一致");
    ok(Typy.str.endsWith("ABCDE", /[ED]{2}/), "正規表現で末尾一致");
    ok(Typy.str.endsWith("ABCDE", /[ed]{2}/i), "正規表現で大文字小文字区別なしで末尾一致");
    ok(!Typy.str.endsWith("ABCDE", /[ed]{2}/i, false), "正規表現で指定したignoreCaseよりも引数のignoreCaseが優先される");
});

test("Typy.str.surroundsWith", function () {

    ok(Typy.str.surroundsWith("{ABCDE}", "{", "}"), "start end 指定");
    ok(!Typy.str.surroundsWith("{ABCDE}", "[", "]"), "start end 指定 ミス");
    ok(Typy.str.surroundsWith("/ABCDE/", "/"), "end が指定されていない場合は start が利用される");
    ok(Typy.str.surroundsWith("ABCDE"), "無指定");
    ok(Typy.str.surroundsWith("ABCDE", "ab", "de", true), "大文字小文字区別なし");
    ok(!Typy.str.surroundsWith("ABCDE", "ab", "de"), "大文字小文字区別あり");
    ok(Typy.str.surroundsWith("ABCDEAB", "ab", true), "大文字小文字区別なし");

    ok(Typy.str.surroundsWith("ABCDEAB", /[BA]{2}/), "正規表現");
    ok(Typy.str.surroundsWith("ABCDEAB", /[ba]{2}/i), "正規表現で大文字小文字区別なし");
    ok(Typy.str.surroundsWith("ABCDE", /[BA]{2}/,/[ED]{2}/), "正規表現で両側指定");
    ok(Typy.str.surroundsWith("ABCDE", /[ab]{2}/i,/[ED]{2}/), "正規表現で両側指定の先頭のみ大文字小文字区別なし");
    ok(!Typy.str.surroundsWith("ABCDE", /[ab]{2}/i,/[ed]{2}/), "正規表現で両側指定の先頭のみ大文字小文字区別なし(2)");
    ok(Typy.str.surroundsWith("ABCDE", /[AB]{2}/,/[ed]{2}/i), "正規表現で両側指定の末尾のみ大文字小文字区別なし");
    ok(!Typy.str.surroundsWith("ABCDE", /[ab]{2}/,/[ed]{2}/i), "正規表現で両側指定の末尾のみ大文字小文字区別なし(2)");
});

test("Typy.str.format", function () {

    strictEqual(Typy.str.format("{0}-{1}-{2}-{{2}}", "foo", "bar", "baz"), "foo-bar-baz-{2}", "数値での指定");
    strictEqual(Typy.str.format("{A}-{B}-{C}-{{C}}", {A:"foo", B:"bar", C:"baz"}), "foo-bar-baz-{C}", "プロパティ名での指定");
    strictEqual(Typy.str.format("{A}-{B}-{1}", {A:"foo", B:"bar"}, "baz"), "foo-bar-baz", "数値とプロパティ名での指定");
    strictEqual(Typy.str.format("{0}-{1}-{2}", ["foo", "bar", "baz"]), "foo-bar-baz", "単一の配列を引数にして指定");

    strictEqual(Typy.str("{0}-{1}-{2}-{{2}}").format("foo", "bar", "baz").val(), "foo-bar-baz-{2}", "Typy.str() 数値での指定");
    strictEqual(Typy.str("{A}-{B}-{C}-{{C}}").format({A:"foo", B:"bar", C:"baz"}).val(), "foo-bar-baz-{C}", "Typy.str() プロパティ名での指定");
    strictEqual(Typy.str("{A}-{B}-{1}").format({A:"foo", B:"bar"}, "baz").val(), "foo-bar-baz", "Typy.str() 数値とプロパティ名での指定");
    strictEqual(Typy.str("{0}-{1}-{2}").format(["foo", "bar", "baz"]).val(), "foo-bar-baz", "Typy.str() 単一の配列を引数にして指定");

    strictEqual(Typy.str.format("{Foo[0].A}", {Foo:[
        {A:"foo", B:"bar"},
        {A:"baz"}
    ]}), "foo", "ネストしたプロパティを取得できる");

});
test("Typy.str.lower", function () {
    strictEqual(Typy.str.lower("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), "abcdefghijklmnopqrstuvwxyz", "アルファベット");
    strictEqual(Typy.str.lower("!\"#$%&'()=-^~\\|@`[{;+:*]},<.>/?_"), "!\"#$%&'()=-^~\\|@`[{;+:*]},<.>/?_", "記号");
    strictEqual(Typy.str.lower("あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやいゆいぇよらりるれろわをんぁぃぅぇぉ"),
        "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやいゆいぇよらりるれろわをんぁぃぅぇぉ", "全角");

    strictEqual(Typy.str("ABCDEFGHIJKLMNOPQRSTUVWXYZ").lower().val(), "abcdefghijklmnopqrstuvwxyz", "アルファベット");
    strictEqual(Typy.str("!\"#$%&'()=-^~\\|@`[{;+:*]},<.>/?_").lower().val(), "!\"#$%&'()=-^~\\|@`[{;+:*]},<.>/?_", "記号");
    strictEqual(Typy.str("あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやいゆいぇよらりるれろわをんぁぃぅぇぉ").lower().val(),
        "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやいゆいぇよらりるれろわをんぁぃぅぇぉ", "全角");
});
test("Typy.str.upper", function () {
    strictEqual(Typy.str.upper("abcdefghijklmnopqrstuvwxyz"), "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "アルファベット");
    strictEqual(Typy.str.upper("!\"#$%&'()=-^~\\|@`[{;+:*]},<.>/?_"), "!\"#$%&'()=-^~\\|@`[{;+:*]},<.>/?_", "記号");
    strictEqual(Typy.str.upper("あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやいゆいぇよらりるれろわをんぁぃぅぇぉ"),
        "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやいゆいぇよらりるれろわをんぁぃぅぇぉ", "全角");

    strictEqual(Typy.str("abcdefghijklmnopqrstuvwxyz").upper().val(), "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "アルファベット");
    strictEqual(Typy.str("!\"#$%&'()=-^~\\|@`[{;+:*]},<.>/?_").upper().val(), "!\"#$%&'()=-^~\\|@`[{;+:*]},<.>/?_", "記号");
    strictEqual(Typy.str("あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやいゆいぇよらりるれろわをんぁぃぅぇぉ").upper().val(),
        "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやいゆいぇよらりるれろわをんぁぃぅぇぉ", "全角");
});
test("Typy.str.repeat", function () {
    strictEqual(Typy.str.repeat("123", 3), "123123123", "整数");
    strictEqual(Typy.str.repeat("123", -1), "", "負数");
    strictEqual(Typy.str.repeat("123", 0), "", "0");
    strictEqual(Typy.str.repeat("123", 5.5), "123123123123123", "少数は切り捨てられる");
    strictEqual(Typy.str.repeat("123", "4.2"), "123123123123", "数値に直せる文字列");

    strictEqual(Typy.str("123").repeat(3).val(), "123123123", "整数");
    strictEqual(Typy.str("123").repeat(-1).val(), "", "負数");
    strictEqual(Typy.str("123").repeat(0).val(), "", "0");
    strictEqual(Typy.str("123").repeat(5.5).val(), "123123123123123", "少数は切り捨てられる");
    strictEqual(Typy.str("123").repeat("4.2").val(), "123123123123", "数値に直せる文字列");
});
test("Typy.str.padLeft", function () {
    strictEqual(Typy.str.padLeft("12345", 8, "ABC"), "ABC12345", "デフォルト");
    strictEqual(Typy.str.padLeft("12345", 10, "ABC"), "ABCAB12345", "付加文字が余る場合は、先頭から設定されている");
    strictEqual(Typy.str.padLeft("12345", 10.5, "ABC"), "ABCAB12345", "小数点は切り捨てられる");
    strictEqual(Typy.str.padLeft("12345", "9.2", "ABC"), "ABCA12345", "数値に直せる文字列");
    strictEqual(Typy.str.padLeft("12345", 9, 0.123), "0.1212345", "付加文字が文字列以外");
    strictEqual(Typy.str.padLeft("12345", 9), "    12345", "指定されていない場合は、半角スペース");
    strictEqual(Typy.str.padLeft("12345", 9, void 0), "    12345", "undefined の場合は、半角スペース");
    strictEqual(Typy.str.padLeft("12345", 9, null), "    12345", "null の場合は、半角スペース");

    strictEqual(Typy.str("12345").padLeft(8, "ABC").val(), "ABC12345", "デフォルト");
    strictEqual(Typy.str("12345").padLeft(10, "ABC").val(), "ABCAB12345", "付加文字が余る場合は、先頭から設定されている");
    strictEqual(Typy.str("12345").padLeft(10.5, "ABC").val(), "ABCAB12345", "小数点は切り捨てられる");
    strictEqual(Typy.str("12345").padLeft("9.2", "ABC").val(), "ABCA12345", "数値に直せる文字列");
    strictEqual(Typy.str("12345").padLeft(9, 0.123).val(), "0.1212345", "付加文字が文字列以外");
    strictEqual(Typy.str("12345").padLeft(9).val(), "    12345", "指定されていない場合は、半角スペース");
    strictEqual(Typy.str("12345").padLeft(9, void 0).val(), "    12345", "undefined の場合は、半角スペース");
    strictEqual(Typy.str("12345").padLeft(9, null).val(), "    12345", "null の場合は、半角スペース");
});
test("Typy.str.padRight", function () {
    strictEqual(Typy.str.padRight("12345", 8, "ABC"), "12345ABC", "デフォルト");
    strictEqual(Typy.str.padRight("12345", 10, "ABC"), "12345ABCAB", "付加文字が余る場合は、先頭から設定されている");
    strictEqual(Typy.str.padRight("12345", 10.5, "ABC"), "12345ABCAB", "小数点は切り捨てられる");
    strictEqual(Typy.str.padRight("12345", "9.2", "ABC"), "12345ABCA", "数値に直せる文字列");
    strictEqual(Typy.str.padRight("12345", 9, 0.123), "123450.12", "付加文字が文字列以外");
    strictEqual(Typy.str.padRight("12345", 9), "12345    ", "指定されていない場合は、半角スペース");
    strictEqual(Typy.str.padRight("12345", 9, void 0), "12345    ", "undefined の場合は、半角スペース");
    strictEqual(Typy.str.padRight("12345", 9, null), "12345    ", "null の場合は、半角スペース");

    strictEqual(Typy.str("12345").padRight(8, "ABC").val(), "12345ABC", "デフォルト");
    strictEqual(Typy.str("12345").padRight(10, "ABC").val(), "12345ABCAB", "付加文字が余る場合は、先頭から設定されている");
    strictEqual(Typy.str("12345").padRight(10.5, "ABC").val(), "12345ABCAB", "小数点は切り捨てられる");
    strictEqual(Typy.str("12345").padRight("9.2", "ABC").val(), "12345ABCA", "数値に直せる文字列");
    strictEqual(Typy.str("12345").padRight(9, 0.123).val(), "123450.12", "付加文字が文字列以外");
    strictEqual(Typy.str("12345").padRight(9).val(), "12345    ", "指定されていない場合は、半角スペース");
    strictEqual(Typy.str("12345").padRight(9, void 0).val(), "12345    ", "undefined の場合は、半角スペース");
    strictEqual(Typy.str("12345").padRight(9, null).val(), "12345    ", "null の場合は、半角スペース");
});
test("Typy.str.surrounds", function () {
    strictEqual(Typy.str.surrounds("12345", "{", "}"), "{12345}", "start end 指定");
    strictEqual(Typy.str.surrounds("12345", "{[(", ")]}"), "{[(12345)]}", "start end 複数指定");
    strictEqual(Typy.str.surrounds("12345", 987, 987), "98712345987", "start end 数値指定");
    strictEqual(Typy.str.surrounds("12345", "!"), "!12345!", "start のみ指定");
    strictEqual(Typy.str.surrounds("12345", "!/"), "!/12345!/", "start のみ複数指定");
    strictEqual(Typy.str.surrounds("12345"), "12345", "無指定");
    strictEqual(Typy.str.surrounds("12345", "12", "45"), "12345", "既定ではすでに囲まれている場合は囲まない");
    strictEqual(Typy.str.surrounds("12345", "12", "45", true), "121234545", "force が指定されている場合は囲む");
    strictEqual(Typy.str.surrounds("12345", "12", true), "121234512", "end の位置に bool が指定されている場合は force として扱う 1");
    strictEqual(Typy.str.surrounds("12345", "12", false), "1234512", "end の位置に bool が指定されている場合は force として扱う 2");

    strictEqual(Typy.str("12345").surrounds("{", "}").val(), "{12345}", "start end 指定");
    strictEqual(Typy.str("12345").surrounds("{[(", ")]}").val(), "{[(12345)]}", "start end 複数指定");
    strictEqual(Typy.str("12345").surrounds(987, 987).val(), "98712345987", "start end 数値指定");
    strictEqual(Typy.str("12345").surrounds("!").val(), "!12345!", "start のみ指定");
    strictEqual(Typy.str("12345").surrounds("!/").val(), "!/12345!/", "start のみ複数指定");
    strictEqual(Typy.str("12345").surrounds().val(), "12345", "無指定");
    strictEqual(Typy.str("12345").surrounds("12", "45").val(), "12345", "既定ではすでに囲まれている場合は囲まない");
    strictEqual(Typy.str("12345").surrounds("12", "45", true).val(), "121234545", "force が指定されている場合は囲む");
    strictEqual(Typy.str("12345").surrounds("12", true).val(), "121234512", "end の位置に bool が指定されている場合は force として扱う 1");
    strictEqual(Typy.str("12345").surrounds("12", false).val(), "1234512", "end の位置に bool が指定されている場合は force として扱う 2");
});

QUnit.module("Typy.array");
test("indexOf", function () {
    var target = ["A", "B", "C", "D", "E", "A", "B", "C", "D", "E"];
    strictEqual(Typy.array.indexOf(target, "B"), 1, "先頭からの指定");
    strictEqual(Typy.array.indexOf(target, "B", 3), 6, "開始位置の指定");
    strictEqual(Typy.array.indexOf(target, "Z"), -1, "存在しない値");
    strictEqual(Typy.array.indexOf(target, "B", 20), -1, "開始位置が存在しない位置");

    strictEqual(Typy.array(target).indexOf("B"), 1, "Typy.array() 先頭からの指定");
    strictEqual(Typy.array(target).indexOf("B", 3), 6, "Typy.array() 開始位置の指定");
    strictEqual(Typy.array(target).indexOf("Z"), -1, "Typy.array() 存在しない値");
    strictEqual(Typy.array(target).indexOf("B", 20), -1, "Typy.array() 開始位置が存在しない位置");
});

test("lastIndexOf", function () {
    var target = ["A", "B", "C", "D", "E", "A", "B", "C", "D", "E"];
    strictEqual(Typy.array.lastIndexOf(target, "B"), 6, "最後からの指定");
    strictEqual(Typy.array.lastIndexOf(target, "B", 5), 1, "開始位置の指定");
    strictEqual(Typy.array.lastIndexOf(target, "Z"), -1, "存在しない値");
    strictEqual(Typy.array.lastIndexOf(target, "B", 20), 6, "開始位置が存在しない位置");

    strictEqual(Typy.array(target).lastIndexOf("B"), 6, "Typy.array() 最後からの指定");
    strictEqual(Typy.array(target).lastIndexOf("B", 5), 1, "Typy.array() 開始位置の指定");
    strictEqual(Typy.array(target).lastIndexOf("Z"), -1, "Typy.array() 存在しない値");
    strictEqual(Typy.array(target).lastIndexOf("B", 20), 6, "Typy.array() 開始位置が存在しない位置");
});

var arrayAddPushTest = function (name) {
    deepEqual(Typy.array[name](["A", "B", "C", "D", "E"], 1), ["A", "B", "C", "D", "E", 1], "1件追加");
    deepEqual(Typy.array[name](["A", "B", "C", "D", "E"], 1, 2, 3), ["A", "B", "C", "D", "E", 1, 2, 3], "複数件追加");
    deepEqual(Typy.array[name](["A", "B", "C", "D", "E"], [1, 2, 3]), ["A", "B", "C", "D", "E", 1, 2, 3], "1件の配列を追加");
    deepEqual(Typy.array[name](["A", "B", "C", "D", "E"], [1, 2, 3], [9, 8, 7]), ["A", "B", "C", "D", "E", [1, 2, 3], [9, 8, 7]], "複数件で1件目を配列で追加");
    equal(Typy.array[name](), undefined, "引数なしの場合は undefined");
    equal(Typy.array[name]("AA", 1), undefined, "先頭引数がArray以外は undefined 1");
    equal(Typy.array[name](123, 1), undefined, "先頭引数がArray以外は undefined 2");
    equal(Typy.array[name]({A:1, B:2, C:3}, 1), undefined, "先頭引数がArray以外は undefined 3");

    var target = ["A", "B"];
    var dest = Typy.array[name](target, "C");
    dest.push("D");
    equal(target, dest, "戻り値には引数とおなじ配列のインスタンスが返される");

    target = ["A", "B", "C", "D", "E"];
    dest = Typy.array(target)[name](1, 2, 3).val();
    notEqual(target, dest, "TypyArray化したときは別のインスタンスが返される");
    notDeepEqual(target, dest, "別インスタンスなので影響を及ぼさない");
};

test("add", function () {
    arrayAddPushTest("add");
});

test("push", function () {
    arrayAddPushTest("push");
});

test("insert", function () {
    deepEqual(Typy.array.insert(["A", "B", "C", "D", "E"], 2, "-"), ["A", "B", "-", "C", "D", "E"], "途中に1件追加");
    deepEqual(Typy.array.insert(["A", "B", "C", "D", "E"], 3, "-", "/", "?"), ["A", "B", "C", "-", "/", "?", "D", "E"], "途中に複数件追加");
    deepEqual(Typy.array.insert(["A", "B", "C", "D", "E"], 4, ["-", "/", "?"]), ["A", "B", "C", "D", "-", "/", "?", "E"], "途中に配列で1件追加");
    deepEqual(Typy.array.insert(["A", "B", "C", "D", "E"], 4, ["-", "/", "?"], "Z"), ["A", "B", "C", "D", ["-", "/", "?"], "Z", "E"], "途中に複数件で1件目を配列で追加");
    deepEqual(Typy.array.insert(["A", "B", "C", "D", "E"], -1, "Z"), ["A", "B", "C", "D", "Z", "E"], "負数を指定した場合は後ろを0として計算した位置に追加される。内部の動きに依存");
    deepEqual(Typy.array.insert(["A", "B", "C", "D", "E"], -10, "Z"), ["Z", "A", "B", "C", "D", "E"], "件数以上の負数を指定した場合、先頭になる。内部の動きに依存");
    deepEqual(Typy.array.insert(["A", "B", "C", "D", "E"], 10, "Z"), [ "A", "B", "C", "D", "E", "Z"], "件数以上を指定した場合、末尾になる。内部の動きに依存");
    equal(Typy.array.insert(), undefined, "引数なしの場合は undefined");
    equal(Typy.array.insert("AA", 1), undefined, "先頭引数がArray以外は undefined 1");
    equal(Typy.array.insert(123, 1), undefined, "先頭引数がArray以外は undefined 2");
    equal(Typy.array.insert({A:1, B:2, C:3}, 1), undefined, "先頭引数がArray以外は undefined 3");
    equal(Typy.array.insert(["A", "B", "C", "D", "E"]), undefined, "第２引数なしの場合は undefined");
    deepEqual(Typy.array.insert(["A", "B", "C", "D", "E"], 2), ["A", "B", "C", "D", "E"], "値の引数なしの時には変化なし");
    deepEqual(Typy.array.insert(["A", "B", "C", "D", "E"], 2, void 0), ["A", "B", undefined, "C", "D", "E"], "値の引数が undefined の場合は、undefinedが設定される");
});

test("each", function () {
    expect(8);
    var target = ["a", "b", "c", "d", "e"];
    Typy.array.each(target, function (item, i, source) {
        strictEqual(item.charCodeAt() - 97, i);
    });
    var obj = {};
    target = [1];
    var res = Typy.array.each(target, function (item, i, source) {
        equal(source, target);
        equal(this, obj);
    }, obj);
    equal(res, target);
});

test("Typy.array() each", function () {
    expect(7);
    var target = ["a", "b", "c", "d", "e"];
    Typy.array(target).each(function (item, i, source) {
        strictEqual(item.charCodeAt() - 97, i);
    });
    var obj = {};
    target = [1];
    Typy.array(target).each(function (item, i, source) {
        //TypyArrayのインスタンス化時にコピーされた別オブジェクトになる
        notEqual(source, target);
        equal(this, obj);
    }, obj);
});

test("map", function () {
    expect(5);
    var target = ["a", "b", "c", "d", "e"],
        res = Typy.array.map(target, function (item, i, source) {
            return item.charCodeAt();
        }),
        i, l;
    for (i = 0, l = res.length; i < l; i++) {
        strictEqual(res[i], 97 + i);
    }
});

test("Typy.array() map", function () {
    expect(6);
    var target = ["a", "b", "c", "d", "e"],
        res = Typy.array(target).map(function (item, i, source) {
            return item.charCodeAt();
        }).each(function (item, i, source) {
                strictEqual(item, 97 + i);
            }).val();
    strictEqual(res.length, 5);
});

test("filter", function () {
    expect(3);
    var target = ["a", "b", "c", "d", "e"],
        res = Typy.array.filter(target, function (item, i, source) {
            return (item.charCodeAt() % 2) === 0;
        });
    strictEqual(res.length, 2);
    strictEqual(res[0], "b");
    strictEqual(res[1], "d");
});

test("Typy.array() filter", function () {
    expect(3);
    var target = ["a", "b", "c", "d", "e"],
        res = Typy.array(target).filter(function (item, i, source) {
            return (item.charCodeAt() % 2) === 0;
        }).val();
    strictEqual(res.length, 2);
    strictEqual(res[0], "b");
    strictEqual(res[1], "d");
});

test("every", function () {
    ok(Typy.array.every([2, 4, 6, 8, 10], function (item, i) {
        return (item % 2) === 0;
    }), "全て true");
    ok(!Typy.array.every([2, 4, 6, 8, 10, 11], function (item, i) {
        return (item % 2) === 0;
    }), "一つだけ false");
});

test("Typy.array() every", function () {
    ok(Typy.array([2, 4, 6, 8, 10]).every(function (item, i) {
        return (item % 2) === 0;
    }), "全て true");
    ok(!Typy.array([2, 4, 6, 8, 10, 11]).every(function (item, i) {
        return (item % 2) === 0;
    }), "一つだけ false");
});

test("some", function () {
    ok(Typy.array.some([2, 4, 6, 7, 8, 10], function (item, i) {
        return (item % 2) !== 0;
    }), "一つだけ true");
    ok(!Typy.array.some([2, 4, 6, 8, 10], function (item, i) {
        return (item % 2) !== 0;
    }), "全て false");
});

test("Typy.array() some", function () {
    ok(Typy.array([2, 4, 6, 7, 8, 10]).some(function (item, i) {
        return (item % 2) !== 0;
    }), "一つだけ true");
    ok(!Typy.array([2, 4, 6, 8, 10]).some(function (item, i) {
        return (item % 2) !== 0;
    }), "全て false");
});

test("take", function () {
    var ary = ["A", "B", "C", "D", "E"];
    var result = Typy.array.take(ary, 3);
    strictEqual(ary[0], result[0]);
    strictEqual(ary[1], result[1]);
    strictEqual(ary[2], result[2]);
    strictEqual(result.length, 3);

    result = Typy.array.take(ary, 6);
    strictEqual(ary[0], result[0]);
    strictEqual(ary[1], result[1]);
    strictEqual(ary[2], result[2]);
    strictEqual(ary[3], result[3]);
    strictEqual(ary[4], result[4]);
    strictEqual(result.length, 5);

    result = Typy.array.take(ary, -1);
    strictEqual(ary[0], result[0]);
    strictEqual(ary[1], result[1]);
    strictEqual(ary[2], result[2]);
    strictEqual(ary[3], result[3]);
    strictEqual(result.length, 4);

    result = Typy.array.take(ary, -6);
    strictEqual(result.length, 0);
});

test("skip", function () {
    var ary = ["A", "B", "C", "D", "E"];
    var result = Typy.array.skip(ary, 3);
    strictEqual(ary[3], result[0]);
    strictEqual(ary[4], result[1]);
    strictEqual(result.length, 2);

    result = Typy.array.skip(ary, 6);
    strictEqual(result.length, 0);

    result = Typy.array.skip(ary, -3);
    strictEqual(ary[2], result[0]);
    strictEqual(ary[3], result[1]);
    strictEqual(ary[4], result[2]);
    strictEqual(result.length, 3);

    result = Typy.array.skip(ary, -6);
    strictEqual(ary[0], result[0]);
    strictEqual(ary[1], result[1]);
    strictEqual(ary[2], result[2]);
    strictEqual(ary[3], result[3]);
    strictEqual(ary[4], result[4]);
    strictEqual(result.length, 5);
});

test("reduce", function(){

    var result = {val: 0};
    Typy.array([1,2,3]).reduce(function(prev, current){
        result.val += current;
    }, 0);
    strictEqual(result.val, 6);

});

QUnit.module("Typy.lang");

test("text", function(){

    Typy.lang.current("en");
    Typy.lang.text({
        greeting: "Hello",
        selfintro: "My name is ..."
    });
    Typy.lang.current("ja");
    Typy.lang.text({
        greeting: "こんにちは"
    });

    Typy.lang.current("en");
    strictEqual(Typy.lang.text().greeting, "Hello");
    strictEqual(Typy.lang.text().selfintro, "My name is ...");

    Typy.lang.current("ja");
    strictEqual(Typy.lang.text().greeting, "こんにちは");
    strictEqual(Typy.lang.text().selfintro, undefined);

});

QUnit.module("Typy.date");

var checkDate = function (expect, actual) {
    return expect.getFullYear() === actual[0] &&
        expect.getMonth() === actual[1] &&
        expect.getDate() === actual[2] &&
        expect.getHours() === actual[3] &&
        expect.getMinutes() === actual[4] &&
        expect.getSeconds() === actual[5] &&
        expect.getMilliseconds() === actual[6];
};

Typy.lang.current("ja");
Typy.lang.calendar({
    months:{
        shortNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
        names:["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"]
    },
    meridiem:{
        shortNames:{ ante:"午前", post:"午後" },
        names:{ ante:"午前", post:"午後" }
    },
    weekdays:{
        shortNames:["日", "月", "火", "水", "木", "金", "土"],
        names:["日曜日", "月曜日", "火曜日", "水曜日", "木曜日", "金曜日", "土曜日"]
    }
});
Typy.lang.current("en");

test("parse year", function () {
    var now = new Date();

    ok(checkDate(Typy.date.parse("01", "y"), [2001, 0, 1, 0, 0, 0, 0]), "y 2桁");
    ok(checkDate(Typy.date.parse("1", "y"), [2001, 0, 1, 0, 0, 0, 0]), "y 1桁");
    ok(checkDate(Typy.date.parse("06", "yy"), [2006, 0, 1, 0, 0, 0, 0]), "yy 2桁");

    ok(checkDate(Typy.date.parse("00", "y"), [2000, 0, 1, 0, 0, 0, 0]), "y 2桁 0");
    ok(checkDate(Typy.date.parse("0", "y"), [2000, 0, 1, 0, 0, 0, 0]), "y 1桁 0");
    ok(checkDate(Typy.date.parse("00", "yy"), [2000, 0, 1, 0, 0, 0, 0]), "yy 2桁 0");

    ok(checkDate(Typy.date.parse("010", "yyy"), [10, 0, 1, 0, 0, 0, 0]), "yyy 先頭0埋め2桁");
    ok(checkDate(Typy.date.parse("2010", "yyyy"), [2010, 0, 1, 0, 0, 0, 0]), "yyyy");
    ok(checkDate(Typy.date.parse("0010", "yyyy"), [10, 0, 1, 0, 0, 0, 0]), "yyyy 先頭0埋め2桁");

    // 0 年を許可するかどうかを検討(JavaScriptとしてはOKのようなので許可している)
    ok(checkDate(Typy.date.parse("000", "yyy"), [0, 0, 1, 0, 0, 0, 0]), "yyy 0年");
    ok(checkDate(Typy.date.parse("0000", "yyyy"), [0, 0, 1, 0, 0, 0, 0]), "yyyy 0年");

    ok(checkDate(Typy.date.parse("30", "yy"), [1930, 0, 1, 0, 0, 0, 0]), "yy 2桁 30年");
    ok(checkDate(Typy.date.parse("30", "y"), [1930, 0, 1, 0, 0, 0, 0]), "y 2桁 30年");
    ok(checkDate(Typy.date.parse("29", "yy"), [2029, 0, 1, 0, 0, 0, 0]), "yy 2桁 29年");
    ok(checkDate(Typy.date.parse("29", "y"), [2029, 0, 1, 0, 0, 0, 0]), "y 2桁 29年");

    ok(checkDate(Typy.date("01", "y").val(), [2001, 0, 1, 0, 0, 0, 0]), "Typy.date() y 2桁");
    ok(checkDate(Typy.date("1", "y").val(), [2001, 0, 1, 0, 0, 0, 0]), "Typy.date() y 1桁");
    ok(checkDate(Typy.date("06", "yy").val(), [2006, 0, 1, 0, 0, 0, 0]), "Typy.date() yy 2桁");

    ok(checkDate(Typy.date("00", "y").val(), [2000, 0, 1, 0, 0, 0, 0]), "Typy.date() y 2桁 0");
    ok(checkDate(Typy.date("0", "y").val(), [2000, 0, 1, 0, 0, 0, 0]), "Typy.date() y 1桁 0");
    ok(checkDate(Typy.date("00", "yy").val(), [2000, 0, 1, 0, 0, 0, 0]), "Typy.date() yy 2桁 0");

    ok(checkDate(Typy.date("010", "yyy").val(), [10, 0, 1, 0, 0, 0, 0]), "Typy.date() yyy 先頭0埋め2桁");
    ok(checkDate(Typy.date("2010", "yyyy").val(), [2010, 0, 1, 0, 0, 0, 0]), "Typy.date() yyyy");
    ok(checkDate(Typy.date("0010", "yyyy").val(), [10, 0, 1, 0, 0, 0, 0]), "Typy.date() yyyy 先頭0埋め2桁");

    // 0 年を許可するかどうかを検討(JavaScriptとしてはOKのようなので許可している)
    ok(checkDate(Typy.date("000", "yyy").val(), [0, 0, 1, 0, 0, 0, 0]), "Typy.date() yyy 0年");
    ok(checkDate(Typy.date("0000", "yyyy").val(), [0, 0, 1, 0, 0, 0, 0]), "Typy.date() yyyy 0年");

    ok(checkDate(Typy.date("30", "yy").val(), [1930, 0, 1, 0, 0, 0, 0]), "Typy.date() yy 2桁 30年");
    ok(checkDate(Typy.date("30", "y").val(), [1930, 0, 1, 0, 0, 0, 0]), "Typy.date() y 2桁 30年");
    ok(checkDate(Typy.date("29", "yy").val(), [2029, 0, 1, 0, 0, 0, 0]), "Typy.date() yy 2桁 29年");
    ok(checkDate(Typy.date("29", "y").val(), [2029, 0, 1, 0, 0, 0, 0]), "Typy.date() y 2桁 29年");
});
test("parse month", function () {
    var now = new Date();

    Typy.lang.current("en");

    ok(checkDate(Typy.date.parse("06", "M"), [now.getFullYear(), 5, 1, 0, 0, 0, 0]), "M 2桁 先頭0埋め");
    ok(checkDate(Typy.date.parse("1", "M"), [now.getFullYear(), 0, 1, 0, 0, 0, 0]), "M 1桁");
    ok(checkDate(Typy.date.parse("11", "M"), [now.getFullYear(), 10, 1, 0, 0, 0, 0]), "M 2桁");
    ok(checkDate(Typy.date.parse("06", "MM"), [now.getFullYear(), 5, 1, 0, 0, 0, 0]), "MM 2桁 先頭0埋め");
    ok(checkDate(Typy.date.parse("11", "MM"), [now.getFullYear(), 10, 1, 0, 0, 0, 0]), "MM 2桁");

    ok(checkDate(Typy.date.parse("Feb", "MMM"), [now.getFullYear(), 1, 1, 0, 0, 0, 0]), "MMM");
    ok(checkDate(Typy.date.parse("February", "MMMM"), [now.getFullYear(), 1, 1, 0, 0, 0, 0]), "MMMM");

    equal(Typy.date.parse("0", "M"), undefined, "M 1桁 1-12以外");
    equal(Typy.date.parse("13", "M"), undefined, "M 2桁 1-12以外");
    equal(Typy.date.parse("00", "MM"), undefined, "MM 2桁 00");
    equal(Typy.date.parse("13", "MM"), undefined, "MM 2桁 1-12以外");

    ok(checkDate(Typy.date("06", "M").val(), [now.getFullYear(), 5, 1, 0, 0, 0, 0]), "Typy.date() M 2桁 先頭0埋め");
    ok(checkDate(Typy.date("1", "M").val(), [now.getFullYear(), 0, 1, 0, 0, 0, 0]), "Typy.date() M 1桁");
    ok(checkDate(Typy.date("11", "M").val(), [now.getFullYear(), 10, 1, 0, 0, 0, 0]), "Typy.date() M 2桁");
    ok(checkDate(Typy.date("06", "MM").val(), [now.getFullYear(), 5, 1, 0, 0, 0, 0]), "Typy.date() MM 2桁 先頭0埋め");
    ok(checkDate(Typy.date("11", "MM").val(), [now.getFullYear(), 10, 1, 0, 0, 0, 0]), "Typy.date() MM 2桁");
    equal(Typy.date("0", "M").val(), undefined, "Typy.date() M 1桁 1-12以外");
    equal(Typy.date("13", "M").val(), undefined, "Typy.date() M 2桁 1-12以外");
    equal(Typy.date("00", "MM").val(), undefined, "Typy.date() MM 2桁 00");
    equal(Typy.date("13", "MM").val(), undefined, "Typy.date() MM 2桁 1-12以外");

    Typy.lang.current("ja");
    ok(checkDate(Typy.date.parse("2", "MMM"), [now.getFullYear(), 1, 1, 0, 0, 0, 0]), "ja MMM");
    ok(checkDate(Typy.date.parse("2月", "MMMM"), [now.getFullYear(), 1, 1, 0, 0, 0, 0]), "ja MMMM");

    Typy.lang.current("en");
});
test("parse date", function () {
    var now = new Date();
    ok(checkDate(Typy.date.parse("06", "d"), [now.getFullYear(), 0, 6, 0, 0, 0, 0]), "d 2桁 先頭0埋め");
    ok(checkDate(Typy.date.parse("1", "d"), [now.getFullYear(), 0, 1, 0, 0, 0, 0]), "d 1桁");
    ok(checkDate(Typy.date.parse("11", "d"), [now.getFullYear(), 0, 11, 0, 0, 0, 0]), "d 2桁");
    ok(checkDate(Typy.date.parse("06", "dd"), [now.getFullYear(), 0, 6, 0, 0, 0, 0]), "dd 2桁 先頭0埋め");
    ok(checkDate(Typy.date.parse("11", "dd"), [now.getFullYear(), 0, 11, 0, 0, 0, 0]), "dd 2桁");
    equal(Typy.date.parse("0", "d"), undefined, "d 1桁 0");
    equal(Typy.date.parse("32", "d"), undefined, "d 2桁 月末日より大きい");
    equal(Typy.date.parse("00", "dd"), undefined, "dd 2桁 00");
    equal(Typy.date.parse("32", "dd"), undefined, "dd 2桁 月末日より大きい");

    ok(checkDate(Typy.date("06", "d").val(), [now.getFullYear(), 0, 6, 0, 0, 0, 0]), "Typy.date() d 2桁 先頭0埋め");
    ok(checkDate(Typy.date("1", "d").val(), [now.getFullYear(), 0, 1, 0, 0, 0, 0]), "Typy.date() d 1桁");
    ok(checkDate(Typy.date("11", "d").val(), [now.getFullYear(), 0, 11, 0, 0, 0, 0]), "Typy.date() d 2桁");
    ok(checkDate(Typy.date("06", "dd").val(), [now.getFullYear(), 0, 6, 0, 0, 0, 0]), "Typy.date() dd 2桁 先頭0埋め");
    ok(checkDate(Typy.date("11", "dd").val(), [now.getFullYear(), 0, 11, 0, 0, 0, 0]), "Typy.date() dd 2桁");
    equal(Typy.date("0", "d").val(), undefined, "Typy.date() d 1桁 0");
    equal(Typy.date("32", "d").val(), undefined, "Typy.date() d 2桁 月末日より大きい");
    equal(Typy.date("00", "dd").val(), undefined, "Typy.date() dd 2桁 00");
    equal(Typy.date("32", "dd").val(), undefined, "Typy.date() dd 2桁 月末日より大きい");
});
test("parse hours", function () {
    var now = new Date();
    ok(checkDate(Typy.date.parse("06", "h"), [now.getFullYear(), now.getMonth(), now.getDate(), 6, 0, 0, 0]), "h 2桁 先頭0埋め");
    ok(checkDate(Typy.date.parse("1", "h"), [now.getFullYear(), now.getMonth(), now.getDate(), 1, 0, 0, 0]), "h 1桁");
    ok(checkDate(Typy.date.parse("11", "h"), [now.getFullYear(), now.getMonth(), now.getDate(), 11, 0, 0, 0]), "h 2桁");
    ok(checkDate(Typy.date.parse("06", "hh"), [now.getFullYear(), now.getMonth(), now.getDate(), 6, 0, 0, 0]), "hh 2桁 先頭0埋め");
    ok(checkDate(Typy.date.parse("0", "h"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "h 1桁 0");
    ok(checkDate(Typy.date.parse("00", "h"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "h 2桁 0");
    ok(checkDate(Typy.date.parse("00", "hh"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "hh 2桁 0");
    ok(checkDate(Typy.date.parse("12", "h"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "h 2桁 12");
    ok(checkDate(Typy.date.parse("12", "hh"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "hh 2桁 12");
    equal(Typy.date.parse("13", "h"), undefined, "h 2桁 13以上");
    equal(Typy.date.parse("13", "hh"), undefined, "hh 2桁 13以上");

    ok(checkDate(Typy.date("06", "h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 6, 0, 0, 0]), "Typy.date() h 2桁 先頭0埋め");
    ok(checkDate(Typy.date("1", "h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 1, 0, 0, 0]), "Typy.date() h 1桁");
    ok(checkDate(Typy.date("11", "h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 11, 0, 0, 0]), "Typy.date() h 2桁");
    ok(checkDate(Typy.date("06", "hh").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 6, 0, 0, 0]), "Typy.date() hh 2桁 先頭0埋め");
    ok(checkDate(Typy.date("0", "h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() h 1桁 0");
    ok(checkDate(Typy.date("00", "h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() h 2桁 0");
    ok(checkDate(Typy.date("00", "hh").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() hh 2桁 0");
    ok(checkDate(Typy.date("12", "h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() h 2桁 12");
    ok(checkDate(Typy.date("12", "hh").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() hh 2桁 12");
    equal(Typy.date("13", "h").val(), undefined, "Typy.date() h 2桁 13以上はNG");
    equal(Typy.date("13", "hh").val(), undefined, "Typy.date() hh 2桁 13以上はNG");
});

test("parse military hours", function () {
    var now = new Date();
    ok(checkDate(Typy.date.parse("06", "H"), [now.getFullYear(), now.getMonth(), now.getDate(), 6, 0, 0, 0]), "H 2桁 先頭0埋め");
    ok(checkDate(Typy.date.parse("1", "H"), [now.getFullYear(), now.getMonth(), now.getDate(), 1, 0, 0, 0]), "H 1桁");
    ok(checkDate(Typy.date.parse("11", "H"), [now.getFullYear(), now.getMonth(), now.getDate(), 11, 0, 0, 0]), "H 2桁");
    ok(checkDate(Typy.date.parse("06", "HH"), [now.getFullYear(), now.getMonth(), now.getDate(), 6, 0, 0, 0]), "HH 2桁 先頭0埋め");
    ok(checkDate(Typy.date.parse("0", "H"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "H 1桁 0");
    ok(checkDate(Typy.date.parse("00", "H"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "H 2桁 0");
    ok(checkDate(Typy.date.parse("00", "HH"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "HH 2桁 0");
    ok(checkDate(Typy.date.parse("12", "H"), [now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0, 0]), "H 2桁 12");
    ok(checkDate(Typy.date.parse("12", "HH"), [now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0, 0]), "HH 2桁 12");
    ok(checkDate(Typy.date.parse("13", "H"), [now.getFullYear(), now.getMonth(), now.getDate(), 13, 0, 0, 0]), "H 2桁 13以上");
    ok(checkDate(Typy.date.parse("13", "HH"), [now.getFullYear(), now.getMonth(), now.getDate(), 13, 0, 0, 0]), "HH 2桁 13以上");
    equal(Typy.date.parse("24", "H"), undefined, "H 2桁 24以上はNG");
    equal(Typy.date.parse("24", "HH"), undefined, "HH 2桁 24以上はNG");

    ok(checkDate(Typy.date("06", "H").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 6, 0, 0, 0]), "Typy.date() H 2桁 先頭0埋め");
    ok(checkDate(Typy.date("1", "H").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 1, 0, 0, 0]), "Typy.date() H 1桁");
    ok(checkDate(Typy.date("11", "H").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 11, 0, 0, 0]), "Typy.date() H 2桁");
    ok(checkDate(Typy.date("06", "HH").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 6, 0, 0, 0]), "Typy.date() HH 2桁 先頭0埋め");
    ok(checkDate(Typy.date("0", "H").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() H 1桁 0");
    ok(checkDate(Typy.date("00", "H").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() H 2桁 0");
    ok(checkDate(Typy.date("00", "HH").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() HH 2桁 0");
    ok(checkDate(Typy.date("12", "H").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0, 0]), "Typy.date() H 2桁 12");
    ok(checkDate(Typy.date("12", "HH").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0, 0]), "Typy.date() HH 2桁 12");
    ok(checkDate(Typy.date("13", "H").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 13, 0, 0, 0]), "Typy.date() H 2桁 13以上");
    ok(checkDate(Typy.date("13", "HH").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 13, 0, 0, 0]), "Typy.date() HH 2桁 13以上");
    equal(Typy.date("24", "H").val(), undefined, "Typy.date() H 2桁 24以上はNG");
    equal(Typy.date("24", "HH").val(), undefined, "Typy.date() HH 2桁 24以上はNG");
});

test("parse meridiem", function () {

    var now = new Date();

    Typy.lang.current("en");

    equal(Typy.date.parse("AM", "tt"), undefined, "tt のみは NG");
    equal(Typy.date.parse("AM", "t"), undefined, "t のみは NG");
    equal(Typy.date.parse("2012/01/01 AM", "yyyy/MM/dd tt"), undefined, "tt がある場合に時間が設定されていない場合はNG");
    equal(Typy.date.parse("2012/01/01 A", "yyyy/MM/dd tt"), undefined, "t がある場合に時間が設定されていない場合はNG");
    ok(checkDate(Typy.date.parse("AM 1", "tt h"), [now.getFullYear(), now.getMonth(), now.getDate(), 1, 0, 0, 0]), "tt");
    ok(checkDate(Typy.date.parse("A 1", "t h"), [now.getFullYear(), now.getMonth(), now.getDate(), 1, 0, 0, 0]), "t");
    ok(checkDate(Typy.date.parse("PM 1", "tt h"), [now.getFullYear(), now.getMonth(), now.getDate(), 13, 0, 0, 0]), "tt PMの場合に12未満は12時間加算");
    ok(checkDate(Typy.date.parse("P 1", "t h"), [now.getFullYear(), now.getMonth(), now.getDate(), 13, 0, 0, 0]), "t PMの場合に12未満は12時間加算");
    ok(checkDate(Typy.date.parse("PM 12", "tt h"), [now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0, 0]), "tt PMの場合に12時は12時");
    ok(checkDate(Typy.date.parse("P 12", "t h"), [now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0, 0]), "t PMの場合に12時は12時");
    ok(checkDate(Typy.date.parse("PM 0", "tt h"), [now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0, 0]), "tt PMの場合に0時は12時");
    ok(checkDate(Typy.date.parse("P 0", "t h"), [now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0, 0]), "t PMの場合に0時は12時");
    equal(Typy.date.parse("PM 13", "tt h"), undefined, "tt 時間が h で13以上の場合は h が優先されてNG");
    equal(Typy.date.parse("P 13", "t h"), undefined, "t 時間が h で13以上の場合は h が優先されてNG");
    equal(Typy.date.parse("AM 12", "tt H"), undefined, "tt 時間が H で午前が指定されている場合、12以上の場合は NG");
    equal(Typy.date.parse("A 12", "t H"), undefined, "t 時間が H で午前が指定されている場合、12以上の場合は NG");
    equal(Typy.date.parse("PM 11", "tt H"), undefined, "tt 時間が H で午後が指定されている場合、11以下の場合は NG");
    equal(Typy.date.parse("P 11", "t H"), undefined, "t 時間が H で午後が指定されている場合、11以下の場合は NG");

    equal(Typy.date("AM", "tt").val(), undefined, "Typy.date() tt のみは NG");
    equal(Typy.date("AM", "t").val(), undefined, "Typy.date() t のみは NG");
    equal(Typy.date("2012/01/01 AM", "yyyy/MM/dd tt").val(), undefined, "Typy.date() tt がある場合に時間が設定されていない場合はNG");
    equal(Typy.date("2012/01/01 A", "yyyy/MM/dd tt").val(), undefined, "Typy.date() t がある場合に時間が設定されていない場合はNG");
    ok(checkDate(Typy.date("AM 1", "tt h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 1, 0, 0, 0]), "Typy.date() tt");
    ok(checkDate(Typy.date("A 1", "t h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 1, 0, 0, 0]), "Typy.date() t");
    ok(checkDate(Typy.date("PM 1", "tt h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 13, 0, 0, 0]), "Typy.date() tt PMの場合に12未満は12時間加算");
    ok(checkDate(Typy.date("P 1", "t h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 13, 0, 0, 0]), "Typy.date() t PMの場合に12未満は12時間加算");
    ok(checkDate(Typy.date("PM 12", "tt h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0, 0]), "Typy.date() tt PMの場合に12時は12時");
    ok(checkDate(Typy.date("P 12", "t h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0, 0]), "Typy.date() t PMの場合に12時は12時");
    ok(checkDate(Typy.date("PM 0", "tt h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0, 0]), "Typy.date() tt PMの場合に0時は12時");
    ok(checkDate(Typy.date("P 0", "t h").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0, 0]), "Typy.date() t PMの場合に0時は12時");
    equal(Typy.date("PM 13", "tt h").val(), undefined, "Typy.date() tt 時間が h で13以上の場合は h が優先されてNG");
    equal(Typy.date("P 13", "t h").val(), undefined, "Typy.date() t 時間が h で13以上の場合は h が優先されてNG");
    equal(Typy.date("AM 12", "tt H").val(), undefined, "Typy.date() tt 時間が H で午前が指定されている場合、12以上の場合は NG");
    equal(Typy.date("A 12", "t H").val(), undefined, "Typy.date() t 時間が H で午前が指定されている場合、12以上の場合は NG");
    equal(Typy.date("PM 11", "tt H").val(), undefined, "Typy.date() tt 時間が H で午後が指定されている場合、11以下の場合は NG");
    equal(Typy.date("P 11", "t H").val(), undefined, "Typy.date() t 時間が H で午後が指定されている場合、11以下の場合は NG");

    Typy.lang.current("ja");

    ok(checkDate(Typy.date.parse("午前 1", "tt h"), [now.getFullYear(), now.getMonth(), now.getDate(), 1, 0, 0, 0]), "ja - tt");
    ok(checkDate(Typy.date.parse("午前 1", "t h"), [now.getFullYear(), now.getMonth(), now.getDate(), 1, 0, 0, 0]), "ja - t");
    ok(checkDate(Typy.date.parse("午後 1", "tt h"), [now.getFullYear(), now.getMonth(), now.getDate(), 13, 0, 0, 0]), "ja - tt");
    ok(checkDate(Typy.date.parse("午後 1", "t h"), [now.getFullYear(), now.getMonth(), now.getDate(), 13, 0, 0, 0]), "ja - t");

    Typy.lang.current("en");
});

test("parse minutes", function () {
    var now = new Date();
    ok(checkDate(Typy.date.parse("06", "m"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 6, 0, 0]), "m 2桁 先頭0埋め");
    ok(checkDate(Typy.date.parse("1", "m"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 1, 0, 0]), "m 1桁");
    ok(checkDate(Typy.date.parse("11", "m"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 11, 0, 0]), "m 2桁");
    ok(checkDate(Typy.date.parse("06", "mm"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 6, 0, 0]), "mm 2桁 先頭0埋め");
    ok(checkDate(Typy.date.parse("0", "m"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "m 1桁 0");
    ok(checkDate(Typy.date.parse("00", "m"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "m 2桁 0");
    ok(checkDate(Typy.date.parse("00", "mm"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "mm 2桁 0");
    equal(Typy.date.parse("60", "m"), undefined, "m 2桁 60以上");
    equal(Typy.date.parse("60", "mm"), undefined, "mm 2桁 60以上");

    ok(checkDate(Typy.date("06", "m").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 6, 0, 0]), "Typy.date() m 2桁 先頭0埋め");
    ok(checkDate(Typy.date("1", "m").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 1, 0, 0]), "Typy.date() m 1桁");
    ok(checkDate(Typy.date("11", "m").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 11, 0, 0]), "Typy.date() m 2桁");
    ok(checkDate(Typy.date("06", "mm").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 6, 0, 0]), "Typy.date() mm 2桁 先頭0埋め");
    ok(checkDate(Typy.date("0", "m").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() m 1桁 0");
    ok(checkDate(Typy.date("00", "m").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() m 2桁 0");
    ok(checkDate(Typy.date("00", "mm").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() mm 2桁 0");
    equal(Typy.date("60", "m").val(), undefined, "Typy.date() m 2桁 60以上");
    equal(Typy.date("60", "mm").val(), undefined, "Typy.date() mm 2桁 60以上");
});

test("parse seconds", function () {
    var now = new Date();
    ok(checkDate(Typy.date.parse("06", "s"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 6, 0]), "s 2桁 先頭0埋め");
    ok(checkDate(Typy.date.parse("1", "s"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 1, 0]), "s 1桁");
    ok(checkDate(Typy.date.parse("11", "s"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 11, 0]), "s 2桁");
    ok(checkDate(Typy.date.parse("06", "ss"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 6, 0]), "ss 2桁 先頭0埋め");
    ok(checkDate(Typy.date.parse("0", "s"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "s 1桁 0");
    ok(checkDate(Typy.date.parse("00", "s"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "s 2桁 0");
    ok(checkDate(Typy.date.parse("00", "ss"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "ss 2桁 0");
    equal(Typy.date.parse("60", "s"), undefined, "s 2桁 60以上");
    equal(Typy.date.parse("60", "ss"), undefined, "ss 2桁 60以上");

    ok(checkDate(Typy.date("06", "s").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 6, 0]), "Typy.date() s 2桁 先頭0埋め");
    ok(checkDate(Typy.date("1", "s").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 1, 0]), "Typy.date() s 1桁");
    ok(checkDate(Typy.date("11", "s").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 11, 0]), "Typy.date() s 2桁");
    ok(checkDate(Typy.date("06", "s").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 6, 0]), "Typy.date() s 2桁 先頭0埋め");
    ok(checkDate(Typy.date("0", "s").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() s 1桁 0");
    ok(checkDate(Typy.date("00", "s").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() s 2桁 0");
    ok(checkDate(Typy.date("00", "s").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "Typy.date() ss 2桁 0");
    equal(Typy.date("60", "s").val(), undefined, "Typy.date() s 2桁 60以上");
    equal(Typy.date("60", "ss").val(), undefined, "Typy.date() ss 2桁 60以上");
});

test("parse milliseconds", function () {
    var now = new Date();
    ok(checkDate(Typy.date.parse("1", "f"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 100]), "f");
    ok(checkDate(Typy.date.parse("11", "ff"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 110]), "ff");
    ok(checkDate(Typy.date.parse("111", "fff"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 111]), "fff");
    ok(checkDate(Typy.date.parse("05", "ff"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 50]), "ff 0埋め2桁");
    ok(checkDate(Typy.date.parse("005", "fff"), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 5]), "fff 0埋め3桁");

    ok(checkDate(Typy.date("1", "f").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 100]), "Typy.date() f");
    ok(checkDate(Typy.date("11", "ff").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 110]), "Typy.date() ff");
    ok(checkDate(Typy.date("111", "fff").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 111]), "Typy.date() fff");
    ok(checkDate(Typy.date("05", "ff").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 50]), "Typy.date() ff 0埋め2桁");
    ok(checkDate(Typy.date("005", "fff").val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 5]), "Typy.date() fff 0埋め3桁");
});

test("parse weekday", function () {
    var now = new Date();
    ok(checkDate(Typy.date.parse("2012/10/12(Fri)", "yyyy/MM/dd(ddd)"), [2012, 9, 12, 0, 0, 0, 0]));
    ok(checkDate(Typy.date.parse("2012/10/12(Friday)", "yyyy/MM/dd(dddd)"), [2012, 9, 12, 0, 0, 0, 0]));

    Typy.lang.current("ja");
    ok(checkDate(Typy.date.parse("2012/10/12(金)", "yyyy/MM/dd(ddd)"), [2012, 9, 12, 0, 0, 0, 0]));
    ok(checkDate(Typy.date.parse("2012/10/12(金曜日)", "yyyy/MM/dd(dddd)"), [2012, 9, 12, 0, 0, 0, 0]));

    Typy.lang.current("en");

    var weekday = now.getDay();
    var str = "Sun Mon Tue Wed Thu Fri Sat".split(" ")[weekday];
    ok(typeof Typy.date.parse(str, "ddd") === "undefined");

    str = "Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" ")[weekday];
    ok(typeof Typy.date.parse(str, "dddd") === "undefined");
});

test("parse odata", function () {
    var now = new Date();
    ok(checkDate(Typy.date.parse("/Date(0)/", "ODataJSON"), [1970, 0, 1, 9, 0, 0, 0]), "/Date(0)/");
    ok(checkDate(Typy.date.parse("/Date(" + now.getTime() + ")/", "ODataJSON"),
        [now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes(), now.getSeconds(), now.getMilliseconds()]),
        "/Date(now)/");
});

test("dayOfWeek", function () {
    strictEqual(Typy.date.dayOfWeek(new Date(2012, 7, 12)), 0, "dayOfWeek 日曜日");
    strictEqual(Typy.date.dayOfWeek(new Date(2012, 7, 14)), 2, "dayOfWeek 火曜日");
    strictEqual(Typy.date.dayOfWeek(new Date(2012, 7, 18)), 6, "dayOfWeek 土曜日");
    strictEqual(Typy.date.dayOfWeek(new Date(2012, 7, 14), 1), 1, "dayOfWeek 火曜日 で 月曜日始まり");

    strictEqual(Typy.date(new Date(2012, 7, 12)).dayOfWeek(), 0, "Typy.date() dayOfWeek 日曜日");
    strictEqual(Typy.date(new Date(2012, 7, 14)).dayOfWeek(), 2, "Typy.date() dayOfWeek 火曜日");
    strictEqual(Typy.date(new Date(2012, 7, 18)).dayOfWeek(), 6, "Typy.date() dayOfWeek 土曜日");

    strictEqual(Typy.date(new Date(2012, 7, 14)).dayOfWeek(1), 1, "Typy.date() dayOfWeek 火曜日 で 月曜日始まり");
});

test("dayOfMonth", function () {
    strictEqual(Typy.date.dayOfMonth(new Date(2012, 7, 1)), 1, "dayOfMonth 月初");
    strictEqual(Typy.date.dayOfMonth(new Date(2012, 7, 31)), 31, "dayOfMonth 月末");
    strictEqual(Typy.date.dayOfMonth(new Date(2012, 7, 15), 5), 15, "dayOfMonth 第2引数指定");

    strictEqual(Typy.date(new Date(2012, 7, 1)).dayOfMonth(), 1, "Typy.date() dayOfMonth 月初");
    strictEqual(Typy.date(new Date(2012, 7, 31)).dayOfMonth(), 31, "Typy.date() dayOfMonth 月末");
    strictEqual(Typy.date(new Date(2012, 7, 15)).dayOfMonth(5), 15, "Typy.date() dayOfWeek 第2引数指定");
});

test("dayOfYear", function () {
    strictEqual(Typy.date.dayOfYear(new Date(2011, 0, 1)), 1, "dayOfYear 年初");
    strictEqual(Typy.date.dayOfYear(new Date(2011, 11, 31)), 365, "dayOfYear 年末");
    strictEqual(Typy.date.dayOfYear(new Date(2012, 11, 31)), 366, "dayOfYear うるう年年末");

    strictEqual(Typy.date(new Date(2011, 0, 1)).dayOfYear(), 1, "Typy.date() dayOfYear 年初");
    strictEqual(Typy.date(new Date(2011, 11, 31)).dayOfYear(), 365, "Typy.date() dayOfYear 年末");
    strictEqual(Typy.date(new Date(2012, 11, 31)).dayOfYear(), 366, "Typy.date() dayOfYear うるう年年末");
});

test("startOf", function () {
    var now = new Date();
    ok(checkDate(Typy.date.startOfYear(now), [now.getFullYear(), 0, 1, 0, 0, 0, 0]), "start of year");
    ok(checkDate(Typy.date.startOfMonth(now), [now.getFullYear(), now.getMonth(), 1, 0, 0, 0, 0]), "start of month");
    ok(checkDate(Typy.date.startOfDay(now), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "start of day");
    ok(checkDate(Typy.date.startOfHour(now), [now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), 0, 0, 0]), "start of hour");
    ok(checkDate(Typy.date.startOfSecond(now), [now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getSeconds(), 0, 0]), "start of second");
    ok(checkDate(Typy.date.startOfMinute(now), [now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getSeconds(), now.getMinutes(), 0]), "start of minute");

    ok(checkDate(Typy.date(now).startOfYear().val(), [now.getFullYear(), 0, 1, 0, 0, 0, 0]), "start of year");
    ok(checkDate(Typy.date(now).startOfMonth().val(), [now.getFullYear(), now.getMonth(), 1, 0, 0, 0, 0]), "start of month");
    ok(checkDate(Typy.date(now).startOfDay().val(), [now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0]), "start of day");
    ok(checkDate(Typy.date(now).startOfHour().val(), [now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), 0, 0, 0]), "start of hour");
    ok(checkDate(Typy.date(now).startOfSecond().val(), [now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getSeconds(), 0, 0]), "start of second");
    ok(checkDate(Typy.date(now).startOfMinute().val(), [now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getSeconds(), now.getMinutes(), 0]), "start of minute");
});

test("endOf", function () {
    var year = 2012, month = 1, date = 15, hour = 9, minute = 10, second = 11,
        target = function () {
            return new Date(year, month, date, hour, minute, second, 123);
        };
    ok(checkDate(Typy.date.endOfYear(target()), [year, 11, 31, 23, 59, 59, 999]), "end of year");
    ok(checkDate(Typy.date.endOfMonth(target()), [year, month, 29, 23, 59, 59, 999]), "end of month");
    ok(checkDate(Typy.date.endOfDay(target()), [year, month, date, 23, 59, 59, 999]), "end of day");
    ok(checkDate(Typy.date.endOfHour(target()), [year, month, date, hour, 59, 59, 999]), "end of hour");
    ok(checkDate(Typy.date.endOfMinute(target()), [year, month, date, hour, minute, 59, 999]), "end of minute");
    ok(checkDate(Typy.date.endOfSecond(target()), [year, month, date, hour, minute, second, 999]), "end of second");

    ok(checkDate(Typy.date(target()).endOfYear().val(), [year, 11, 31, 23, 59, 59, 999]), "end of year");
    ok(checkDate(Typy.date(target()).endOfMonth().val(), [year, month, 29, 23, 59, 59, 999]), "end of month");
    ok(checkDate(Typy.date(target()).endOfDay().val(), [year, month, date, 23, 59, 59, 999]), "end of day");
    ok(checkDate(Typy.date(target()).endOfHour().val(), [year, month, date, hour, 59, 59, 999]), "end of hour");
    ok(checkDate(Typy.date(target()).endOfMinute().val(), [year, month, date, hour, minute, 59, 999]), "end of minute");
    ok(checkDate(Typy.date(target()).endOfSecond().val(), [year, month, date, hour, minute, second, 999]), "end of second");
});

test("format", function(){
    var year = 2012, month = 1, date = 15, hour = 9, minute = 10, second = 11,
        target = function () {
            return new Date(year, month, date, hour, minute, second, 123);
        };
    Typy.lang.current("en");
    strictEqual(Typy.date.format(target(), "yyyy/MM/dd"), "2012/02/15");
    strictEqual(Typy.date.format(target(), "yy/MMM/MMMM/M/d (ddd) (dddd) zzz fff"), "12/Feb/February/2/15 (Wed) (Wednesday) +09:00 123");

    strictEqual(Typy.date.format(target(), "yyyy\\y\\mMM\\\\"), "2012ym02\\", "\\でエスケープ");
    strictEqual(Typy.date.format(target(), "yyyy'ym\\\\\\''MM\"ymd\""), "2012ym\\'02ymd", "クォーテーションでエスケープ");
});

QUnit.module("Typy.uri");

test("uri resolve - pinpoint", function () {
    var tc = ["//g", "http://a/b/c/d;p?q", "http://g"];
    strictEqual(Typy.uri(tc[1], tc[0]).val(), tc[2], "Typy.data - [" + tc.join(", ") + "]");
    strictEqual(Typy.uri.resolve(tc[1], tc[0]), tc[2], "Typy.data.resolve - [" + tc.join(", ") + "]");
});

test("uri resolve", function () {
    // http://skew.org/uri/uri_tests.js

    var BASE_URI0 = "http://a/b/c/d;p?q",
        BASE_URI1 = "http://a/b/c/d;p?q=1/2",
        BASE_URI2 = "http://a/b/c/d;p=1/2?q",
        BASE_URI3 = "fred:///s//a/b/c",
        BASE_URI4 = "http:///s//a/b/c",
    // [ref, base, expected]
        absolutizeTestCases = [
            ["", "http://example.com/path?query#frag", "http://example.com/path?query"],
            ["../c", "foo:a/b", "foo:c"],
            ["foo:.", "foo:a", "foo:"],
            ["/foo/../../../bar", "zz:abc", "zz:/bar"],
            ["/foo/../bar", "zz:abc", "zz:/bar"],
            ["foo/../../../bar", "zz:abc", "zz:bar"],
            ["foo/../bar", "zz:abc", "zz:bar"],
            ["zz:.", "zz:abc", "zz:"],
            ["/.", BASE_URI0, "http://a/"],
            ["/.foo", BASE_URI0, "http://a/.foo"],
            [".foo", BASE_URI0, "http://a/b/c/.foo"],
            ["g:h", BASE_URI0, "g:h"],
            ["g", BASE_URI0, "http://a/b/c/g"],
            ["./g", BASE_URI0, "http://a/b/c/g"],
            ["g/", BASE_URI0, "http://a/b/c/g/"],
            ["/g", BASE_URI0, "http://a/g"],
            ["//g", BASE_URI0, "http://g"],
            ["?y", BASE_URI0, "http://a/b/c/d;p?y"],
            ["g?y", BASE_URI0, "http://a/b/c/g?y"],
            ["#s", BASE_URI0, "http://a/b/c/d;p?q#s"],
            ["g#s", BASE_URI0, "http://a/b/c/g#s"],
            ["g?y#s", BASE_URI0, "http://a/b/c/g?y#s"],
            [";x", BASE_URI0, "http://a/b/c/;x"],
            ["g;x", BASE_URI0, "http://a/b/c/g;x"],
            ["g;x?y#s", BASE_URI0, "http://a/b/c/g;x?y#s"],
            ["", BASE_URI0, "http://a/b/c/d;p?q"],
            [".", BASE_URI0, "http://a/b/c/"],
            ["./", BASE_URI0, "http://a/b/c/"],
            ["..", BASE_URI0, "http://a/b/"],
            ["../", BASE_URI0, "http://a/b/"],
            ["../g", BASE_URI0, "http://a/b/g"],
            ["../..", BASE_URI0, "http://a/"],
            ["../../", BASE_URI0, "http://a/"],
            ["../../g", BASE_URI0, "http://a/g"],
            ["../../../g", BASE_URI0, "http://a/g"],
            ["../../../../g", BASE_URI0, "http://a/g"],
            ["/./g", BASE_URI0, "http://a/g"],
            ["/../g", BASE_URI0, "http://a/g"],
            ["g.", BASE_URI0, "http://a/b/c/g."],
            [".g", BASE_URI0, "http://a/b/c/.g"],
            ["g..", BASE_URI0, "http://a/b/c/g.."],
            ["..g", BASE_URI0, "http://a/b/c/..g"],
            ["./../g", BASE_URI0, "http://a/b/g"],
            ["./g/.", BASE_URI0, "http://a/b/c/g/"],
            ["g/./h", BASE_URI0, "http://a/b/c/g/h"],
            ["g/../h", BASE_URI0, "http://a/b/c/h"],
            ["g;x=1/./y", BASE_URI0, "http://a/b/c/g;x=1/y"],
            ["g;x=1/../y", BASE_URI0, "http://a/b/c/y"],
            ["g?y/./x", BASE_URI0, "http://a/b/c/g?y/./x"],
            ["g?y/../x", BASE_URI0, "http://a/b/c/g?y/../x"],
            ["g#s/./x", BASE_URI0, "http://a/b/c/g#s/./x"],
            ["g#s/../x", BASE_URI0, "http://a/b/c/g#s/../x"],
            ["http:g", BASE_URI0, "http:g"],
            // "http://a/b/c/g"]],
            ["http:", BASE_URI0, "http:"],
            // BASE_URI0]],
            ["/a/b/c/./../../g", BASE_URI0, "http://a/a/g"],

            ["g", BASE_URI1, "http://a/b/c/g"],
            ["./g", BASE_URI1, "http://a/b/c/g"],
            ["g/", BASE_URI1, "http://a/b/c/g/"],
            ["/g", BASE_URI1, "http://a/g"],
            ["//g", BASE_URI1, "http://g"],
            ["?y", BASE_URI1, "http://a/b/c/d;p?y"],
            ["g?y", BASE_URI1, "http://a/b/c/g?y"],
            ["g?y/./x", BASE_URI1, "http://a/b/c/g?y/./x"],
            ["g?y/../x", BASE_URI1, "http://a/b/c/g?y/../x"],
            ["g#s", BASE_URI1, "http://a/b/c/g#s"],
            ["g#s/./x", BASE_URI1, "http://a/b/c/g#s/./x"],
            ["g#s/../x", BASE_URI1, "http://a/b/c/g#s/../x"],
            ["./", BASE_URI1, "http://a/b/c/"],
            ["../", BASE_URI1, "http://a/b/"],
            ["../g", BASE_URI1, "http://a/b/g"],
            ["../../", BASE_URI1, "http://a/"],
            ["../../g", BASE_URI1, "http://a/g"],

            ["g", BASE_URI2, "http://a/b/c/d;p=1/g"],
            ["./g", BASE_URI2, "http://a/b/c/d;p=1/g"],
            ["g/", BASE_URI2, "http://a/b/c/d;p=1/g/"],
            ["g?y", BASE_URI2, "http://a/b/c/d;p=1/g?y"],
            [";x", BASE_URI2, "http://a/b/c/d;p=1/;x"],
            ["g;x", BASE_URI2, "http://a/b/c/d;p=1/g;x"],
            ["g;x=1/./y", BASE_URI2, "http://a/b/c/d;p=1/g;x=1/y"],
            ["g;x=1/../y", BASE_URI2, "http://a/b/c/d;p=1/y"],
            ["./", BASE_URI2, "http://a/b/c/d;p=1/"],
            ["../", BASE_URI2, "http://a/b/c/"],
            ["../g", BASE_URI2, "http://a/b/c/g"],
            ["../../", BASE_URI2, "http://a/b/"],
            ["../../g", BASE_URI2, "http://a/b/g"],

            ["g:h", BASE_URI3, "g:h"],
            ["g", BASE_URI3, "fred:///s//a/b/g"],
            ["./g", BASE_URI3, "fred:///s//a/b/g"],
            ["g/", BASE_URI3, "fred:///s//a/b/g/"],
            ["/g", BASE_URI3, "fred:///g"],
            ["//g", BASE_URI3, "fred://g"],
            ["//g/x", BASE_URI3, "fred://g/x"],
            ["///g", BASE_URI3, "fred:///g"],
            ["./", BASE_URI3, "fred:///s//a/b/"],
            ["../", BASE_URI3, "fred:///s//a/"],
            ["../g", BASE_URI3, "fred:///s//a/g"],
            ["../../", BASE_URI3, "fred:///s//"],
            ["../../g", BASE_URI3, "fred:///s//g"],
            ["../../../g", BASE_URI3, "fred:///s/g"],
            ["../../../../g", BASE_URI3, "fred:///g"],

            ["g:h", BASE_URI4, "g:h"],
            ["g", BASE_URI4, "http:///s//a/b/g"],
            ["./g", BASE_URI4, "http:///s//a/b/g"],
            ["g/", BASE_URI4, "http:///s//a/b/g/"],
            ["/g", BASE_URI4, "http:///g"],
            ["//g", BASE_URI4, "http://g"],
            ["//g/x", BASE_URI4, "http://g/x"],
            ["///g", BASE_URI4, "http:///g"],
            ["./", BASE_URI4, "http:///s//a/b/"],
            ["../", BASE_URI4, "http:///s//a/"],
            ["../g", BASE_URI4, "http:///s//a/g"],
            ["../../", BASE_URI4, "http:///s//"],
            ["../../g", BASE_URI4, "http:///s//g"],
            ["../../../g", BASE_URI4, "http:///s/g"],
            ["../../../../g", BASE_URI4, "http:///g"],

            ["bar:abc", "foo:xyz", "bar:abc"],
            ["../abc", "http://example/x/y/z", "http://example/x/abc"],
            ["http://example/x/abc", "http://example2/x/y/z", "http://example/x/abc"],
            ["../r", "http://ex/x/y/z", "http://ex/x/r"],
            ["q/r", "http://ex/x/y", "http://ex/x/q/r"],
            ["q/r#s", "http://ex/x/y", "http://ex/x/q/r#s"],
            ["q/r#s/t", "http://ex/x/y", "http://ex/x/q/r#s/t"],
            ["ftp://ex/x/q/r", "http://ex/x/y", "ftp://ex/x/q/r"],
            ["", "http://ex/x/y", "http://ex/x/y"],
            ["", "http://ex/x/y/", "http://ex/x/y/"],
            ["", "http://ex/x/y/pdq", "http://ex/x/y/pdq"],
            ["z/", "http://ex/x/y/", "http://ex/x/y/z/"],
            ["#Animal", "file:/swap/test/animal.rdf", "file:/swap/test/animal.rdf#Animal"],
            ["../abc", "file:/e/x/y/z", "file:/e/x/abc"],
            ["/example/x/abc", "file:/example2/x/y/z", "file:/example/x/abc"],
            ["../r", "file:/ex/x/y/z", "file:/ex/x/r"],
            ["/r", "file:/ex/x/y/z", "file:/r"],
            ["q/r", "file:/ex/x/y", "file:/ex/x/q/r"],
            ["q/r#s", "file:/ex/x/y", "file:/ex/x/q/r#s"],
            ["q/r#", "file:/ex/x/y", "file:/ex/x/q/r#"],
            ["q/r#s/t", "file:/ex/x/y", "file:/ex/x/q/r#s/t"],
            ["ftp://ex/x/q/r", "file:/ex/x/y", "ftp://ex/x/q/r"],
            ["", "file:/ex/x/y", "file:/ex/x/y"],
            ["", "file:/ex/x/y/", "file:/ex/x/y/"],
            ["", "file:/ex/x/y/pdq", "file:/ex/x/y/pdq"],
            ["z/", "file:/ex/x/y/", "file:/ex/x/y/z/"],
            ["file://meetings.example.com/cal#m1", "file:/devel/WWW/2000/10/swap/test/reluri-1.n3", "file://meetings.example.com/cal#m1"],
            ["file://meetings.example.com/cal#m1", "file:/home/connolly/w3ccvs/WWW/2000/10/swap/test/reluri-1.n3", "file://meetings.example.com/cal#m1"],
            ["./#blort", "file:/some/dir/foo", "file:/some/dir/#blort"],
            ["./#", "file:/some/dir/foo", "file:/some/dir/#"],
            ["./", "http://example/x/abc.efg", "http://example/x/"],
            ["//example/x/abc", "http://example2/x/y/z", "http://example/x/abc"],
            ["/r", "http://ex/x/y/z", "http://ex/r"],
            ["./q:r", "http://ex/x/y", "http://ex/x/q:r"],
            ["./p=q:r", "http://ex/x/y", "http://ex/x/p=q:r"],
            ["?pp/rr", "http://ex/x/y?pp/qq", "http://ex/x/y?pp/rr"],
            ["y/z", "http://ex/x/y?pp/qq", "http://ex/x/y/z"],
            ["local/qual@domain.org#frag", "mailto:local", "mailto:local/qual@domain.org#frag"],
            ["more/qual2@domain2.org#frag", "mailto:local/qual1@domain1.org", "mailto:local/more/qual2@domain2.org#frag"],
            ["y?q", "http://ex/x/y?q", "http://ex/x/y?q"],
            ["/x/y?q", "http://ex?p", "http://ex/x/y?q"],
            ["c/d", "foo:a/b", "foo:a/c/d"],
            ["/c/d", "foo:a/b", "foo:/c/d"],
            ["", "foo:a/b?c#d", "foo:a/b?c"],
            ["b/c", "foo:a", "foo:b/c"],
            ["../b/c", "foo:/a/y/z", "foo:/a/b/c"],
            ["./b/c", "foo:a", "foo:b/c"],
            ["/./b/c", "foo:a", "foo:/b/c"],
            ["../../d", "foo://a//b/c", "foo://a/d"],
            [".", "foo:a", "foo:"],
            ["..", "foo:a", "foo:"],
            ["abc", "http://example/x/y%2Fz", "http://example/x/abc"],
            ["../../x%2Fabc", "http://example/a/x/y/z", "http://example/a/x%2Fabc"],
            ["../x%2Fabc", "http://example/a/x/y%2Fz", "http://example/a/x%2Fabc"],
            ["abc", "http://example/x%2Fy/z", "http://example/x%2Fy/abc"],
            ["q%3Ar", "http://ex/x/y", "http://ex/x/q%3Ar"],
            ["/x%2Fabc", "http://example/x/y%2Fz", "http://example/x%2Fabc"],
            ["/x%2Fabc", "http://example/x/y/z", "http://example/x%2Fabc"],
            ["local2@domain2", "mailto:local1@domain1?query1", "mailto:local2@domain2"],
            ["local2@domain2?query2", "mailto:local1@domain1", "mailto:local2@domain2?query2"],
            ["local2@domain2?query2", "mailto:local1@domain1?query1", "mailto:local2@domain2?query2"],
            ["?query2", "mailto:local@domain?query1", "mailto:local@domain?query2"],
            ["local@domain?query2", "mailto:?query1", "mailto:local@domain?query2"],
            ["?query2", "mailto:local@domain?query1", "mailto:local@domain?query2"],
            ["http://example/a/b?c/../d", "foo:bar", "http://example/a/b?c/../d"],
            ["http://example/a/b#c/../d", "foo:bar", "http://example/a/b#c/../d"],
            ["http:this", "http://example.org/base/uri", "http:this"],
            ["http:this", "http:base", "http:this"],
            [".//g", "f:/a", "f://g"],
            ["b/c//d/e", "f://example.org/base/a", "f://example.org/base/b/c//d/e"],
            ["m2@example.ord/c2@example.org", "mid:m@example.ord/c@example.org", "mid:m@example.ord/m2@example.ord/c2@example.org"],
            ["mini1.xml", "file:///C:/DEV/Haskell/lib/HXmlToolbox-3.01/examples/", "file:///C:/DEV/Haskell/lib/HXmlToolbox-3.01/examples/mini1.xml"],
            ["../b/c", "foo:a/y/z", "foo:a/b/c"],
            ["b", "foo:", "foo:b"],
            ["b", "foo://a", "foo://a/b"],
            ["b", "foo://a?q", "foo://a/b"],
            ["b?q", "foo://a", "foo://a/b?q"],
            ["b?q", "foo://a?r", "foo://a/b?q"]
        ],
        i, l, tc;

    for (i = 0, l = absolutizeTestCases.length; i < l; i++) {
        tc = absolutizeTestCases[i];
        strictEqual(Typy.uri(tc[1], tc[0]).val(), tc[2], "Typy.data - [" + tc.join(", ") + "]");
        strictEqual(Typy.uri.resolve(tc[1], tc[0]), tc[2], "Typy.data.resolve - [" + tc.join(", ") + "]");
    }
});

test("uri schema get", function () {
    strictEqual(Typy.uri("a://foo/bar").schema(), "a", "Typy.uri() - normal");
    strictEqual(Typy.uri("mailto:John.Doe@example.com").schema(), "mailto", "Typy.uri() - スラッシュなし");
    strictEqual(Typy.uri("urn:oasis:names:specification:docbook:dtd:xml:4.1.2").schema(), "urn", "Typy.uri() - 階層");
    strictEqual(Typy.uri("/foo/bar").schema(), undefined, "Typy.uri() - なし");

    strictEqual(Typy.uri.schema("a://foo/bar"), "a", "Typy.uri.schema - normal");
    strictEqual(Typy.uri.schema("mailto:John.Doe@example.com"), "mailto", "Typy.uri.schema - スラッシュなし");
    strictEqual(Typy.uri.schema("urn:oasis:names:specification:docbook:dtd:xml:4.1.2"), "urn", "Typy.uri.schema - 階層");
    strictEqual(Typy.uri.schema("/foo/bar"), undefined, "Typy.uri.schema - なし");
});

test("uri schema set", function () {
    strictEqual(Typy.uri("a://foo/bar").schema("b").val(), "b://foo/bar", "Typy.uri() - デフォルト");
    strictEqual(Typy.uri("mailto:John.Doe@example.com").schema("http").val(),
        "http:John.Doe@example.com", "Typy.uri() - スラッシュなし");
    strictEqual(Typy.uri("urn:oasis:names:specification:docbook:dtd:xml:4.1.2").schema("url").val(),
        "url:oasis:names:specification:docbook:dtd:xml:4.1.2", "Typy.uri() - 階層");
    strictEqual(Typy.uri("/foo/bar").schema("test").val(),
        "test:/foo/bar", "Typy.uri() - なし");

    strictEqual(Typy.uri.schema("a://foo/bar", "b"), "b://foo/bar", "Typy.uri.schema - デフォルト");
    strictEqual(Typy.uri.schema("mailto:John.Doe@example.com", "http"),
        "http:John.Doe@example.com", "Typy.uri.schemat - スラッシュなし");
    strictEqual(Typy.uri.schema("urn:oasis:names:specification:docbook:dtd:xml:4.1.2", "url"),
        "url:oasis:names:specification:docbook:dtd:xml:4.1.2", "Typy.uri.schema - 階層");
    strictEqual(Typy.uri.schema("/foo/bar", "test"), "test:/foo/bar", "Typy.uri.schema - なし");
});

test("uri authority get", function () {
    strictEqual(Typy.uri("a://foo/bar").authority(), "foo", "Typy.uri() - normal");
    strictEqual(Typy.uri("http://www.Typy.js/bar").authority(), "www.Typy.js", "Typy.uri() - .区切り");
    strictEqual(Typy.uri("mailto:John.Doe@example.com").authority(), undefined, "Typy.uri() - absolute なし");
    strictEqual(Typy.uri("/foo/bar").authority(), undefined, "Typy.uri() - relative なし");

    strictEqual(Typy.uri.authority("a://foo/bar"), "foo", "Typy.uri.authority() - normal");
    strictEqual(Typy.uri.authority("http://www.Typy.js/bar"), "www.Typy.js", "Typy.uri.authority() - .区切り");
    strictEqual(Typy.uri.authority("mailto:John.Doe@example.com"), undefined, "Typy.uri.authority() - absolute なし");
    strictEqual(Typy.uri.authority("/foo/bar"), undefined, "Typy.uri.authority() - relative なし");
});

test("uri authority set", function () {
    strictEqual(Typy.uri("a://foo/bar").authority("bar").val(), "a://bar/bar", "Typy.uri() - normal");
    strictEqual(Typy.uri("http://www.Typy.js/bar").authority("bar.foo.jp").val(), "http://bar.foo.jp/bar", "Typy.uri() - .区切り");
    strictEqual(Typy.uri("mailto:John.Doe@example.com").authority("localhost").val(),
        "mailto://localhost/John.Doe@example.com", "Typy.uri() - absolute なし");
    strictEqual(Typy.uri("/foo/bar").authority("test").val(),
        "//test/foo/bar", "Typy.uri() - relative なし");

    strictEqual(Typy.uri.authority("a://foo/bar", "bar"), "a://bar/bar", "Typy.uri() - normal");
    strictEqual(Typy.uri.authority("http://www.Typy.js/bar", "bar.foo.jp"), "http://bar.foo.jp/bar", "Typy.uri() - .区切り");
    strictEqual(Typy.uri.authority("mailto:John.Doe@example.com", "localhost"),
        "mailto://localhost/John.Doe@example.com", "Typy.uri() - absolute なし");
    strictEqual(Typy.uri.authority("/foo/bar", "test"), "//test/foo/bar", "Typy.uri() - relative なし");
});

QUnit.module("Typy.num");

test("num round, ceil, floor", function(){

    strictEqual(Typy.num.round(123.5), 124, "引数なし");
    strictEqual(Typy.num.ceil(123.5), 124, "引数なし");
    strictEqual(Typy.num.floor(123.5), 123, "引数なし");

    strictEqual(Typy.num.round(123.4), 123, "引数なし");
    strictEqual(Typy.num.ceil(123.4), 124, "引数なし");
    strictEqual(Typy.num.floor(123.4), 123, "引数なし");

    strictEqual(Typy.num.round(123.6), 124, "引数なし");
    strictEqual(Typy.num.ceil(123.6), 124, "引数なし");
    strictEqual(Typy.num.floor(123.6), 123, "引数なし");

    strictEqual(Typy.num.round(123.123, 2), 123.12, "2桁の引数");
    strictEqual(Typy.num.ceil(123.123, 2), 123.13, "2桁の引数");
    strictEqual(Typy.num.floor(123.123, 2), 123.12, "2桁の引数");

    strictEqual(Typy.num.round(123.125, 2), 123.13, "2桁の引数");
    strictEqual(Typy.num.ceil(123.125, 2), 123.13, "2桁の引数");
    strictEqual(Typy.num.floor(123.125, 2), 123.12, "2桁の引数");

    strictEqual(Typy.num.round(123.126, 2), 123.13, "2桁の引数");
    strictEqual(Typy.num.ceil(123.126, 2), 123.13, "2桁の引数");
    strictEqual(Typy.num.floor(123.126, 2), 123.12, "2桁の引数");

    strictEqual(Typy.num.round(123.123, -2), 100, "-2桁の引数");
    strictEqual(Typy.num.ceil(123.123, -2), 200, "-2桁の引数");
    strictEqual(Typy.num.floor(123.123, -2), 100, "-2桁の引数");

    strictEqual(Typy.num.round(150.125, -2), 200, "-2桁の引数");
    strictEqual(Typy.num.ceil(150.125, -2), 200, "-2桁の引数");
    strictEqual(Typy.num.floor(150.125, -2), 100, "-2桁の引数");

    strictEqual(Typy.num.round(151.126, -2), 200, "-2桁の引数");
    strictEqual(Typy.num.ceil(151.126, -2), 200, "-2桁の引数");
    strictEqual(Typy.num.floor(151.126, -2), 100, "-2桁の引数");
});

test("num math funcs", function(){
    strictEqual(Typy.num.abs(-123), Math.abs(-123), "abs");
    strictEqual(Typy.num(-123).abs().val(), Math.abs(-123), "abs");

    strictEqual(Typy.num.pow(12,3), Math.pow(12,3), "pow");
    strictEqual(Typy.num(12).pow(3).val(), Math.pow(12,3), "pow");

    strictEqual(Typy.num.sin(0.1743), Math.sin(0.1743), "sin");
    strictEqual(Typy.num(0.1743).sin().val(), Math.sin(0.1743), "sin");

    strictEqual(Typy.num.asin(0.1743), Math.asin(0.1743), "asin");
    strictEqual(Typy.num(0.1743).asin().val(), Math.asin(0.1743), "asin");

    strictEqual(Typy.num.cos(0.1743), Math.cos(0.1743), "cos");
    strictEqual(Typy.num(0.1743).cos().val(), Math.cos(0.1743), "cos");

    strictEqual(Typy.num.acos(0.1743), Math.acos(0.1743), "acos");
    strictEqual(Typy.num(0.1743).acos().val(), Math.acos(0.1743), "acos");

    strictEqual(Typy.num.tan(0.1743), Math.tan(0.1743), "tan");
    strictEqual(Typy.num(0.1743).tan().val(), Math.tan(0.1743), "tan");

    strictEqual(Typy.num.atan(0.1743), Math.atan(0.1743), "atan");
    strictEqual(Typy.num(0.1743).atan().val(), Math.atan(0.1743), "atan");

    strictEqual(Typy.num.exp(5), Math.exp(5), "exp");
    strictEqual(Typy.num(5).exp().val(), Math.exp(5), "exp");

    strictEqual(Typy.num.sqrt(5), Math.sqrt(5), "sqrt");
    strictEqual(Typy.num(5).sqrt().val(), Math.sqrt(5), "sqrt");

    strictEqual(Typy.num.log(5), Math.log(5), "log");
    strictEqual(Typy.num(5).log().val(), Math.log(5), "log");
});

test("num format", function () {

    strictEqual(Typy.num.format(123, "####"), "123", "# は値のないところは出力されない");
    strictEqual(Typy.num.format(123, "0000"), "0123", "0 は値のないところにも出力される");
    strictEqual(Typy.num.format(123, "0###"), "0123", "# は値のないところは出力されない");
    strictEqual(Typy.num.format(12345678, "##,##"), "12,345,678", "整数部の末尾以外にカンマがある場合は桁区切りが設定される");
    strictEqual(Typy.num.format(12345678, "#,#,,#,#"), "12,345,678", "整数部の末尾以外にカンマが複数あった場合も桁区切りが設定され、桁区切り以外のカンマは出力されない");
    strictEqual(Typy.num.format(12345678, "##,##"), "12,345,678", "整数部の末尾以外にカンマがある場合は桁区切りが設定される");
    strictEqual(Typy.num.format(12345678, "####,,"), "12", "整数部の末尾にカンマがある場合は、連続しているカンマの数の1000乗が除算される");

    strictEqual(Typy.num.format(123.456, "#.###"), "123.456", ". の後ろは小数点以下として出力される");
    strictEqual(Typy.num.format(123.456, "#.#.##"), "123.456", ". が複数あった場合は最初のものが優先され後ろは無視して出力される");
    strictEqual(Typy.num.format(123.456, "#.##"), "123.45", "小数点以下の # は指定された桁までしか出力されない");
    strictEqual(Typy.num.format(123.456, "#.#####"), "123.456", "小数点以下の # は桁以上に指定されていても出力されない");
    strictEqual(Typy.num.format(123.456, "#.00"), "123.45", "小数点以下の 0 は指定された桁までしか出力されない");
    strictEqual(Typy.num.format(123.456, "#.00000"), "123.45600", "小数点以下の 0 は指定された桁まで出力される");

    strictEqual(Typy.num.format(123.456, "#.#%"), "12345.6%", "%が含まれる場合は100倍される");
    strictEqual(Typy.num.format(123.456, "#.#‰"), "123456‰", "‰が含まれる場合は1000倍される");

    strictEqual(Typy.num.format(123.456, "%#.#%"), "%1234560%", "%が複数含まれる場合は100の含まれる数乗倍される");
    strictEqual(Typy.num.format(123.456, "‰#.#‰"), "‰123456000‰", "‰が複数含まれる場合は1000の含まれる数乗倍される");

    strictEqual(Typy.num.format(123456.789, "\\\\\\0\\##\\,\\.\\%\\‰#\\,.00\\00"), "\\0#12345,.%‰6,.7809", "\\でエスケープできる");

    strictEqual(Typy.num.format(-1234.567, "ABC0,0.000"), "-ABC1,234.567", "負数の場合は先頭に - が付加される");

    strictEqual(Typy.num.format(1234.567, "ABC0,0.000;(0,0.000)"), "ABC1,234.567", "正数の場合は正数のパートが利用される");
    strictEqual(Typy.num.format(-1234.567, "ABC0,0.000;(0,0.000)"), "(1,234.567)", "負数の場合は負数のパートが利用される");
    strictEqual(Typy.num.format(0, "ABC0,0.000;(0,0.000);ZERO"), "ZERO", "0の場合は0のパートが利用される");
    strictEqual(Typy.num.format(0.123, "ABC0,0.ABC;;ZERO"), "ZERO", "正数でフォーマット結果が0の場合は0のパートが利用される");
    strictEqual(Typy.num.format(-0.123, ";(0,0);ZERO"), "ZERO", "負数でフォーマット結果が0の場合は0のパートが利用される");

    strictEqual(Typy.num.format(12345.789, ""), "", "フォーマットが指定されていない場合は空文字");
    strictEqual(Typy.num.format(12345.789, ";;"), "", "フォーマットが指定されていない場合は空文字");

    strictEqual(Typy.num.format(123.456, "#'#0#0;\\''#.#"), "12#0#0;'3.4", "シングルクォーテーションでのエスケープ");
    strictEqual(Typy.num.format(123.456, "#\"#0#0;\\\"\"#.#"), "12#0#0;\"3.4", "ダブルクォーテーションでのエスケープ");
});

QUnit.module("Typy.timeSpan");

test("timeSpan format", function(){

    strictEqual(Typy.timeSpan(5,4,3,2,111).format("dd.hh:mm:ss.fff"), "05.04:03:02.111");
    strictEqual(Typy.timeSpan(5,4,3,2,111).format("dddd.hhhh:mmmm:ssss.ffffff"), "0005.04:03:02.111");
    strictEqual(Typy.timeSpan(5,4,3,2,111).format("dd.\\hh:'m'm:\"s\"s.\\\\fff"), "05.h4:m3:s2.\\111");

});