/* string */
(function (global, Typy) {

    "use strict";

    var namedFormat = {},
        regexp = RegExp,
        escapeRegExp = function (value){
            if(!isStr(value)) value = value + "";
            return value.replace(/([\\/'*+?|()\[\]{}.^$])/g,'\\$1');
        },
        repeat = function (target, count) {
            if (!isStr(target)) return;
            if (isNumeric(count)) {
                count = parseInt10(count);
            }
            count = isNum(count) ? count < 0 ? 0 : Math.floor(count) : 0;
            if (count === 0) return "";

            return (new Array(count + 1)).join(target);
        },
        startsWith = function (target, value, ignoreCase) {
            if (!isStr(target)) return;
            var source = isRegExp(value) ? value.source : escapeRegExp(value);
            ignoreCase = !isUndef(ignoreCase) ? !!ignoreCase :
                         isRegExp(value) ? value.ignoreCase : false;
            source = (source.charAt(0) === "^" ? "" : "^") + source;
            return regexp(source, ignoreCase ? "i" : "").test(target);
        },
        endsWith = function (target, value, ignoreCase) {
            if (!isStr(target)) return;
            var source = isRegExp(value) ? value.source : escapeRegExp(value);
            ignoreCase = !isUndef(ignoreCase) ? !!ignoreCase :
                isRegExp(value) ? value.ignoreCase : false;
            source = source + (source.charAt(source.length - 1) === "$" ? "" : "$");
            return regexp(source, ignoreCase ? "i" : "").test(target);
        },
        format = function (target) {
            var args = takeArg(arguments, 1);
            if (!target) {
                return target;
            }
            if (args.length === 0) {
                return target;
            }
            if (args.length === 1 && isArray(args[0])) {
                args = args[0];
            }
            return target.toString().replace(/\{?\{(.+?)\}\}?/g, function (match, arg1) {
                var val, splitPos, prop, rootProp, format, formatPos,
                    param = arg1;
                if (match.substr(0, 2) === "{{" && match.substr(match.length - 2) === "}}") {
                    return match.replace("{{", "{").replace("}}", "}");
                }
                formatPos = param.indexOf(":");
                if(formatPos > -1){
                    format = param.substr(formatPos + 1);
                    param = param.substr(0, formatPos);
                }
                splitPos = Math.min(param.indexOf(".") === -1 ? param.length : param.indexOf("."),
                    param.indexOf("[") === -1 ? param.length : param.indexOf("["));
                if (splitPos < param.length) {
                    rootProp = param.substr(0, splitPos);
                    prop = "['" + param.substr(0, splitPos) + "']" + param.substr(splitPos);
                } else {
                    rootProp = param;
                    prop = "['" + param + "']";
                }
                val = (new Function("return arguments[0]" + prop + ";"))(isNumeric(rootProp) ? args : args[0]);
                if(format){
                    if(isStr(val)){
                        if(namedFormat[format] && isFunc(namedFormat[format].format)){
                            val = namedFormat[format].format(val, format);
                        }
                    }else{
                        if(!isFunc(val.format) && hasExt(val)){
                            val = Typy.ext(val);
                        }
                        if(isFunc(val.format)){
                            val = val.format(format) + "";
                        }
                    }
                }
                val = isUndef(val) ? "" : val;
                if (match.substr(0, 2) === "{{") {
                    val = "{" + val;
                }
                if (match.substr(match.length - 2) === "}}") {
                    val = val + "}";
                }
                return val;
            });
        };

    Typy.str = extDefine(function (constractor, target) {
        if (arguments.length < 3) {
            target = isUndef(target) || target == null ? "" : target;
            return new constractor(target.toString());
        }
        return new constractor(format.apply(null, slice.call(arguments)));
    }, {
        format:function () {
            return format.apply(null, slice.call(arguments));
        },
        lower:function (target) {
            if (isStr(target)) {
                return target.toLowerCase();
            }
        },
        upper:function (target) {
            if (isStr(target)) {
                return target.toUpperCase();
            }
        },
        repeat:repeat,
        padLeft:function (target, length, padChars) {
            if (!isStr(target)) return;
            if (!isNum(length) && isNumeric(length)) {
                length = parseInt10(length);
            }
            length = isNum(length) ? length < 0 ? 0 : Math.floor(length) : 0;
            if (isUndef(padChars) || isNull(padChars)) padChars = " ";
            var margin = length - target.length;
            if (margin < 1) return target;
            var paddingChars = repeat(padChars.toString(), margin);
            return paddingChars.substr(0, margin) + target;
        },
        padRight:function (target, length, padChars) {
            if (!isStr(target)) return;
            if (!isNum(length) && isNumeric(length)) {
                length = parseInt10(length);
            }
            length = isNum(length) ? length < 0 ? 0 : Math.floor(length) : 0;
            if (isUndef(padChars) || isNull(padChars)) padChars = " ";
            var margin = length - target.length;
            if (margin < 1) return target;
            var paddingChars = repeat(padChars.toString(), margin);
            return target + paddingChars.substr(0, margin);
        },
        startsWith:startsWith,
        endsWith:endsWith,
        trimLeft:function (target) {
            if (!isStr(target)) return;
            return target.replace(/^[\s　]+/, "");
        },
        trimRight:function (target) {
            if (!isStr(target)) return;
            return target.replace(/[\s　]+$/, "");
        },
        trim:function (target) {
            if (!isStr(target)) return;
            return target.replace(/^[\s　]+|[\s　]+$/g, "");
        },
        contains:function (target, value, ignoreCase) {
            if (!isStr(target)) return;
            var source = isRegExp(value) ? value.source : escapeRegExp(value);
            ignoreCase = !isUndef(ignoreCase) ? !!ignoreCase :
                isRegExp(value) ? value.ignoreCase : false;
            return regexp(source, ignoreCase ? "i" : "").test(target);
        },
        capitalize:function (target) {
            if (!isStr(target)) return;
            return target.charAt(0).toUpperCase() + target.substring(1);
        },
        camelize:function (target) {
            if (!isStr(target)) return;
            return target.replace(/(\-|_|\s)+(.)?/g, function (match, separator, chr) {
                return chr ? chr.toUpperCase() : '';
            });
        },
        reverse:function (target) {
            if (!isStr(target)) return;
            return target.split("").reverse().join("")
        },
        clipRight:function (target, length) {
            if (!isStr(target)) return;
            length = isNum(length) ? length < 0 ? 0 : length : 0;
            if (target.length <= length) return target;
            return target.substring(target.length - length, target.length);
        },
        clipLeft:function (target, length) {
            if (!isStr(target)) return;
            length = isNum(length) ? length < 0 ? 0 : length : 0;
            if (target.length <= length) return target;
            return target.substr(0, length);
        },
        surrounds:function (target, start, end, force) {
            var normalize = function (val) {
                if (isUndef(val) || isNull(val)) {
                    val = "";
                }
                return val.toString();
            };
            if (!isStr(target)) return;
            start = normalize(start);
            if (arguments.length === 4) {
                end = normalize(end);
                force = !!force;
            } else if (arguments.length === 3) {
                if (isBool(end)) {
                    force = !!end;
                    end = start;
                } else {
                    force = false;
                    end = normalize(end);
                }
            } else if (arguments.length <= 2) {
                end = start;
                force = false;
            }
            if (startsWith(target, start) && !force) {
                start = "";
            }
            if (endsWith(target, end) && !force) {
                end = "";
            }
            return start + target + end;
        },
        surroundsWith:function (target, start, end, ignoreCase) {
            if (!isStr(target)) return;
            start = ifUndefOrNull(start, "");
            if(arguments.length === 3 && isBool(end)){
                ignoreCase = end;
                end = void 0;
            }
            end = ifUndefOrNull(end, start);
            return startsWith(target, start, ignoreCase) && endsWith(target, end, ignoreCase);
        },
        /***
         * @module Typy.str
         * @method split
         * @summary
         * 文字列を指定した区切り文字をもとに複数の部分文字列に区切ることにより、String オブジェクトを文字列の配列に分割します。
         * @param target:String 対象となる文字列
         * @param separator:String 区切り文字
         * @param limit?:Number 見つかった分割結果の数の制限の指定
         * @return String[]
         * 分割された文字列の新しい配列
         */
        /***
         * @module Typy.str
         * @override split
         * @summary
         * 文字列を指定した正規表現に一致する文字列をもとに複数の部分文字列に区切ることにより、String オブジェクトを文字列の配列に分割します。
         * @param target:String 対象となる文字列
         * @param separator:RegExp 区切り正規表現
         * @param limit?:Number 見つかった分割結果の数の制限の指定
         * @return String[]
         * 分割された文字列の新しい配列
         */
        split: function(target, separator, limit){
            if (!isStr(target)) return;
            if(!limit){
                limit = -1;
            }
            return target.split(separator, limit);
        },
        /***
         * @module Typy.str
         * @method replace
         * @summary 指定された値を指定された値で置き換えます。
         */
        replace: function(target, value, replaceValue){
            if (!isStr(target)) return;
            return target.replace(value, replaceValue);
        }
    }, isStr);

    addStaticFunc(Typy.str, "namedFormat", function(name){
        if(!isStr(name)) return;
        if(arguments.length === 1){
            return namedFormat[name];
        }
        if(arguments.length >= 2){
            namedFormat[name] = arguments[1];
        }
    });

})(this, Typy);

var TypyStr = Typy.str;