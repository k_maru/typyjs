/* Date */

// dependon array.js, lang.js, string.js
(function (global, Typy) {

    "use strict";
    var proto = Date.prototype,
        getFullYear = proto.getFullYear,
        getMonth = proto.getMonth,
        getDate = proto.getDate,
        getHours = proto.getHours,
        getMinutes = proto.getMinutes,
        getSeconds = proto.getSeconds,
        getMilliseconds = proto.getMilliseconds,
        getTime = proto.getTime,

        setFullYear = proto.setFullYear,
        setMonth = proto.setMonth,
        setDate = proto.setDate,
        setHours = proto.setHours,
        setMinutes = proto.setMinutes,
        setSeconds = proto.setSeconds,
        setMilliseconds = proto.setMilliseconds,
        setTime = proto.setTime,
        unitYear = "Year", unitMonth = "Month", unitDate = "Date",
        unitHour = "Hours", unitMinute = "Minutes", unitSecond = "Seconds", unitMillSecond = "Milliseconds",
        unitWeek = "Week", unitTime = "Time",
        cachedFormatTokens = {},
        namedFormat = {
            "ODataJSON":{
                parse:function (val, lang, langDef) {
                    var match = /^\/Date\((-?\d+)(\+|-)?(\d+)?\)\/$/i.exec(val),
                        result, offset, curMin;
                    if (match) {
                        result = new Date(+match[1]);
                        if(match[3]){
                            offset = (+match[3]);
                            if(match[2] === "-"){
                                offset = -offset;
                            }
                            curMin = result.getUTCMinutes();
                            result.setUTCMinutes(curMin - offset);
                        }
                        return result;
                    }
                },
                format:function (val, lang, langDef) {
                    return "/Date(" + getTime.call(val) + ")/";
                }
            }
        },
        tokenize = function (format) {
            var i, l, ch,
                tokens = [],
                escaped = false,
                token, quote,
                tokenizeLiteral = function (target, index) {
                    var match = format.substr(index).match(new RegExp(target + "+"));
                    if (match) {
                        return match[0].length;
                    }
                    return 1;
                };
            if (cachedFormatTokens[format]) {
                return cachedFormatTokens[format];
            }
            for (i = 0, l = format.length; i < l; i++) {

                ch = format.charAt(i);
                if(escaped){
                    tokens.push(ch);
                    escaped = false;
                    continue;
                }
                if(ch === "\\"){
                    escaped = true;
                    continue;
                }
                if(ch === "'" || ch === "\""){
                    if(ch === quote){
                        quote = false;
                    }else{
                        quote = ch;
                    }
                    continue;
                }
                if(quote){
                    tokens.push(ch);
                    continue;
                }
                switch (ch) {
                    case "d":
                    case "f":
                    case "h":
                    case "H":
                    case "m":
                    case "M":
                    case "s":
                    case "t":
                    case "y":
                    case "z":
                        token = {
                            type:ch,
                            length:tokenizeLiteral(ch, i)
                        };
                        tokens.push(token);
                        i += (token.length - 1);
                        break;
                    case "/":
                    case ":":
                        token = {
                            type:ch
                        };
                        tokens.push(token);
                        break;
                    default:
                        tokens.push(ch);
                }
            }
            cachedFormatTokens[format] = tokens;
            return tokens;
        },
        resolveTokenForParse = function (token, startPos, value, dateDef, lang, langDef) {
            var meridiemVal, target, result, i, l,
                names, sign,
                tokenType = token.type,
                tokenLength = token.length,
                zeroPaddingTwoLengthResolveToken = function (valPos) {
                    var result, text, num, code;

                    code = value.charCodeAt(startPos);
                    if (48 <= code && code <= 57) { //0-9
                        text = value.charAt(startPos);
                        result = 1;
                        code = value.charCodeAt(startPos + 1);
                        if (48 <= code && code <= 57) { //0-9
                            text += value.charAt(startPos + 1);
                            result = 2;
                        }
                        num = parseInt10(text);
                        if (!isNaN(num)) {
                            dateDef.values[valPos] = num;
                            return result;
                        }
                    }
                },
                fixLengthResolveToken = function (valPos) {
                    var match = value.substr(startPos).match(new RegExp("^\\d{" + tokenLength + "}"));
                    if(match && match.length > 0){
                        dateDef.values[valPos] = parseInt10(match[0]);
                        return token.length;
                    }
                },
                millisecondsResolveToken = function () {
                    var text = "0." + value.substr(startPos, tokenLength);
                    if (isNumeric(text)) {
                        dateDef.values[6] = parseFloat(text) * 1000;
                        return token.length;
                    }
                },
                makeMd = function (index, key, matchName) {
                    if (tokenLength === 1) {
                        return zeroPaddingTwoLengthResolveToken(index);
                    } else if (tokenLength === 2) {
                        return fixLengthResolveToken(index);
                    } else {
                        names = tokenLength === 3 ? langDef[key].shortNames : langDef[key].names;
                        for (i = 0, l = names.length; i < l; i++) {
                            target = value.substr(startPos, names[i].length).toUpperCase();
                            if (target === names[i].toUpperCase()) {
                                matchName(i);
                                return names[i].length;
                            }
                        }
                    }
                },
                makeHhms = function (index) {
                    if (tokenLength === 1) {
                        return zeroPaddingTwoLengthResolveToken(index);
                    } else {
                        return fixLengthResolveToken(index);
                    }
                };

            if (tokenType === "y") {
                if (tokenLength === 1 || tokenLength === 2) {
                    if (tokenLength === 1) {
                        result = zeroPaddingTwoLengthResolveToken(0);
                    } else {
                        result = fixLengthResolveToken(0);
                    }
                    if (result) {
                        //30以下だったら2000年代、30以上だったら1900年代
                        dateDef.values[0] += (dateDef.values[0] < 30 ? 2000 : 1900);
                    }
                    return result;
                } else {
                    return fixLengthResolveToken(0);
                }
            }
            if (tokenType === "M") {
                return makeMd(1, "months", function (index) {
                    dateDef.values[1] = index + 1;
                });
            }
            if (tokenType === "d") {
                return makeMd(2, "weekdays", function (index) {
                    dateDef.weekday = index;
                });
            }
            if (tokenType === "h") {
                dateDef.militaryTime = false;
                return makeHhms(3);
            }

            if (tokenType === "H") {
                dateDef.militaryTime = true;
                return makeHhms(3);
            }

            if (tokenType === "m") {
                return makeHhms(4);
            }

            if (tokenType === "s") {
                return makeHhms(5);
            }

            if (tokenType === "f") {
                return millisecondsResolveToken();
            }

            if (tokenType === "t") {
                meridiemVal = tokenLength === 1 ? langDef.meridiem.shortNames : langDef.meridiem.names;
                target = value.substr(startPos, meridiemVal.ante.length).toUpperCase();
                if (target === meridiemVal.ante.toUpperCase()) {
                    dateDef.anteMeridiem = true;
                    return meridiemVal.ante.length;
                }
                target = value.substr(startPos, meridiemVal.post.length).toUpperCase();
                if (target === meridiemVal.post.toUpperCase()) {
                    dateDef.anteMeridiem = false;
                    return meridiemVal.post.length;
                }
            }

            if (tokenType === "z") {
                target = value.substr(startPos, 1);
                if (target === "+") {
                    sign = 1;
                } else if (target === "-") {
                    sign = -1;
                } else {
                    return;
                }

                if (tokenLength <= 2) {
                    target = value.substr(startPos + 1, 2).match(tokenLength === 1 ? /\d{1,2}/ : /\d{2}/);
                    if (target) {
                        dateDef.timeoffset = parseInt10(target[0]) * 60 * sign;
                        return target[0].length + 1;
                    }
                } else {
                    target = value.substr(startPos + 1, 5).match(/(\d{1,2}):(\d{2})/);
                    if (!target) {
                        target = value.substr(startPos + 1, 4).match(/(\d{2})(\d{2})/);
                    }
                    if (target && parseInt10(target[2]) < 60) {
                        dateDef.timeoffset = ((parseInt10(target[1]) * 60) + parseInt10(target[2])) * sign;
                        return target[0].length + 1
                    }
                }
            }
        },
        makeDateFromArray = function (array, militaryTime, anteMeridiem, weekday, timeoffset) {
            var now = new Date(),
                hour = array[3];
            //全て数値じゃないとNG
            if (!TypyArray.every(array, function (item) {
                return !isUnusable(item) && isNum(item);
            })) {
                return;
            }
            //全て0未満だとNG
            if (!TypyArray.some(array, function (item) {
                return isNum(item) && item > -1;
            })) {
                return;
            }

            //AM/PMが付与されてる場合で13時以上や時間未設定はNG
            if (!isUndef(anteMeridiem)) {
                if (hour > 12 || hour < 0) {
                    return;
                }
            }

            //12時間制で
            if (!isUndef(militaryTime) && !militaryTime && hour > -1) {
                //13時とかはNG
                if (hour > 12) {
                    return;
                }
                //AMの時の12時は0時
                if ((isUndef(anteMeridiem) || (!isUndef(anteMeridiem) && anteMeridiem)) && hour === 12) {
                    array[3] = hour = 0;
                }
                //PMの時の0-11時は+12時間
                if (!isUndef(anteMeridiem) && !anteMeridiem && hour < 12) {
                    array[3] = hour = (hour + 12);
                }
            }

            //24時間制で
            if (!isUndef(militaryTime) && militaryTime && hour > -1) {
                //午前が指定されている場合に12時以上はNG
                if (!isUndef(anteMeridiem) && anteMeridiem) {
                    if (hour >= 12) {
                        return;
                    }
                }
                //午後が指定されている場合に11時以前はNG
                if (!isUndef(anteMeridiem) && !anteMeridiem) {
                    if (hour < 12) {
                        return;
                    }
                }
            }
            //時間しか指定されてない場合は現在日付
            if (array[0] < 0 && array[1] < 0 && array[2] < 0) {
                if (array[0] < 0) array[0] = getFullYear.call(now);
                if (array[1] < 0) array[1] = getMonth.call(now);
                if (array[2] < 0) array[2] = getDate.call(now);
            } else {
                if (array[0] < 0) array[0] = getFullYear.call(now);
                if (array[1] < 0) array[1] = 0;
                if (array[2] < 0) array[2] = 1;
            }

            if (array[3] < 0) array[3] = 0;
            if (array[4] < 0) array[4] = 0;
            if (array[5] < 0) array[5] = 0;
            if (array[6] < 0) array[6] = 0;

            var result = new Date();

            setFullYear.call(result, array[0], array[1], array[2]);
            setHours.call(result, array[3], array[4], array[5], array[6]);

            if (getFullYear.call(result) === array[0] &&
                getMonth.call(result) === array[1] &&
                getDate.call(result) === array[2] &&
                getHours.call(result) === array[3] &&
                getMinutes.call(result) === array[4] &&
                getSeconds.call(result) === array[5] &&
                getMilliseconds.call(result) === array[6]) {

                //曜日が指定されていて日付と一致していない場合はNG
                if (!isUndef(weekday)) {
                    if (result.getDay() !== weekday) {
                        return;
                    }
                }

                //timezoneの時刻調整
                if (!isUndef(timeoffset)) {
                    setMinutes.call(result, getMinutes.call(result) + 0 - timeoffset - (new Date()).getTimezoneOffset());
                }

                return result;
            }
        },
        makeDateFromStringAndFormat = function (value, format) {
            var langDef = TypyLang.calendar(),
                lang = TypyLang.current(),
                dateDef = {
                    values:[-1, -1, -1, -1, -1, -1, -1]
                },
                named = namedFormat[format],
                formatTokens, token, i, l,
                lastValueEndPos = 0,
                resolveLength = 0;

            if (named && isFunc(named.parse)) {
                return named.parse(value, lang, langDef);
            }

            formatTokens = tokenize(format);
            for (i = 0, l = formatTokens.length; i < l; i++) {
                resolveLength = undefined;
                token = formatTokens[i];
                if (isStr(token)) {
                    if (value.substr(lastValueEndPos, token.length) === token) {
                        resolveLength = token.length;
                    }
                } else {
                    //TODO 多言語から取得してチェック
                    if (token.type === "/" || token.type === ":") {
                        if (value.substr(lastValueEndPos, 1) === token.type) {
                            resolveLength = 1;
                        }
                    } else {
                        resolveLength = resolveTokenForParse(formatTokens[i], lastValueEndPos, value, dateDef, lang, langDef);
                    }
                }

                if (isUndef(resolveLength)) {
                    dateDef = null;
                    break;
                }
                lastValueEndPos += resolveLength;
            }
            if (!dateDef) {
                return;
            }
            if (dateDef.values[1] === 0) {
                return;
            } else if (dateDef.values[1] >= 1) {
                dateDef.values[1] -= 1;
            }

            return makeDateFromArray(dateDef.values, dateDef.militaryTime, dateDef.anteMeridiem, dateDef.weekday, dateDef.timeoffset);
        },
        makeDateFromStringAndDefaultFormats = function (value) {
            var formats = TypyLang.calendar().defaultFormats,
                i = 0,
                length = (isArray(formats) ? formats.length : 0),
                result;
            for (; i < length; i++) {
                result = makeDateFromStringAndFormat(value, formats[i]);
                if (result) {
                    break;
                }
            }
            return result;
        },
        makeTextByFormat = function (source, format) {
            if (!isStr(format)) {
                return;
            }
            var named = namedFormat[format];
            if (named && isFunc(named.format)) {
                return named.format(source, TypyLang.current(), TypyLang.calendar());
            }
            return makeTextByFormatTokens(source, tokenize(format), TypyLang.current(), TypyLang.calendar());
        },
        makeTextByFormatTokens = function (source, tokens, lang, langDef) {
            var tokenItem,
                type,
                tlength,
                meridiem,
                timezoneoffset,
                texts = [],
                clipRight = TypyStr.clipRight,
                i, l,
                getNonMilitaryHour = function () {
                    var hour = source.getHours();
                    return hour > 12 ? hour - 12 : hour;
                },
                makeMd = function (l, lang, addend, func, func2) {
                    if (l === 1) {
                        texts.push(func.call(source) + addend);
                    }
                    if (l === 2) {
                        texts.push(clipRight("0" + (func.call(source) + addend), 2));
                    }
                    if (l >= 3) {
                        texts.push((tlength === 3 ? langDef[lang].shortNames : langDef[lang].names)[(func2 || func).call(source)]);
                    }
                },
                makeHhms = function (l, func) {
                    if (l === 1) {
                        texts.push(func(source));
                    }
                    if (l >= 2) {
                        texts.push(clipRight("0" + func(source), 2));
                    }
                };

            for (i = 0, l = tokens.length; i < l; i++) {
                tokenItem = tokens[i];
                if (isStr(tokenItem)) {
                    texts.push(tokenItem);
                    continue;
                }
                type = tokenItem.type;
                tlength = tokenItem.length;
                if (type === "/" || type === ":") {
                    //TODO: 多言語から取得
                    texts.push(type);
                } else if (type === "y") {
                    if (tlength === 1) {
                        texts.push(clipRight(parseInt10(("" + getFullYear.call(source))).toString(), 2));
                    } else {
                        texts.push(clipRight(Array(tlength + 1).join("0") + getFullYear.call(source), tlength));
                    }
                } else if (type === "M") {
                    makeMd(tlength, "months", 1, getMonth);
                } else if (type === "d") {
                    makeMd(tlength, "weekdays", 0, getDate, proto.getDay);
                } else if (type === "h") {
                    makeHhms(tlength, getNonMilitaryHour);
                } else if (type === "H") {
                    makeHhms(tlength, function (s) {
                        return getHours.call(s);
                    });
                } else if (type === "m") {
                    makeHhms(tlength, function (s) {
                        return getMinutes.call(s);
                    });
                } else if (type === "s") {
                    makeHhms(tlength, function (s) {
                        return getSeconds.call(s);
                    });
                } else if (type === "f") {
                    texts.push((getMilliseconds.call(source) + "000").substr(0, tlength));
                } else if (type === "t") {
                    meridiem = tlength === 1 ? langDef.meridiem.shortNames : langDef.meridiem.names;
                    texts.push(getHours.call(source) > 12 ? meridiem.post : meridiem.ante);
                } else if (type === "z") {
                    timezoneoffset = source.getTimezoneOffset();
                    texts.push((timezoneoffset < 0 ? "+" : "-") + (function () {
                        if (tlength === 1) {
                            return Math.floor(Math.abs(timezoneoffset / 60)).toString();
                        } else if (tlength === 2) {
                            return clipRight("0" + Math.floor(Math.abs(timezoneoffset / 60)).toString(), 2);
                        } else {
                            return clipRight("0" + Math.floor(Math.abs(timezoneoffset / 60)).toString(), 2) +
                                ":" +
                                clipRight("0" + Math.floor(Math.abs(timezoneoffset % 60)).toString(), 2);
                        }
                    })());
                }
            }
            return texts.join("");
        },
        add = function (target, unit, value) {
            if (isDate(target) && isStr(unit) && isNumeric(value)) {
                var firstDayInMonth = new Date(getFullYear.call(target), getMonth.call(target), 1),
                    addend = parseInt10(value);
                if (unit === unitYear) {
                    unit = "FullYear";
                }
                if (unit === unitWeek) {
                    setDate.call(target, getDate.call(target) + (addend * 7));
                } else {
                    target["set" + unit](target["get" + unit]() + addend);
                    if (unit === unitMonth) {
                        setMonth.call(firstDayInMonth, getMonth.call(firstDayInMonth) + addend);
                        if (getMonth.call(firstDayInMonth) !== getMonth.call(target)) {
                            setDate.call(target, 0);
                        }
                    }
                }
            }
            return target;
        },
        dayOf = function (target, unit, start) {
            if (isDate(target) && isStr(unit)) {
                var newYear,
                    diff;
                if (unit === unitYear) {
                    newYear = new Date(getFullYear.call(target), 0, 1);
                    return Math.floor((getTime.call(target) - getTime.call(newYear)) / (24 * 60 * 60 * 1000)) + 1;
                }
                if (unit === unitMonth) {
                    return getDate.call(target);
                }
                if (unit === unitWeek) {
                    start = isNumeric(start) ?
                        Math.max(0, Math.min(parseInt(start, 10), 7)) : 0;
                    diff = target.getDay() - start;
                    return diff < 0 ? 7 + diff :
                        diff > 6 ? diff - 6 :
                            diff;
                }
            }
        },
        startOf = function (target, unit, start) {
            if (isDate(target) && isStr(unit)) {
                var dayOfWeek = 0,
                    diff;
                if (unit === unitWeek) {
                    dayOfWeek = dayOf(target, unitWeek, 0);
                    start = isNumeric(start) ? Math.max(0, Math.min(parseInt10(start), 7)) : 0;

                    diff = 0 - dayOfWeek + start;
                    setDate.call(target, getDate.call(target) + (diff > 0 ? diff - 7 : diff));
                    unit = unitDate;
                }
                switch (unit) {
                    case unitYear:
                        setMonth.call(target, 0);
                    case unitMonth:
                        setDate.call(target, 1);
                    case unitDate:
                        setHours.call(target, 0);
                    case unitHour:
                        setMinutes.call(target, 0);
                    case unitMinute:
                        setSeconds.call(target, 0);
                    case unitSecond:
                        setMilliseconds.call(target, 0);
                }
            }
            return target;
        },
        endOf = function (target, unit, start) {
            if (isDate(target) && isStr(unit)) {
                startOf(target, unit, start);
                if (unit === unitWeek) {
                    setDate.call(target, getDate.call(target) + 7);
                } else {
                    add(target, unit, 1);
                }
                setMilliseconds.call(target, -1);
            }
            return target;
        };

    var TypyDate = Typy.date = extDefine(function (constractor, target) {
        var args = slice.call(arguments),
            now = new Date(),
            arrayDate;
        if (isUndef(target)) {
            return new constractor(now);
        }
        if (isUnusable(target)) {
            return new constractor();
        }
        if (isDate(target) || isNum(target)) {
            //clone
            return new constractor(new Date(+target));
        }
        if (isStr(target)) {
            if (args.length > 2 && isStr(args[2])) {
                return new constractor(makeDateFromStringAndFormat(target, args[2]));
            } else {
                return new constractor(makeDateFromStringAndDefaultFormats(target));
            }
        }
        if (isArray(target)) {
            arrayDate = target.slice(0);
            if (arrayDate.length < 1) arrayDate[0] = now.getFullYear();
            if (arrayDate.length < 2) arrayDate[1] = now.getMonth();
            if (arrayDate.length < 3) arrayDate[2] = 1;
            if (arrayDate.length < 4) arrayDate[3] = 0;
            if (arrayDate.length < 5) arrayDate[4] = 0;
            if (arrayDate.length < 6) arrayDate[5] = 0;
            if (arrayDate.length < 7) arrayDate[6] = 0;

            return new constractor(makeDateFromArray.call(null, arrayDate));
        }
    }, {
        format: unchainable(function(target, format) {
            if (isDate(target)) {
                return makeTextByFormat(target, format ? format : TypyDate.defaultFormats[0], TypyLang.current());
            }
        }),
        isLeapYear: unchainable(function (target) {
            if (isDate(target)) {
                var year = target.getFullYear();
                return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
            }
        }),
        diff: unchainable(function(target, dest){
            if(!isDate(target) || isUndef(dest)){
                return;
            }
            if(!isDate(dest)){
                dest = TypyDate.apply(null, takeArg(arguments, 1)).val();
            }
            if(!isDate(dest)){
                return;
            }
            return target - dest;
        })
    }, isDate);

    addStaticFunc(TypyDate, "parse", function (target, format) {
        if (isStr(target)) {
            if (isStr(format)) {
                return makeDateFromStringAndFormat(target, format, TypyLang.current());
            } else {
                return makeDateFromStringAndDefaultFormats(target, TypyLang.current());
            }
        }
    });
    //unit
    TypyArray.each([unitYear, unitMonth, unitDate, unitHour, unitMinute, unitSecond, unitMillSecond, unitTime], function (item) {
        addFunc(TypyDate, item.toLowerCase(), function (target, value) {
            if (isDate(target)) {
                if (isNumeric(value)) {
                    target["set" + (item === unitYear ? "Full" : "") + item](parseInt10(value));
                    return target;
                } else {
                    return target["get" + (item === unitYear ? "Full" : "") + item]();
                }
            }
        });
    });
    //add
    TypyArray.each([unitYear, unitMonth, unitDate, unitHour, unitMinute, unitSecond, unitMillSecond, unitWeek, unitTime], function (item) {
        var name = item.charAt(item.length - 1) !== "s" ? (item + "s") : item;
        name = item === unitDate ? "Days" : name;
        addFunc(TypyDate, "add" + name, function (target, value) {
            return add(target, item, value);
        });
    });
    //dayOf
    TypyArray.each([unitYear, unitMonth, unitWeek], function (item) {
        addFunc(TypyDate, "dayOf" + item, function (target, start) {
            return dayOf(target, item, start);
        });
    });
    //startOf - endOf
    TypyArray.each([unitYear, unitMonth, unitDate, unitHour, unitMinute, unitSecond, unitWeek], function (item) {
        var name = item.charAt(item.length - 1) === "s" ? item.substr(0, item.length - 1) : item;
        name = item === unitDate ? "Day" : name;
        addFunc(TypyDate, "startOf" + name, function (target, start) {
            return startOf(target, item, start);
        });
        addFunc(TypyDate, "endOf" + name, function (target, start) {
            return endOf(target, item, start);
        });
    });
    addStaticFunc(TypyDate, "namedFormat", function(name){
        if(!isStr(name)) return;
        if(arguments.length === 1){
            return namedFormat[name];
        }
        if(arguments.length >= 2){
            namedFormat[name] = arguments[1];
        }
    });
})(this, Typy);

var TypyDate = Typy.date;