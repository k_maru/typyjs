/* object */
(function (global, Typy) {

    "use strict";

    Typy.obj = extDefine(function (constractor, target) {
        if(isObj(target)){
            return new constractor(target);
        }
        return new constractor({});
    }, {
        keys: unchainable(function(target){
            return Typy.keys(target);
        }),
        values: unchainable(function(target){
            var p, results = [];
            for(p in target){
                if (hasOwnProperty.call(target, p)){
                    results.push(target[p]);
                }
            }
            return results;
        }),
        pairs: unchainable(function(target){
            var p, results = [];
            for(p in target){
                if (hasOwnProperty.call(target, p)){
                    results.push([p, target[p]]);
                }
            }
            return results;
        }),
        prop: unchainable(function(target, key, val){
            if(arguments.length <= 1){
                return;
            }
            if(!isObj(target)){
                if(arg.length >= 2){
                    return;
                }
                return target;
            }
            if(arg.length === 2){
                return target[key];
            }
            target[key] = val;
            return target;
        }),
        extend: function(target){
            if(!isObj(target)){
                return;
            }
            return extend.apply(null, arguments);
        },
        pick: function(target){
            var result = {},
                props = takeArg(arguments, 1), prop, i, l;
            if(!isObj(target)){
                return;
            }
            for(i = 0, l = props.length; i < l; i++){
                prop = props[i];
                if(prop in target){
                    result[prop] = target[prop]
                }
            }
            return result;
        },
        omit: function(target){
            var result = {},
                props = takeArg(arguments, 1), i, l, keys;
            if(!isObj(target)){
                return;
            }
            keys = Typy.keys(target);
            for(i = 0, l = keys.length; i < l; i++){
                if(props.indexOf(keys[i]) < 0){
                    result[keys[i]] = target[keys[i]]
                }
            }
            return result;
        },
        merge: function(target){
            var val, i, l, p;
            if(!isObj(target)){
                return;
            }
            for(i = 1, l = arguments.length; i < l; i++){
                val = arguments[i];
                for(p in val){
                    if(!(p in target) && hasOwnProperty.call(val, p)){
                        target[p] = val[p];
                    }
                }
            }
            return target;
        }
    }, isObj);

})(this, Typy);

var TypyObj = Typy.obj;