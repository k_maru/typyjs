/* number */

// dependon array.js
(function (global, Typy) {

    "use strict";

    var math = Math,
        namedFormat = {},
        tokenize = function (format) {
            var i = 0, l = format.length, c,
                escaped = false,
                quote = false,
                result = {
                    plus: {
                        ints: []
                    }
                },
                current = result.plus.ints,
                section = result.plus,
                sectionPos = 0,
                sectionName,
                isInts = true,
                prepareSeparator = function (section) {
                    var i = 0,
                        token,
                        holder = false,
                        separator = false,
                        ints = section.ints;
                    while ((function () {
                        if (ints.length && ints[ints.length - 1].type === ",") {
                            section.pows = (section.pows || 0) + 1;
                            ints.pop();
                            return true;
                        }
                        return false;
                    })()) { }
                    while (i < ints.length) {
                        token = ints[i];
                        if (!isStr(token)) {
                            if (token.holder) {
                                if (holder && separator) {
                                    section.separate = true;
                                }
                                holder = true;
                            } else if (token.separator) {
                                separator = true;
                                ints.splice(i, 1);
                                i--;
                            } else {
                                holder = false;
                                separator = false;
                            }
                        }
                        i++;
                    }

                };
            for (; i < l; i++) {

                c = format.charAt(i);
                if (escaped) {
                    current.push(c);
                    escaped = false;
                } else if (c === "\\") {
                    escaped = true;
                } else if (c === "\"" || c === "'") {
                    if (quote === c) {
                        quote = false;
                    }  else {
                        quote = c;
                    }
                } else if(quote){
                    current.push(c);
                } else if (c === "0" || c === "#") {
                    current.push({
                        type: c,
                        holder: true
                    });
                } else if (c === ",") {
                    if (isInts) {
                        current.push({
                            type: c,
                            separator: true
                        });
                    }
                } else if (c === "%" || c === "‰") {
                    current.push(c);
                    if (c === "%") {
                        section.parcent = (section.parcent || 0) + 1;
                    } else if (c === "‰") {
                        section.permil = (section.permil || 0) + 1;
                    }
                } else if (c === ".") {
                    if (isInts) {
                        isInts = false;
                        current = [];
                        result[(sectionPos === 0) ? "plus" : (sectionPos === 1 ? "minus" : "zero")].decs = current;
                    }
                } else if (c === ";") {
                    prepareSeparator(section);
                    isInts = true;
                    if (sectionPos < 2) {
                        sectionName = sectionPos === 0 ? "minus" : "zero";
                        result[sectionName] = { ints: [] };
                        section = result[sectionName];
                        current = section.ints;
                        sectionPos++;
                    } else {
                        break;
                    }
                } else {
                    current.push(c);
                }
            }

            prepareSeparator(section);

            return result;
        },
        exponentToNumStr = function (val) {
            var ePos = val.indexOf("e");
            if (ePos < 0) {
                return val;
            }
            var mantissa = val.substr(0, ePos);
            var exponent = parseInt(val.substr(ePos + 1), 10);
            var isMinus = val.charAt(0) === "-";
            if (isMinus) {
                mantissa = mantissa(1);
            }
            var dotPos = mantissa.indexOf(".");
            mantissa = mantissa.replace(".", "");
            if (dotPos < 0) {
                dotPos = mantissa.length;
            }
            dotPos = dotPos + exponent;
            if (dotPos <= 0) {
                return "0." + (new Array(Math.abs(dotPos))).join("0") + mantissa;
            } else if (mantissa.length <= dotPos) {
                return mantissa + (new Array(dotPos - mantissa.length + 1)).join("0")
            }
            return mantissa.substr(0, dotPos) + "." + mantissa.substr(dotPos);

        },
        resolveNamedFormat = function(target, format){
            var f = namedFormat[format];
            if(!f || !isFunc(f.format)){
                return false;
            }
            return f.format(target, format) || "";
        },
        round = function(target, prec, func){
            func = func || math.round;
            prec = prec || 0;
            var mul = math.pow(10, math.abs(prec));
            if(prec < 0) mul = 1 / mul;
            return func(target * mul) / mul;
        };

    var TypyNum = Typy.num = extDefine(function (constractor, target) {
        if (isNum(target)) {
            return new constractor(target);
        }
        if (isNumeric(target)) {
            return new constractor(parseFloat(target));
        }
        if(isBool(target)){
            return new constractor(target ? 1 : 0);
        }
        return new constractor(0);
    }, {
        format:unchainable(function (target, format) {
            var namedFormatVal = resolveNamedFormat(target, format);
            if(isStr(namedFormatVal)){
                return namedFormatVal;
            }
            var tokens = tokenize(format),
                isMinus = target < 0,
                targetVal = target,
                targetStr,
                hasToken = function(v){
                    if(!v){
                        return false;
                    }
                    if(v.ints && v.ints.length){
                        return v;
                    }
                    return false;
                },
                targetTokens = isMinus ? (hasToken(tokens.minus) || tokens.plus) : tokens.plus,
                hasDecPlaceholder = function (v) {
                    var i, l;
                    if (!v) {
                        return false;
                    }
                    for (i = 0, l = v.length; i < l; i++) {
                        if (v[i].holder) {
                            return true;
                        }
                    }
                    return false;
                },
                splitPart = function (v) {
                    var splited = v.split(".");
                    if (splited.length === 1) {
                        splited[1] = "";
                    }
                    return splited;
                },
                i, j, l;

            if (targetTokens.parcent > 0) {
                targetVal = targetVal * math.pow(100, targetTokens.parcent);
            }
            if (targetTokens.permil > 0) {
                targetVal = targetVal * math.pow(1000, targetTokens.permil);
            }
            if (targetTokens.pows > 0) {
                targetVal = targetVal / math.pow(1000, targetTokens.pows);
            }

            targetStr = exponentToNumStr(math.abs(targetVal) + "");
            targetVal = parseFloat(targetStr);

            if (targetVal === 0) {
                targetTokens = hasToken(tokens.zero) || targetTokens;
                targetStr = "0";
            } else if (targetVal < 1 && !hasDecPlaceholder(targetTokens.decs)) {
                targetTokens = hasToken(tokens.zero) || targetTokens;
                targetStr = "0";
            }
            var part = splitPart(targetStr),
                tokenPart = targetTokens.ints,
                targetPart = part[0],
                tokenPartVal,
                result = { ints: "", decs: ""},
                dec, hasDec = false;

            for (i = tokenPart.length - 1; i > -1; i--) {
                tokenPartVal = tokenPart[i];
                if (isStr(tokenPartVal)) {
                    result.ints = tokenPartVal + result.ints;
                } else {
                    result.ints = (targetPart.length !== 0 ? targetPart.substr(targetPart.length - 1) :
                        tokenPartVal.type === "0" ? "0" : "") + result.ints;
                    targetPart = targetPart.substr(0, targetPart.length - 1);
                    if (targetTokens.separate) {
                        if ((part[0].length - targetPart.length) % 3 === 0 && targetPart.length !== 0) {
                            result.ints = "," + result.ints;
                        }
                    }

                    if (!TypyArray(tokenPart).take(i).some(function (v) { return !isStr(v); })) {
                        if (targetTokens.separate) {
                            for (j = targetPart.length; j; j--) {
                                result.ints = targetPart.charAt(j - 1) + result.ints;
                                targetPart = targetPart.substr(0, targetPart.length - 1);
                                if ((part[0].length - targetPart.length) % 3 === 0 && targetPart.length !== 0) {
                                    result.ints = "," + result.ints;
                                }
                            }
                        } else {
                            result.ints = targetPart + result.ints;
                        }
                    }
                }
            }

            tokenPart = targetTokens.decs || [];
            targetPart = part[1];

            for (i = 0, l = tokenPart.length; i < l; i++) {
                tokenPartVal = tokenPart[i];
                if(isStr(tokenPartVal)){
                    result.decs += tokenPartVal;
                } else {
                    dec = targetPart.charAt(0) ? targetPart.charAt(0) :
                        tokenPartVal.type === "0" ? "0" : "";
                    if (dec) {
                        hasDec = true;
                    }
                    result.decs += dec;
                    targetPart = targetPart.substr(1);
                }
            }
            return ((isMinus && targetTokens == tokens.plus) ? "-" : "") + result.ints + (hasDec ? "." + result.decs : result.decs);
        }),
        clamp: function (target, min, max) {
            if (isNumeric(target)) {
                target = parseFloat(target);
            }
            if (!isNum(target)) {
                return;
            }
            if (!isNum(min) || !isNum(max)) {
                return NaN;
            }
            var swap;
            if (min > max) {
                swap = min;
                min = max;
                max = swap;
            }
            return math.max(min, math.min(max, target));
        },
        log10:function (target) {
            return math.LOG10E * math.log(target);
        },
        round: round,
        ceil: function(target, prec){
            return round(target, prec, math.ceil);
        },
        floor: function(target, prec){
            return round(target, prec, math.floor);
        }
    }, isNum);

    TypyArray.each("abs,pow,sin,asin,cos,acos,tan,atan,exp,pow,sqrt,log".split(","), function(item){
        addFunc(TypyNum, item, function(){
            return math[item].apply(math, arguments);
        });
    });

    addStaticFunc(TypyNum, "namedFormat", function(name){
        if(!isStr(name)) return;
        if(arguments.length === 1){
            return namedFormat[name];
        }
        if(arguments.length >= 2){
            namedFormat[name] = arguments[1];
        }
    });
})(this, Typy);

var TypyNum = Typy.num;