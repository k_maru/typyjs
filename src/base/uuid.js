/* uuid */
// This program referred to UUID.js.
// https://github.com/LiosK/UUID.js

// Copyright (c) 2010-2012 LiosK.
//
//     Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
//     The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
//     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// dependon array.js, string.js

(function (global, Typy) {

//0: timeLow
//1: timeMid
//2: timeHiAndVersion
//3: clockSeqHiAndReserved
//4: clockSeqLow
//5: node

    function Uuid() {
        var vals = slice.call(arguments),
            valStrs = TypyArray.map(vals, function (val, index) {
                return align(val, index === 0 ? 8 :
                    index === 1 || index === 2 ? 4 :
                        index === 3 || index === 4 ? 2 :
                            12);
            }),
            empty = !TypyArray.some(vals, function (val) {
                return val !== 0
            });

        this._vals = vals;
        this._valStrs = valStrs;
        this.isEmpty = function () {
            return empty;
        }
    }

    Uuid.prototype.toString = function (format) {
        var placeholder, args;

        if (isUndef(format) || isNull(format) || format === "") {
            format = "D";
        }
        placeholder = formats[format];
        if (placeholder) {
            args = Array.apply(null, this._valStrs);
            args.unshift(placeholder);
            return TypyStr.format.apply(TypyStr, args);
        }
        return "";
    };

    var random = function (range) {
            if (range < 0) return NaN;
            if (range <= 30) return Math.floor(Math.random() * (1 << range));
            if (range <= 53) return Math.floor(Math.random() * (1 << 30)) +
                Math.floor(Math.random() * (1 << range - 30)) * (1 << 30);
            return NaN;
        },
        parseRegx = /^([0-9a-f]{8})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{12})$/i,
        parseSeparatedRegx = /^[\(\{]?([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{2})([0-9a-f]{2})-([0-9a-f]{12})[\)\}]?$/i,
        parse = function (value) {
            var items;
            if (!isStr(value)) {
                return;
            }
            if (value.length === 32) {
                items = parseRegx.exec(value);
            } else {
                items = parseSeparatedRegx.exec(value);
            }
            if (!items) {
                return;
            }
            return new Uuid(
                parseInt16(items[1]), parseInt16(items[2]),
                parseInt16(items[3]), parseInt16(items[4]),
                parseInt16(items[5]), parseInt16(items[6])
            );
        },
        gen = function () {
            return new Uuid(
                random(32), random(16),
                0x4000 | random(12),
                0x80 | random(6),
                random(8), random(48));
        },
        align = function (value, length) {
            return TypyStr.padLeft(value.toString(16), length, "0");
        },
        emptyuuid = new Uuid(0, 0, 0, 0, 0, 0),
        formats = {
            "N":"{0}{1}{2}{3}{4}{5}",
            "D":"{0}-{1}-{2}-{3}{4}-{5}",
            "B":"{{0}-{1}-{2}-{3}{4}-{5}}",
            "P":"({0}-{1}-{2}-{3}{4}-{5})"
        },
        isUuid = function (val) {
            return val instanceof Uuid;
        };

    var TypyUuid = Typy.uuid = extDefine(function (constractor, target) {
        var empty = false;

        constractor.prototype.val = function(){
            return !isUuid(this._val) ? void 0 : this._val.toString();
        };

        if (isStr(target)) {
            if (target) {
                return new constractor(parse(target));
            }
            empty = false;
        }
        if (isBool(target)) {
            empty = target;
        }
        if (isNum(target)) {
            if (target !== 0) {
                return new constractor();
            }
            empty = true;
        }
        if (empty) {
            return new constractor(emptyuuid);
        }
        return new constractor(gen());
    }, {
        format: unchainable(function(target, format){
            if (isUuid(target)) {
                return target.toString(format);
            }
        })
    }, isUuid);

    addInstanceFunc(TypyUuid, "toString", unchainable(function (format) {
        if (isUuid(this._val)) {
            return this._val.toString(format);
        }
        return "";
    }));

})(this, Typy);

var TypyUuid = Typy.uuid;