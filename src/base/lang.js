/* lang */
(function (global, Typy) {

    var langs = {},
        currentLang,
        defaultLangDef = {
            text:{

            },
            numeric:{

            },
            calendar:{
                months:{
                    shortNames:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),
                    names:"January February March April May June July August September October November December".split(" ")
                },
                meridiem:{
                    shortNames:{ ante:"A", post:"P" },
                    names:{ ante:"AM", post:"PM" }
                },
                weekdays:{
                    shortNames:"Sun Mon Tue Wed Thu Fri Sat".split(" "),
                    names:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" ")
                },
                defaultFormats:["yyyy/M/d", "M/d/yyyy"]
            }
        },
        mixin = function () {
            var options, name, src, copy, copyIsArray, clone,
                target = arguments[0] || {},
                i = 1,
                length = arguments.length;

            if (typeof target !== "object" && !isFunc(target)) {
                target = {};
            }

            for (; i < length; i++) {
                if ((options = arguments[ i ]) != null) {
                    for (name in options) {
                        src = target[ name ];
                        copy = options[ name ];
                        if (target === copy) {
                            continue;
                        }
                        if (copy && ( isObj(copy) || (copyIsArray = isArray(copy)) )) {
                            if (copyIsArray) {
                                copyIsArray = false;
                                clone = src && isArray(src) ? src : [];
                            } else {
                                clone = src && isObj(src) ? src : {};
                            }
                            target[ name ] = mixin(clone, copy);
                        } else if (copy !== undefined) {
                            target[ name ] = copy;
                        }
                    }
                }
            }
            return target;
        }, i, l, props = ["calendar", "numeric", "text"];

    langs["en"] = mixin({}, defaultLangDef);
    currentLang = "en";

    var TypyLang = Typy.lang = function () {
        var targetLang,
            targetLangDef,
            langDef,
            newLangDef;
        if (arguments.length === 0) {
            return langs[currentLang];
        }
        if (arguments.length === 1) {
            return langs[arguments[0]];
        } else if (arguments.length >= 2) {
            targetLang = arguments[0];
            targetLangDef = arguments[1] || {};
            langDef = langs[targetLang] || {};
            newLangDef = mixin({}, defaultLangDef, targetLangDef, langDef);
            langs[arguments[0]] = newLangDef;
        }
    };
    Typy.lang.current = function () {
        if (arguments.length === 0) {
            return currentLang;
        }
        if (arguments.length >= 1) {
            currentLang = arguments[0];
            return TypyLang;
        }
    };

    for(i = 0, l = props.length; i < l; i++){
        (function(p){
            Typy.lang[p] = function(){
                var def, arg = arguments;
                if (arg.length === 0) {
                    return langs[currentLang] ? langs[currentLang][p] : undefined
                }
                if (arg.length > 0) {
                    def = langs[currentLang] || mixin({}, defaultLangDef);
                    def[p] = def[p] || {};
                    def[p] = mixin(def[p], arg[0]);
                    langs[currentLang] = def;
                    return def;
                }
            }
        }).call(null, props[i]);
    }


})(this, Typy);

var TypyLang = Typy.lang;