/* uri */
// This program referred to uri_funcs.js.
// Author (original): Mike J. Brown <mike at skew.org>
// http://skew.org/uri/uri_funcs.js

/******************************************************************************
 uri_funcs.js - URI functions based on STD 66 / RFC 3986

 Author (original): Mike J. Brown <mike at skew.org>
 Version: 2007-01-04

 License: Unrestricted use and distribution with any modifications permitted,
 so long as:
 1. Modifications are attributed to their author(s);
 2. The original author remains credited;
 3. Additions derived from other code libraries are credited to their sources
 and used under the terms of their licenses.

 *******************************************************************************/

// depends array

(function (global, Typy, undefined) {

    //RFC 3986 appendix B.
    var parseUriRegx = /^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?$/,
        missingGroupSupport = (typeof "".match(/(a)?/)[1] !== "string"),
        parseUri = function (target) {
            var matches;
            if (!isStr(target)) {
                return;
            }
            matches = target.match(parseUriRegx);
            matches.shift();
            return {
                schema:!missingGroupSupport && matches[0] === "" ? undefined : matches[1],
                authority:!missingGroupSupport && matches[2] === "" ? undefined : matches[3],
                path:matches[4],
                query:!missingGroupSupport && matches[5] === "" ? undefined : matches[6],
                fragment:!missingGroupSupport && matches[7] === "" ? undefined : matches[8]
            };
        },
        isAbsoluteRegx = /^[A-Z][0-9A-Z+\-\.]*:/i,
        isAbsolute = function (target) {
            return isAbsoluteRegx.test(target);
        },
        toUriString = function (target) {
            var result = "",
                path = target.path;
            if (!isUndef(target.schema)) {
                result += target.schema + ":";
            }
            if (!isUndef(target.authority)) {
                result += "//" + target.authority;
            }
            if (!isUndef(path) && path.length > 0) {
                if (!isUndef(target.authority)) {
                    if (path.charAt(0) !== "/") {
                        path = "/" + path;
                    }
                }
                //TODO 仕様の再確認
//                else{
//                    if(path.substr(0, 2) === "//"){
//                        path = "/" + path.replace(/^\/+/, "");
//                    }
//                }
                result += path;
            }

            if (!isUndef(target.query)) {
                result += "?" + target.query;
            }
            if (!isUndef(target.fragment)) {
                result += "#" + target.fragment;
            }
            return result;
        },
    //RFC 3986 5.2.4
    //Based on code from 4Suite XML:
    //http://skew.org/uri/uri_funcs.js
        removeDotSegments = function (path) {
            var startIsSeparator = false,
                segments,
                seg,
                keepers = [];
            // return empty string if entire path is just "." or ".."
            if (path === "." || path === "..") {
                return "";
            }
            // remove all "./" or "../" segments at the beginning
            while (path) {
                if (path.substring(0, 2) === "./") {
                    path = path.substring(2);
                } else if (path.substring(0, 3) === "../") {
                    path = path.substring(3);
                } else {
                    break;
                }
            }
            if (path.charAt(0) === "/") {
                path = path.substring(1);
                startIsSeparator = true;
            }
            if (path.substring(path.length - 2) === "/.") {
                path = path.substring(0, path.length - 1);
            }
            segments = path.split("/").reverse();
            while (segments.length) {
                seg = segments.pop();
                if (seg === "..") {
                    if (keepers.length) {
                        keepers.pop();
                    } else if (!startIsSeparator) {
                        keepers.push(seg);
                    }
                    if (!segments.length) {
                        keepers.push("");
                    }
                } else if (seg !== ".") {
                    keepers.push(seg);
                }
            }
            // reassemble the kept segments
            return (startIsSeparator && "/" || "") + keepers.join("/");
        },
    //RFC 3986 5.2.2
        absolutize = function (base, relative) {
            var part,
                relSchema, relAuth, relPath, relQuery, relFrag,
                bSchema, bAuth, bPath, bQuery,
                resSchema, resAuth, resPath, resQuery, resFrag;
            if (!isStr(base) || !isAbsolute(base) || !isStr(relative)) {
                return;
            }

            if (relative == "" || relative.charAt(0) == "#") {
                return parseUri(base.split('#')[0] + relative);
            }
            part = parseUri(relative) || {};
            relSchema = part.schema;
            relAuth = part.authority;
            relPath = part.path;
            relQuery = part.query;
            relFrag = part.fragment;
            if (relSchema) {
                resSchema = relSchema;
                resAuth = relAuth;
                resPath = removeDotSegments(relPath);
                resQuery = relQuery;
            } else {
                part = parseUri(base) || {};
                bSchema = part.schema;
                bAuth = part.authority;
                bPath = part.path;
                bQuery = part.query;
                if (relAuth) {
                    resAuth = relAuth;
                    resPath = removeDotSegments(relPath);
                    resQuery = relQuery;
                } else {
                    if (!relPath) {
                        resPath = bPath;
                        resQuery = relQuery ? relQuery : bQuery;
                    } else {
                        if (relPath.charAt(0) === "/") {
                            resPath = removeDotSegments(relPath);
                        } else {
                            //RFC 3986 5.2.3
                            if (bAuth && !bPath) {
                                resPath = "/" + relPath;
                            } else {
                                resPath = bPath.substring(0, bPath.lastIndexOf("/") + 1) + relPath;
                            }
                            resPath = removeDotSegments(resPath);
                        }
                        resQuery = relQuery;
                    }
                    resAuth = bAuth;
                }
                resSchema = bSchema;
            }
            resFrag = relFrag;
            return {
                schema:resSchema,
                authority:resAuth,
                path:resPath,
                query:resQuery,
                fragment:resFrag
            };
        },

        isUri = function (val) {
            return val instanceof Uri;
        },

        property = function (target, value, prop, setCallback) {
            var source;
            if (isUndef(target) && isUndef(value)) {
                return;
            }
            source = isStr(target) ? parseUri(target) : (isUri(target) ? target._val : void 0);
            if (!source) {
                return;
            }
            if (isUndef(value)) {
                return source[prop];
            }
            if (isStr(value)) {
                if (isFunc(setCallback)) {
                    value = setCallback(source, value);
                }
                source[prop] = value;
            }
            if (isStr(target)) {
                return toUriString(source);
            }
            return target;
        };

    function Uri(args) {
        this._val = {};
        if (args.length) {
            if (args.length < 2) {
                this._val = parseUri(args[0]);
            } else {
                this._val = absolutize(args[0], args[1]);
            }
        }
        return this;
    }

    Uri.prototype.toString = function(){
        return toUriString(this._val);
    };

    var TypyUri = Typy.uri = extDefine(function (constractor) {
        constractor.prototype.val = function(){
            return !isUri(this._val) ? void 0 : this._val.toString();
        };

        return new constractor(new Uri(takeArg(arguments, 1)));
    }, {}, isUri);

    TypyArray.each(["schema", "authority", "path", "query", "fragment"], function (item) {
        addFunc(TypyUri, item, function (target, value) {
            return property(target, value, item);
        })
    });

    addStaticFunc(TypyUri, "resolve", function (base, relative) {
        if (!isStr(base) || !isStr(relative)) {
            return;
        }
        var uri = absolutize(base, relative);
        return toUriString(uri);
    });

})(this, Typy);

var TypyUri = Typy.uri;