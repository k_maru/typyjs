/* function */

(function (global, Typy) {
    "use strict";

    Typy.func = extDefine(function (constractor, target) {
        if (isFunc(target)) {
            return new constractor(target);
        }
        return new constractor(Typy.noop());
    }, {
        bind: function(target, context){
            var args;
            if (!isFunc(target)) {
                return;
            }
            if (Function.prototype.bind && target.bind === Function.prototype.bind) {
                return target.bind.apply(target, slice.call(arguments, 1));
            } else {
                args = slice.call(arguments, 2);
                return function () {
                    return target.apply(context, args.concat(slice.call(arguments)));
                };
            }
        }
    }, isFunc);
    addInstanceFunc(Typy.func, "exec", unchainable(function () {
        return this._val.apply(void 0, arguments);
    }));
})(this, Typy);

var TypyFunc = Typy.func;