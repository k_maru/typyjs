/* Array */

(function (global, Typy) {
    "use strict";

    var proto = Array.prototype,
        insert = function (target, index, values) {
            if (!isArray(target) || !isNum(index)) {
                return;
            }
            if (arguments.length === 2) {
                return target;
            }
            if (arguments.length === 3) {
                if (!isArray(values)) {
                    values = [values];
                }
            }
            if (arguments.length > 3) {
                values = slice.call(arguments);
                values.shift();
                values.shift();
            }
            target.splice.apply(target, [index, 0].concat(values));
            return target;
        },
        push = function (target, values) {
            if (!isArray(target)) {
                return;
            }
            var args = slice.call(arguments);
            args.shift();
            return insert.apply(insert, [target, target.length].concat(args));
        },
        flatten = function (target, deep, result) {
            var i, l;
            if (!isArray(target)) {
                return;
            }
            for (i = 0, l = target.length; i < l; i++) {
                if (isArray(target[i])) {
                    !!deep ? flatten(target[i], deep, result) : proto.push.apply(result, target[i]);
                } else {
                    result.push(target[i]);
                }
            }
            return result;
        },
        findDataWithIndex = function(target, callback, fromIndex){
            var i = 0, l, result = {index: -1};
            target = toArray(target);
            if (!isArray(target)) {
                return;
            }
            if (!isFunc(callback)) {
                result.index = TypyArray.indexOf(target, callback, fromIndex);
                if (result.index !== -1) {
                    result.data = target[result.index];
                    return result;
                }
            } else {
                if (isNumeric(fromIndex)) {
                    i = Math.max(0, parseInt10(fromIndex));
                }
                for (l = target.length; i < l; i++) {
                    if (callback(target[i], i)) {
                        result.index = i;
                        result.data = target[i];
                        return result;
                    }
                }
            }
            return result;
        },
        findLastDataWithIndex =  function (target, callback, fromIndex) {
            var l, result = {index: -1};

            target = toArray(target);
            if (!isArray(target)) {
                return;
            }
            if (!isFunc(callback)) {
                result.index = TypyArray.lastIndexOf(target, callback, fromIndex);
                if (result.index !== -1) {
                    result.data = target[result.index];
                    return result;
                }
            }
            l = target.length;
            if (isNumeric(fromIndex)) {
                l = Math.max(Math.min(parseInt10(fromIndex) + 1, target.length), 0);
            }
            while (l--) {
                if (callback(target[l], l)) {
                    result.index = l;
                    result.data = target[l];
                    return result;
                }
            }
            return result;
        };

    var TypyArray = Typy.array = extDefine(function (constractor, target) {
        if (isArray(target)) {
            //copy
            return new constractor(target.concat());
        }
        return new constractor(toArray(target));
    }, {
        /***
         * @module Typy.array
         * @method indexOf
         * @summary 引数に与えられた内容と同じ内容を持つ配列要素の内、最初のもののインデックスを返します。存在しない場合は -1 を返します。
         * @param target:Any[] 検索する配列。配列でない場合は Typy.toArray を利用して配列化されます。
         * @param item:Any 検索する値
         * @param fromIndex?:Number 検索する開始インデックス
         * @return Number インデックス
         */
        indexOf: unchainable(function (target, item, fromIndex) {
            var i = 0, l;
            target = toArray(target);
            if (!isArray(target)) {
                return -1;
            }
            if (proto.indexOf && target.indexOf === proto.indexOf) {
                return proto.indexOf.apply(target, takeArg(arguments, 1));
            }
            if (isNumeric(fromIndex)) {
                i = Math.max(0, parseInt10(fromIndex));
            }
            for (l = target.length; i < l; i++) {
                if (i in target && target[i] === item) {
                    return i;
                }
            }
            return -1;
        }),
        /***
         * @module Typy.array
         * @method lastIndexOf
         * @summary 引数に与えられた内容と同じ内容を持つ配列要素の内、最後のもののインデックスを返します。存在しない場合は -1 を返します。
         * @param target:Any[] 検索する配列。配列でない場合は Typy.toArray を利用して配列化されます。
         * @param item:Any 検索する値
         * @param fromIndex?:Number 検索する開始インデックス
         * @return Number インデックス
         * @remarks 検索は後方から開始されます。
         */
        lastIndexOf: unchainable(function (target, item, fromIndex) {
            var start = fromIndex, l;
            target = toArray(target);
            if (!isArray(target)) {
                return -1;
            }
            if (proto.lastIndexOf && target.lastIndexOf === proto.lastIndexOf) {
                return proto.lastIndexOf.apply(target, takeArg(arguments, 1));
            }
            if (arguments.length < 3) {
                start = target.length;
            }
            if (isNumeric(start)) {
                l = Math.max(Math.min(parseInt10(start) + 1, target.length), 0);
            }
            while (l--) {
                if (l in target && target[l] === item) {
                    return l;
                }
            }
            return -1;
        }),
        /***
         * @module Typy.array
         * @method find
         * @summary 指定された関数の結果が真を返す最初の要素を返します。
         * @return Any 判定の関数の結果で真が返った最初の要素
         * @param target:Any[] 検索する配列。配列でない場合は Typy.toArray を利用して配列化されます。
         * @param callback:Function(value:Any,index:Number) 判定を実行する関数
         * @param fromIndex?:Number 検索を開始するインデックス
         */
        find: unchainable(function (target, callback, fromIndex) {
            var result = findDataWithIndex(target, callback, fromIndex);
            if(result){
                return result.data;
            }
        }),
        /***
         * @module Typy.array
         * @method findIndex
         * @summary 指定された関数の結果が真を返す最初の要素のインデックスを返します。
         * @return Number 判定の関数の結果で真が返った最初の要素のインデックス
         * @param target:Any[] 検索する配列。配列でない場合は Typy.toArray を利用して配列化されます。
         * @param callback:Function(value:Any,index:Number) 判定を実行する関数
         * @param fromIndex?:Number 検索を開始するインデックス
         */
        findIndex: unchainable(function (target, callback, fromIndex) {
            var result = findDataWithIndex(target, callback, fromIndex);
            if(result){
                return result.index;
            }
        }),
        /***
         * @module Typy.array
         * @method findLast
         * @summary 指定された関数の結果が真を返す最後の要素を返します。
         * @return Any 判定の関数の結果で真が返った最後の要素
         * @param target:Any[] 検索する配列。配列でない場合は Typy.toArray を利用して配列化されます。
         * @param callback:Function(value:Any,index:Number) 判定を実行する関数
         * @param fromIndex?:Number 検索を開始するインデックス
         * @remarks 検索は後方から開始されます。
         */
        findLast: unchainable(function (target, callback, fromIndex) {
            var result = findLastDataWithIndex(target, callback, fromIndex);
            if(result){
                return result.data;
            }
        }),
        /***
         * @module Typy.array
         * @method findLastIndex
         * @summary 指定された関数の結果が真を返す最後の要素のインデックスを返します。
         * @return Any 判定の関数の結果で真が返った最後の要素のインデックス
         * @param target:Any[] 検索する配列。配列でない場合は Typy.toArray を利用して配列化されます。
         * @param callback:Function(value:Any,index:Number) 判定を実行する関数
         * @param fromIndex?:Number 検索を開始するインデックス
         * @remarks 検索は後方から開始されます。
         */
        findLastIndex: unchainable(function (target, callback, fromIndex) {
            var result = findLastDataWithIndex(target, callback, fromIndex);
            if(result){
                return result.index;
            }
        }),
        each: function (target, callback, context) {
            var i, l;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return;
            }
            if (proto.forEach && target.forEach === proto.forEach) {
                proto.forEach.apply(target, takeArg(arguments, 1));
                return target;
            }
            for (i = 0, l = target.length; i < l; i++) {
                if (i in target) {
                    callback.call(context, target[i], i, target);
                }
            }
            return target;
        },
        map: function (target, callback, context) {
            var result = [], i, l;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return [];
            }
            if (proto.map && target.map === proto.map) {
                return proto.map.apply(target, takeArg(arguments, 1));
            }
            for (i = 0, l = target.length; i < l; i++) {
                if (i in target) {
                    result.push(callback.call(context, target[i], i, target));
                }
            }
            return result;
        },
        reduce: unchainable(function (target, callback, initialValue) {
            var value, i, l;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return;
            }
            if (proto.reduce && target.reduce === proto.reduce) {
                return proto.reduce.apply(target, takeArg(arguments, 1));
            }
            i = 0;
            value = initialValue;
            if (arguments.length < 2) {
                i = 1;
                value = target[0];
            }
            for (l = target.length; i < l; i++) {
                value = callback(value, target[i], i, target);
            }
            return value;

        }),
        reduceRight: unchainable(function (target, callback, initialValue) {
            var value, l;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return;
            }
            if (proto.reduceRight && target.reduceRight === proto.reduceRight) {
                return proto.reduceRight.apply(target, takeArg(arguments, 1));
            }
            l = target.length;
            value = initialValue;
            if (arguments.length < 2) {
                l--;
                value = target[l];
            }
            while (l--) {
                value = callback(value, target[l], l, target);
            }
            return value;
        }),
        filter: function (target, callback, context) {
            var result = [], i, l, val;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return [];
            }
            if (proto.filter && proto.filter === target.filter) {
                return proto.filter.apply(target, takeArg(arguments, 1));
            }
            for (i = 0, l = target.length; i < l; i++) {
                if (i in target) {
                    val = target[i];
                    if (callback.call(context, target[i], i, target)) {
                        result.push(val);
                    }
                }
            }
            return result;
        },
        every: function (target, callback, context) {
            var i, l;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return [];
            }
            if (proto.every && proto.every === target.every) {
                return proto.every.apply(target, takeArg(arguments, 1));
            }
            for (i = 0, l = target.length; i < l; i++) {
                if (i in target && !callback.call(context, target[i], i, target)) {
                    return false;
                }
            }
            return true;
        },
        some: function (target, callback, context) {
            var i, l;
            target = toArray(target);
            if (!isArray(target) || !isFunc(callback)) {
                return [];
            }
            if (proto.some && proto.some === target.some) {
                return proto.some.apply(target, takeArg(arguments, 1));
            }
            for (i = 0, l = target.length; i < l; i++) {
                if (i in target && callback.call(context, target[i], i, target)) {
                    return true;
                }
            }
            return false;
        },
        contains: function (target, val) {
            target = toArray(target);
            if (!isArray(target)) {
                return;
            }
            return TypyArray.indexOf(target, val) >= 0;
        },
        skip: function (target, count) {
            var result = [], i, l;
            if (!isArray(target) || !isNum(count)) {
                return result;
            }
            i = Math.max(count < 0 ? target.length + count : count, 0);

            for (l = target.length; i < l; i++) {
                result.push(target[i]);
            }
            return result;
        },
        take: function (target, count) {
            var result = [], i;
            if (!isArray(target) || !isNum(count)) {
                return result;
            }
            count = count < 0 ? target.length + count : count;
            if (count < 0) {
                return result;
            }
            for (i = 0; i < Math.min(count, target.length); i++) {
                result.push(target[i]);
            }
            return result;
        },
        first: unchainable(function (target) {
            if (isArray(target)) {
                return target[0];
            }
        }),
        last: unchainable(function (target) {
            if (isArray(target)) {
                return target[target.length - 1];
            }
        }),
        sort: function (target, callback) {
            var copy;
            if (!isArray(target)) {
                return [];
            }
            copy = target.concat();
            copy.sort(callback);
            return copy;
        },
        remove: function (target, index, length) {
            if (!isArray(target) || !isNum(index)) {
                return;
            }
            length = isNum(length) ? length : 1;
            target.splice(index, length);
            return target;
        },
        insert:insert,
        push:push,
        add:push,
        flatten: function (target, deep) {
            return flatten(target, deep, []);
        },
        max: unchainable(function (target) {
            var i, l, lastVal;

            if (!isArray(target)) {
                return;
            }
            lastVal = target[0];
            for (i = 1, l = target.length; i < l; i++) {
                lastVal = Math.max(lastVal, target[i]);
            }
            return lastVal;
        }),
        min: unchainable(function (target) {
            var i, l, lastVal;

            if (!isArray(target)) {
                return;
            }
            lastVal = target[0];
            for (i = 1, l = target.length; i < l; i++) {
                lastVal = Math.min(lastVal, target[i]);
            }
            return lastVal;
        })
    }, isArray);

    addStaticFunc(TypyArray, "make", function (length, data) {
        var args = toArray(arguments),
            i, dataPoint,
            result = [];
        if (isNum(length) && length >= 0) {
            args.shift();
            dataPoint = 0;
            for (i = 0; i < length; i++) {
                result.push(args.length === 0 ? undefined : (isFunc(args[dataPoint]) ? args[dataPoint](i) : args[dataPoint]));
                dataPoint++;
                if (args.length <= dataPoint) {
                    dataPoint = 0;
                }
            }
            return result;
        }
    });

})(this, Typy);

var TypyArray = Typy.array;
