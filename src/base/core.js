/* core */

var parseInt10 = function (val) {
        return parseInt(val, 10);
    },
    parseInt16 = function (val) {
        return parseInt(val, 16);
    },
    toString = Object.prototype.toString,
    hasOwnProperty = Object.prototype.hasOwnProperty,
    isXWithCallback = function(func, args){
        var i = 0, l = args.length;
        if(l === 0){
            return false;
        }else{
            for(; i < l; i++){
                if(!func(args[i])){
                    return false;
                }
            }
        }
        return true;
    },
    isX = function(name, args, additional){
        var key = "[object " + name + "]";
        additional = additional || function(){return true;};
        return isXWithCallback(function(v){return toString.call(v) === key && additional(v);}, args);
    },
    /***
     * @module Typy
     * @method isUndef
     * @summary
     * 指定された引数の値がすべて undefined かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean 指定された引数の値がすべて undefined の場合は true 、そうでない場合は false
     * @example
     * Typy.isUndef("abcde") -> false
     * Typy.isUndef(undefined) -> true
     * Typy.isUndef("abcde", undefined) -> false
     * Typy.isUndef(void 0, undefined) -> true
     */
    isUndef = function () {
        return isXWithCallback(function(v){return typeof v === "undefined";}, slice.call(arguments));
    },
    /***
     * @module Typy
     * @method isObj
     * @summary
     * 指定された引数の値がすべて Object かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean
     * 指定された引数の値がすべて Object の場合は true 、 そうでない場合は false
     * @example
     * Typy.isObj({param1: "A"}) -> true
     * Typy.isObj({param1: "A"}, {param2: "B"}) -> true
     * Typy.isObj(1) -> false
     * Typy.isObj(1, {param2: "B"}) -> false
     */
    isObj = function () {
        return isX("Object", slice.call(arguments), function(v){
            //IE8以前対策
            return typeof v  !== "undefined" && v !== null;
        });
    },
    /***
     * @module Typy
     * @method isFunc
     * @summary
     * 指定された引数の値がすべて Function かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean
     * 指定された引数の値がすべて Function の場合は true 、 そうでない場合は false
     * @example
     * Typy.isFunc(function(){}) -> true
     * Typy.isFunc(function(){}, function(){}) -> true
     * Typy.isFunc(1) -> false
     * Typy.isFunc(1, function(){}) -> false
     */
    isFunc = function () {
        return isX("Function", slice.call(arguments));
    },
    /***
     * @module Typy
     * @method isStr
     * @summary
     * 指定された引数の値がすべて String かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean
     * 指定された引数の値がすべて String の場合は true 、 そうでない場合は false
     * @example
     * Typy.isStr("abcd") -> true
     * Typy.isStr("abcd", "1234") -> true
     * Typy.isStr(1) -> false
     * Typy.isStr(1, "abcd") -> false
     */
    isStr = function () {
        return isX("String", slice.call(arguments));
    },
    /***
     * @module Typy
     * @method isArray
     * @summary
     * 指定された引数の値がすべて Array かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean
     * 指定された引数の値がすべて Array の場合は true 、 そうでない場合は false
     * @example
     * Typy.isArray([1, 2, 3, 4]) -> true
     * Typy.isArray(["a", "b", "c"], [1, 2, 3]) -> true
     * Typy.isArray(1) -> false
     * Typy.isArray(1, [1, 2, 3]) -> false
     */
    isArray = function () {
        return isX("Array", slice.call(arguments));
    },
    /***
     * @module Typy
     * @method isNum
     * @summary
     * 指定された引数の値がすべて Number かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean
     * 指定された引数の値がすべて Number の場合は true 、 そうでない場合は false
     * @example
     * Typy.isNum(123) -> true
     * Typy.isNum(123, 987) -> true
     * Typy.isNum("abc") -> false
     * Typy.isNum("abc", 123) -> false
     */
    isNum = function () {
        return isX("Number", slice.call(arguments));
    },
    /***
     * @module Typy
     * @method isNull
     * @summary
     * 指定された引数の値がすべて Null かどうかを返します。
     * @param ...args:Any[] 対象となる値
     * @return Boolean
     * 指定された引数の値がすべて Null の場合は true 、 そうでない場合は false
     * @example
     * Typy.isNull(null) -> true
     * Typy.isNull(null, null) -> true
     * Typy.isNull(undefined) -> false
     * Typy.isNull(null, undefined) -> false
     */
    isNull = function () {
        return isXWithCallback(function(v){return v === null;}, slice.call(arguments));
    },
    isUndefOrNull = function () {
        return isXWithCallback(function(v){
            return v === null || typeof v === "undefined";
        }, slice.call(arguments));
    },
    isBool = function () {
        return isX("Boolean", slice.call(arguments));
    },
    isDate = function () {
        return isX("Date", slice.call(arguments));
    },
    isRegExp = function(){
        return isX("RegExp", slice.call(arguments));
    },
    isPrimitive = function () {
        return isXWithCallback(function(v){
            return typeof v === "string" ||
                typeof v === "number" ||
                typeof v === "boolean";
        }, slice.call(arguments));
    },
    isNumeric = function () {
        return isXWithCallback(function(v){
            return !isNaN(parseFloat(v)) && isFinite(v);
        }, slice.call(arguments));
    },
    isInt = function(){
        return isXWithCallback(function(v){
            return isNum(v) &&  (v % 1 === 0);
        }, slice.call(arguments));
    },
    isUnusable = function () {
        return isXWithCallback(function(v){
            return isUndef(v) ||
                isNull(v) ||
                (isNum(v) ? isNaN(v) || !isFinite(v) : false);
        }, slice.call(arguments));
    },
    ifUndef = function (value, def) {
        return isUndef(value) ? def : value;
    },
    ifNull = function (value, def) {
        return isNull(value) ? def : value;
    },
    ifUndefOrNull = function(value, def){
        return isUndef(value) || isNull(value) ? def : value;
    },
    ifUnusable = function (value, def) {
        return isUnusable(value) ? def : value;
    },
    /***
     * @module Typy
     * @method extend
     * @summary 第１引数に指定された Object に対して、後続する引数の値のプロパティを設定します。
     * @param target:Object
     * 拡張する Object
     * @param ...args:Any[] 設定するプロパティを持つ値
     * @return Object プロパティが設定された第１引数に指定された Object
     * @example
     * Typy.extend({prop1: true, prop2: false}, {prop3: "A}, {prop1: "B"})
     *   -> {prop1: "B", prop2: false, prop3: "A"}
     */
    /***
     * @module Typy
     * @override extend
     * @summary 第１引数に指定された Functoin に対して、後続する引数の値のプロパティを設定します。
     * @param target:Functoin 拡張する Functoin
     * @param ...args:Any[] 設定するプロパティを持つ値
     * @return Functoin プロパティが設定された第１引数に指定された Functoin
     */
    /***
     * @module Typy
     * @override extend
     * @summary 引数で指定された値のプロパティを設定した、新しい Object を返します。
     * @param ...args:Any[] 設定するプロパティを持つ値
     * @return Object プロパティが設定された新しい Object
     */
    extend = function () {
        var args = slice.call(arguments),
            target = args[0] || {},
            i, l, p, source;
        if (typeof target !== "object" && !isFunc(target)) {
            target = {};
        }
        if (target == args[0]) {
            args.shift();
        }
        for (i = 0, l = args.length; i < l; i++) {
            source = args[i];
            if (typeof source !== "object" && !isFunc(target)) {
                source = {};
            }
            for (p in source) {
                if (hasOwnProperty.call(source, p)) {
                    target[p] = source[p];
                }
            }
        }
        return target;
    },
    /***
     * @module Typy
     * @method keys
     * @summary 引数で指定された値が持っているプロパティ名を配列で取得します。
     * @param target:Object プロパティを取得する値
     * @return String[] プロパティ名の配列
     * @remarks
     * ES5 で定義される Object.keys 関数が存在する場合は、Object.keys が利用されます。
     * @example
     * Typy.keys({prop1: true, prop2: "A"})
     *   -> ["prop1", "prop2"]
     */
    keys = Object.keys || function (target) {
        if (target !== Object(target)) return [];
        var keys = [], key;
        for (key in target) {
            if (hasOwnProperty.call(target, key)) keys.push(key);
        }
        return keys;
    },
    slice = Array.prototype.slice,
    /***
     * @module Typy
     * @method toArray
     * @summary 引数で指定された値をもとに配列を生成します。
     * @param target:Any 配列を生成する値
     * @return Any[] 生成された配列
     * @remarks
     * 引数が配列の場合は、そのまま返します。引数の値に length プロパティがある場合は、Array.slice　関数を利用して配列が生成されます。
     * それ以外の場合は、引数に指定された値が持つ列挙可能なプロパティの値を配列の値として利用します。
     */
    toArray = function (target) {
        var p,
            result = [];
        if (isUnusable(target)) {
            return;
        }
        if (isArray(target)) {
            return target;
        }
        if (isNum(target.length)) {
            return slice.call(target);
        }
        for (p in target) {
            if (hasOwnProperty.call(target, p)) {
                result.push(target[p]);
            }
        }
        return result;
    },
    takeArg = function(args, count){
        var ary = slice.call(args),
            i = 0, l = count, al = ary.length;
        for(;i < l && i < al; i++){
            ary.shift();
        }
        return ary;
    },
    extcache = [],

    extDefine = function (initializer, funcs, isTarget, unfindable) {
        var constractor,
            extender,
            funckeys, i, l;
        if (!isFunc(initializer)) {
            return;
        }
        constractor = function (target) {
            var self = this;
            for(var p in self){
                if(isFunc(self[p])){
                    (function(p){
                        self[p].x = function(){
                            var r = self[p].apply(self, arguments);
                            if(r instanceof constractor){
                                return r;
                            }
                            return Typy.ext(r);
                        }
                    })(p);
                }
            }

            if (isFunc(isTarget)) {
                if (isTarget(target)) {
                    this._val = target;
                }
            } else {
                this._val = target;
            }
            this._extender = extender;
        };
        extender = function () {
            var args = slice.call(arguments);
            args.unshift(constractor);
            return initializer.apply(null, args);
        };
        constractor.prototype.val = function () {
            return !isTarget(this._val) ? void 0 : this._val;
        };
        constractor.prototype.to = function(target){
            if(isFunc(target) && target._source === Typy){
                return target(this._val);
            }
        };

        extender._constractor = constractor;
        extender._isTarget = isTarget;
        extender._source = Typy;

        addInstanceFunc(extender, "toString", unchainable(function(){
            return this._val + "";
        }));

        funckeys = keys(funcs);
        for (i = 0, l = funckeys.length; i < l; i++) {
            addFunc(extender, funckeys[i], funcs[funckeys[i]]);
        }
        if(!unfindable){
            extcache.push({
                isTarget: isTarget,
                extender: extender
            });
        }

        return extender;
    },
    hasExt = function(val, includeExtended){
        var i, l, extender;
        if(!includeExtended){
            if(!isUnusable(val) && !isUnusable(val._extender)){
                extender = val._extender;
                if(extender._source === Typy && extender._constractor && extender._isTarget){
                    for(i = 0, l = extcache.length; i < l; i++){
                        if(extcache[i].isTarget === extender._isTarget){
                            return true;
                        }
                    }
                }
            }
        }
        for(i = 0, l = extcache.length; i < l; i++){
            if(extcache[i].isTarget(val)){
                return true;
            }
        }
        return false;
    },
    addStaticFunc = function (extender, name, func) {
        if (isUnusable(extender) || !isStr(name) || !isFunc(func)) {
            return;
        }
        extender[name] = func;
    },
    execInstanceFunc = function(extender, context, func, args){
        var result = func.apply(context, args);
        if (!func._unchainable) {
            if (isFunc(extender._isTarget)) {
                if (extender._isTarget(result)) {
                    context._val = result;
                    return context;
                }
            } else {
                context._val = result;
                return context;
            }
        }
        return result;
    },
    addFallbackFunc = function (extender, name) {
        var instanceFunc;
        if (isUnusable(extender) || !isStr(name)) {
            return;
        }
        if (extender._constractor) {
             instanceFunc = function () {
                var args = slice.call(arguments);
                args.unshift(this._val);
                return execInstanceFunc(extender, this, extender[name], args);
            };

            extender._constractor.prototype[name] = instanceFunc;
        }
    },
    addInstanceFunc = function (extender, name, func) {
        var instanceFunc;
        if (isUnusable(extender) || !isStr(name) || !isFunc(func)) {
            return;
        }
        if (extender._constractor) {
            instanceFunc = function () {
                return execInstanceFunc(extender, this, func, slice.call(arguments));
            };

            extender._constractor.prototype[name] = instanceFunc;
        }
    },
    addFunc = function (extender, name, func) {
        addStaticFunc(extender, name, func);
        addFallbackFunc(extender, name);
    },
    unchainable = function (func) {
        if (isFunc(func)) {
            func._unchainable = true;
        }
        return func;
    },
    noop = function () {
    };

extend(Typy, {
    extend: extend,
    noop: noop,
    isUndef: isUndef,
    isUnusable: isUnusable,
    isObj: isObj,
    isStr: isStr,
    isFunc: isFunc,
    isArray: isArray,
    isNull: isNull,
    isNum: isNum,
    isInt: isInt,
    isBool: isBool,
    isDate: isDate,
    isPrimitive: isPrimitive,
    isNumeric: isNumeric,
    isUndefOrNull: isUndefOrNull,
    isRegx: isRegExp,
    ifUndef: ifUndef,
    ifNull: ifNull,
    ifUnusable: ifUnusable,
    ifUndefOrNull: ifUndefOrNull,
    keys: keys,
    toArray:toArray,
    log:function (message) {
        if (console && console.log) {
            console.log(message);
        }
    }
});

Typy.any = extDefine(function (constractor, target){
    return new constractor(target);
}, {}, function(){
    return true;
}, true);

Typy.ext = function(val){
    var i, l, extender;
    if(!isUnusable(val) && !isUnusable(val._extender)){
        extender = val._extender;
        if(extender._source === Typy && extender._constractor && extender._isTarget){
            for(i = 0, l = extcache.length; i < l; i++){
                if(extcache[i].isTarget === extender._isTarget){
                    return val;
                }
            }
        }
    }
    for(i = 0, l = extcache.length; i < l; i++){
        if(extcache[i].isTarget(val)){
            return extcache[i].extender(val);
        }
    }
    return Typy.any(val);
};
extend(Typy.ext,{
    define:extDefine,
    func:addFunc,
    staticFunc:addStaticFunc,
    instanceFunc:addInstanceFunc,
    unchainable:unchainable,
    has: hasExt
});