/* TimeSpan */

// depends array
(function(global, Typy, undef){

    "use strict";

    var msecPerDay = 1000 * 60 * 60 * 24,
        msecPerHour = 1000 * 60 * 60,
        msecPerMinute = 1000 * 60,
        msecPerSecond = 1000,
        msecPerMillisecond = 1,
        cachedFormatTokens = {},
        namedFormat = {},
        isTimeSpan = function(val){
            return val instanceof TimeSpan;
        },
        units = function(val, context,  msecPer, modulo){
            var result = context._val / msecPer,
                round = context._val < 0 ? Math.ceil: Math.floor;

            if(modulo){
                result = result % modulo;
            }
            result = round(result); //Math.floor(result);
            if(isNum(val)){
                context._val -= (result * msecPer);
                context._val += round(val * msecPer); //Math.floor(val * msecPer);
                return context;
            }else{
                return result;
            }
        },
        totalUnit = function(context, msecPer, roundDown){
            var result = context._val / msecPer,
                round = context._val < 0 ? Math.ceil: Math.floor;

            if(roundDown){
                result = round(result);
            }
            return result;
        },
        addUnits = function(context, val, msecPer){
            context._val += val * msecPer;
            return context;
        },
        tokenize = function (format) {
            var i, l, ch,
                tokens = [],
                escaped = false,
                token, quote,
                tokenizeLiteral = function (target, index) {
                    var match = format.substr(index).match(new RegExp(target + "+"));
                    if (match) {
                        return match[0].length;
                    }
                    return 1;
                };
            if (cachedFormatTokens[format]) {
                return cachedFormatTokens[format];
            }
            for (i = 0, l = format.length; i < l; i++) {

                ch = format.charAt(i);
                if(escaped){
                    tokens.push(ch);
                    escaped = false;
                    continue;
                }
                if(ch === "\\"){
                    escaped = true;
                    continue;
                }
                if(ch === "'" || ch === "\""){
                    if(ch === quote){
                        quote = false;
                    }else{
                        quote = ch;
                    }
                    continue;
                }
                if(quote){
                    tokens.push(ch);
                    continue;
                }
                switch (ch) {
                    case "d":
                    case "f":
                    case "h":
                    case "m":
                    case "s":
                        token = {
                            type:ch,
                            length:tokenizeLiteral(ch, i)
                        };
                        tokens.push(token);
                        i += (token.length - 1);
                        break;
                    //TODO: セパレーターをどうするか検討
                    default:
                        tokens.push(ch);
                }
            }
            cachedFormatTokens[format] = tokens;
            return tokens;
        },
        makeText = function(val, format){
            var named = namedFormat[format];
            if (named && isFunc(named.format)) {
                return named.format(source, TypyLang.current());
            }
            var tokens = tokenize(format),
                tokenItem,
                texts = [],
                type, tlength,
                i, l,
                padZero = function(v, length){
                    v = (v === 0 ? 0 : Math.abs(v)) + "";
                    return TypyStr.padLeft(v, Math.max(v.length, length), "0");
                };

            for(i = 0, l = tokens.length; i < l; i++){
                tokenItem = tokens[i];
                if(isStr(tokenItem)){
                    texts.push(tokenItem);
                    continue;
                }
                type = tokenItem.type;
                tlength = tokenItem.length;
                if(type === "d"){
                    texts.push(padZero(val.days(), tlength));
                } else if(type === "h"){
                    texts.push(padZero(val.hours(), Math.min(tlength, 2)));
                } else if(type === "m"){
                    texts.push(padZero(val.minutes(), Math.min(tlength, 2)));
                } else if(type === "s"){
                    texts.push(padZero(val.seconds(), Math.min(tlength, 2)));
                } else if(type === "f"){
                    texts.push((Math.abs(val.milliseconds()) + "000").substr(0, Math.min(tlength, 3)));
                }
            }
            return ((val._val < 0) ? "-" : "") + texts.join("");
        },
        makeTimeSpanFromFormat = function(value, format){
            var named = namedFormat[format],
                tokens, token, i, l, pos = 0,
                values = [0,0,0,0,0],
                isMinus = value.charAt(0) === "-",
                typeNum = {d:0,h:1,m:2,s:3,f:4},
                parseSingleD = function(){
                    var match = value.substr(pos).match(/^[1-9][0-9]*/);
                    if(match && match.length){
                        values[0] = parseInt10(match[0]);
                        return match[0].length;
                    }else{
                        values[0] = -1;
                    }
                },
                parseFixedLength = function(typePos, length){
                    var match = value.substr(pos).match(new RegExp("^\\d{" + length + "}"));
                    if(match && match.length){
                        values[typePos] = parseInt10(match[0]);
                        return length;
                    }else{
                        values[typePos] = -1;
                    }
                },
                parseHMS = function(typePos){
                    var match = value.substr(pos, 2).match(/^\d{1,2}/);
                    if(match && match.length){
                        values[typePos] = parseInt10(match[0]);
                        return match[0].length;
                    }
                },
                parseF = function(length){
                    var match = value.substr(pos, length).match(new RegExp("^\\d{" + length + "}"));
                    if(match && match.length){
                        values[4] = parseInt10(parseFloat("0." + match[0]) * 1000);
                        return length;
                    }
                };
            if (named && isFunc(named.parse)) {
                return named.parse(value);
            }
            if(isMinus){
                value = value.substr(1);
            }

            tokens = tokenize(format);
            for(i = 0, l = tokens.length; i < l; i++){
                token = tokens[i];
                if(isStr(token)){
                    if(value.substr(pos, token.length) === token){
                        pos += token.length;
                    }else{
                        continue;
                    }
                }else{
//                    if(token.type === "." || token.type === ":"){
//                        //TODO: 多言語から取得
//                    }
                    if(token.type === "d"){
                        if(token.length === 1){
                            pos += parseSingleD();
                        }else{
                            pos += parseFixedLength(typeNum[token.type], token.length);
                        }
                    }else if(token.type === "h" || token.type === "m" || token.type === "s"){

                        if(token.length === 1){
                            pos += parseHMS(typeNum[token.type]);
                        }else{
                            pos += parseFixedLength(typeNum[token.type], token.length);
                        }
                    }else if(token.type === "f"){
                        pos += parseF(token.length);
                    }
                }
            }
            if(pos !== value.length){
                return;
            }
            if(values[1] > 23 || values[2] > 59 || values[3] > 59){
                return;
            }
            return  (new TimeSpan(values[0], values[1], values[2], values[3], values[4]))._val * (isMinus ? -1 : 1);
        };

    function TimeSpan(){
        var arg = toArray(arguments);
        this._val = 0;
        if(!isNumeric.apply(null, arg)){
            return;
        }
        if(arg.length === 1){
            //ミリ秒
            this._val = parseInt10(arg[0]);
        }else if(arg.length === 3){
            //時、分、秒
            this._val += msecPerSecond * parseInt10(arg[2]);
            this._val += msecPerMinute * parseInt10(arg[1]);
            this._val += msecPerHour * parseInt10(arg[0]);
        }else if(arg.length === 4){
            //日、時、分、秒
            this._val += msecPerSecond * parseInt10(arg[3]);
            this._val += msecPerMinute * parseInt10(arg[2]);
            this._val += msecPerHour * parseInt10(arg[1]);
            this._val += msecPerDay * parseInt10(arg[0]);
        }else if(arg.length === 5){
            //日、時、分、秒、ミリ秒
            this._val += parseInt10(arg[4]);
            this._val += msecPerSecond * parseInt10(arg[3]);
            this._val += msecPerMinute * parseInt10(arg[2]);
            this._val += msecPerHour * parseInt10(arg[1]);
            this._val += msecPerDay * parseInt10(arg[0]);
        }
    }

    extend(TimeSpan.prototype, {
        days: function(val){
            return units(val, this, msecPerDay);
        },
        hours: function(val){
            return units(val, this, msecPerHour, 24);
        },
        minutes: function(val){
            return units(val, this, msecPerMinute, 60);
        },
        seconds: function(val){
            return units(val, this, msecPerSecond, 60);
        },
        milliseconds: function(val){
            return units(val, this, msecPerMillisecond, 1000);
        },
        totalDays: function(roundDown){
            return totalUnit(this, msecPerDay, roundDown);
        },
        totalHours: function(roundDown){
            return totalUnit(this, msecPerHour, roundDown);
        },
        totalMinutes: function(roundDown){
            return totalUnit(this, msecPerMinute, roundDown);
        },
        totalSeconds: function(roundDown){
            return totalUnit(this, msecPerSecond, roundDown);
        },
        totalMilliseconds: function(roundDown){
            return totalUnit(this, msecPerMillisecond, roundDown);
        },
        add: function(val){
            return addUnits(this, val, msecPerMillisecond);
        },
        addDays: function(val){
            return addUnits(this, val, msecPerDay);
        },
        addHours: function(val){
            return addUnits(this, val, msecPerHour);
        },
        addMinutes: function(val){
            return addUnits(this, val, msecPerMinute);
        },
        addSeconds: function(val){
            return addUnits(this, val, msecPerSecond);
        },
        addMilliseconds: function(val){
            return addUnits(this, val, msecPerMillisecond);
        }
    });

    var TypyTimeSpan = Typy.timeSpan = extDefine(function (constractor, target) {
        var args = takeArg(arguments, 1);
        constractor.prototype.val = function(){
            return !isTimeSpan(this._val) ? void 0 : this._val._val;
        };

        if(args.length === 2 && isStr(target) && isStr(args[1])){
            var milsec = makeTimeSpanFromFormat(target, args[1]);
            if(isNum(milsec)){
                return new constractor(new TimeSpan(milsec))
            }
            return new constractor();
        }
        if(!isNum.apply(Typy, args)){
            return new constractor();
        }

        function F(a){
            return TimeSpan.apply(this, a);
        }
        F.prototype = TimeSpan.prototype;

        return new constractor(new F(args));
    }, {
        format: unchainable(function(target, format) {
            if(!isTimeSpan(target)){
                target = TypyTimeSpan(target)._val;
            }
            if (isTimeSpan(target)) {
                return makeText(target, format);
            }
        })
    }, isTimeSpan);

    TypyArray.each(["days", "hours", "minutes", "seconds", "milliseconds"], function (item) {
        var totalUnitName = "total" + item.charAt(0).toUpperCase() + item.substr(1),
            addUnitsName = "add" + item.charAt(0).toUpperCase() + item.substr(1);
        addInstanceFunc(TypyTimeSpan, item, function (val) {
            if (isTimeSpan(this._val)) {
                return this._val[item].apply(this._val, takeArg(arguments, 0));
            }
        });
        addInstanceFunc(TypyTimeSpan, totalUnitName, function (val) {
            if (isTimeSpan(this._val)) {
                return this._val[totalUnitName].apply(this._val, takeArg(arguments, 0));
            }
        });
        addInstanceFunc(TypyTimeSpan, addUnitsName, function (val) {
            if (isTimeSpan(this._val)) {
                return this._val[addUnitsName].apply(this._val, takeArg(arguments, 0));
            }
        });
    });
    addInstanceFunc(TypyTimeSpan, "add", function (val) {
        if (isTimeSpan(this._val)) {
            return this._val.add(val);
        }
    });
    addStaticFunc(TypyTimeSpan, "namedFormat", function(name){
        if(!isStr(name)) return;
        if(arguments.length === 1){
            return namedFormat[name];
        }
        if(arguments.length >= 2){
            namedFormat[name] = arguments[1];
        }
    });
    addStaticFunc(TypyTimeSpan, "parse", function(target, format){
        if(isStr(target)){
            if(isStr(format)){
                return makeTimeSpanFromFormat(target, format);
            }else{

            }
        }
    });
})(this, Typy);